﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThinClient.Verification.Model
{
    public class SysInformation
    {
        public byte ID { get; set; }

        public string ConfigLabel { get; set; }
        public string ConfigValue { get; set; }
        public string Comments { get; set; }
    }
}
