﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThinClientVerification.Model
{
    class FileDetails
    {
        [System.ComponentModel.DisplayName("File Name")]
        public string FileName { get; set; }
        [System.ComponentModel.DisplayName("File Size (KB)")]
        public float FileSize { get; set; }
        [System.ComponentModel.DisplayName("Status")]
        public string FileStatus { get; set; }
        [System.ComponentModel.DisplayName("File URL")]
        public string FileURI { get; set; }
    }
}
