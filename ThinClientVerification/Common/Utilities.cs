﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Management;
using System.Threading;
using System.Diagnostics;
using System.ServiceProcess;
using ThinClient.Verification.Model;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Linq.Expressions;
using ShareFile.Api.Client;
using System.Threading.Tasks;
using ShareFile.Api.Client.Logging;
using ShareFile.Api.Client.Security.Authentication.OAuth2;
using ShareFile.Api.Models;
using System.Diagnostics.Eventing.Reader;
using System.Text.RegularExpressions;
using System.Net;

namespace ThinClient.Verification.Common
{
    public class Utilities
    {
        #region - Event reader Logging
        public static string EventLogReader(string flag)
        {
            string query = "", path = "", result = null;
            if (flag == "flash")
            {
                query =
                    "<QueryList>" +
                    "  <Query Id=\"0\" Path=\"Citrix-Multimedia-Flash/Admin\">" +
                    "    <Select Path=\"Citrix-Multimedia-Flash/Admin\">" +
                    "        *[System[TimeCreated[timediff(@SystemTime) &lt;= 900000]]]" +
                    "    </Select>" +
                    "  </Query>" +
                    "</QueryList>";

                path = @"%SystemRoot%\System32\Winevt\Logs\Citrix-Multimedia-Flash%4Admin.evtx";
            }
            if (flag == "rave")
            {
                query =
                    "<QueryList>" +
                    "  <Query Id=\"0\" Path=\"Citrix-Multimedia-Rave/Admin\">" +
                    "    <Select Path=\"Citrix-Multimedia-Rave/Admin\">" +
                    "        *[System[TimeCreated[timediff(@SystemTime) &lt;= 900000]]]" +
                    "    </Select>" +
                    "  </Query>" +
                    "</QueryList>";

                path = @"%SystemRoot%\System32\Winevt\Logs\Citrix-Multimedia-Rave%4Admin.evtx";
            }

            try
            {
                EventLogQuery eventQuery = new EventLogQuery(path, PathType.FilePath, query);
                EventLogReader logReader = new EventLogReader(eventQuery);
                EventRecord eventdetail = logReader.ReadEvent();
                if (eventdetail != null)
                {
                    result = eventdetail.FormatDescription();
                }

                return result;
            }
            catch (Exception ex)
            {
                Constants.eventFlag = 1;
                return ex.Message;
            }
        }
        #endregion

        #region - Process Monitor
        public static bool ProcessMonitor(Process proc, int millisecs)
        {
            System.Diagnostics.Stopwatch watcher = new System.Diagnostics.Stopwatch();
            watcher.Start();
            while (watcher.ElapsedMilliseconds < millisecs)
            {
                if (!proc.HasExited)
                {
                    continue;
                }
                else
                {
                    return false;
                }
            }
            proc.Kill();
            watcher.Reset();
            return true;
        }

        public static bool ProcessMonitor(Process proc, int millisecs, System.Windows.Forms.Form _form)
        {
            System.Diagnostics.Stopwatch watcher = new System.Diagnostics.Stopwatch();
            watcher.Start();
            while (watcher.ElapsedMilliseconds < millisecs)
            {
                if (!proc.HasExited)
                {
                    continue;
                }
                else
                {
                    return false;
                }
            }
            proc.Kill();
            watcher.Reset();
            return true;
        }

        public static bool ProcessMonitor(Process proc, int millisecs, bool val)
        {
            System.Diagnostics.Stopwatch watcher = new System.Diagnostics.Stopwatch();
            watcher.Start();
            while (watcher.ElapsedMilliseconds < millisecs)
            {
                if (!proc.HasExited)
                {
                    continue;
                }
                else
                {
                    return false;
                }
            }
            if (val == false)
            {
                proc.Kill();
            }
            watcher.Reset();
            return true;
        }

        public static bool ProcessMonitor(Process proc1, Process proc2, int millisecs)
        {
            System.Diagnostics.Stopwatch watcher = new System.Diagnostics.Stopwatch();
            watcher.Start();
            while (watcher.ElapsedMilliseconds < millisecs)
            {
                if (!proc1.HasExited && !proc2.HasExited)
                {
                    continue;
                }
                else
                {
                    return false;
                }
            }
            proc1.Kill();
            proc2.Kill();
            watcher.Reset();
            return true;
        }

        public static bool ProcessMonitorUnigine(int millisecs)
        {
            System.Diagnostics.Stopwatch watcher = new System.Diagnostics.Stopwatch();
            watcher.Start();
            while (watcher.ElapsedMilliseconds < millisecs)
            {
                continue;
            }
            watcher.Reset();
            return true;
        }

        #endregion - Process Monitor

        #region - Load System Requirements
        public static void LoadSystemRequirements()
        {
            var oSysReq1 = new SysRequirement()
            {
                ID = 1,
                DisplayName = "Microsoft Word",
                DownloadLinkText = "Download",
                DownloadLinkURL = "https://www.office.com/?auth=1",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                Comments = ""
            };
            SysRequirements.oSysRequirements.Add(oSysReq1);


            var oSysReq2 = new SysRequirement()
            {
                ID = 2,
                DisplayName = "Microsoft PowerPoint",
                DownloadLinkText = "Download",
                DownloadLinkURL = "https://www.office.com/?auth=1",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                Comments = ""
            };
            SysRequirements.oSysRequirements.Add(oSysReq2);

            var oSysReq3 = new SysRequirement()
            {
                ID = 3,
                DisplayName = "Microsoft Windows Media Player",
                DownloadLinkText = "Download",
                DownloadLinkURL = "https://www.microsoft.com/en-us/download/windows-media-player-details.aspx",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                Comments = ""
            };
            SysRequirements.oSysRequirements.Add(oSysReq3);

            var oSysReq4 = new SysRequirement()
            {
                ID = 4,
                DisplayName = "Adobe Reader",
                DownloadLinkText = "Download",
                DownloadLinkURL = "https://get.adobe.com/reader/",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                Comments = ""
            };
            SysRequirements.oSysRequirements.Add(oSysReq4);

            var oSysReq5 = new SysRequirement()
            {
                ID = 5,
                DisplayName = "VLC Media Player",
                DownloadLinkText = "Download",
                DownloadLinkURL = "http://www.videolan.org/vlc/",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                Comments = ""
            };
            SysRequirements.oSysRequirements.Add(oSysReq5);

            var oSysReq16 = new SysRequirement()
            {
                ID = 16,
                DisplayName = "Unigine",
                DownloadLinkText = "Download",
                DownloadLinkURL = "https://assets.unigine.com/d/Unigine_Valley-1.0.exe",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                Comments = ""
            };
            SysRequirements.oSysRequirements.Add(oSysReq16);

            /*var oSysReq6 = new SysRequirement()
            {
                ID = 6,
                DisplayName = "Flash Plugin for Internet Explorer",
                DownloadLinkText = "Download",
                DownloadLinkURL = "https://get.adobe.com/flashplayer/",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                Comments = ""
            };*/
            //SysRequirements.oSysRequirements.Add(oSysReq6);

            var oSysReq7 = new SysRequirement()
            {
                ID = 7,
                DisplayName = "HDX Monitor Tool",
                DownloadLinkText = "Download",
                DownloadLinkURL = "https://cis.citrix.com/hdx/download/",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                Comments = ""
            };
            SysRequirements.oSysRequirements.Add(oSysReq7);


            var oSysReq8 = new SysRequirement()
            {
                ID = 8,
                DisplayName = "Microsoft Skype for Business",
                DownloadLinkText = "Download",
                DownloadLinkURL = "https://www.skype.com/en/business/skype-for-business/",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                Comments = ""
            };
            SysRequirements.oSysRequirements.Add(oSysReq8);


            var oSysReq9 = new SysRequirement()
            {
                ID = 9,
                DisplayName = "Microsoft Expression Encoder",
                DownloadLinkText = "Download",
                DownloadLinkURL = "https://citrix.sharefile.com/d-s81489a4e70ac433084fea83f01eb28c0",
                //DownloadLinkURL = "https://www.microsoft.com/en-us/download/details.aspx?id=27870",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                Comments = ""
            };
            SysRequirements.oSysRequirements.Add(oSysReq9);




            //var oSysReq11 = new SysRequirement()
            //{
            //    ID = 11,
            //    DisplayName = "Adobe Flash Player",
            //    DownloadLinkText = "Download",
            //    DownloadLinkURL = "https://get.adobe.com/flashplayer/",
            //    IsLicensed = false,
            //    IsInstalled = false,
            //    Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
            //    Category = Constants.CATEGORY_NAME_HDX_PREMIUM,
            //    Comments = ""
            //};
            //SysRequirements.oSysRequirements.Add(oSysReq11);

            //var oSysReq12 = new SysRequirement()
            //{
            //    ID = 12,
            //    DisplayName = "Microsoft Skype for Business",
            //    DownloadLinkText = "Download",
            //    DownloadLinkURL = "https://www.skype.com/en/business/skype-for-business/",
            //    IsLicensed = false,
            //    IsInstalled = false,
            //    Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
            //    Category = Constants.CATEGORY_NAME_HDX_PREMIUM,
            //    Comments = ""
            //};
            //SysRequirements.oSysRequirements.Add(oSysReq12);


            //var oSysReq13 = new SysRequirement()
            //{
            //    ID = 13,
            //    DisplayName = "Microsoft Expression Encoder",
            //    DownloadLinkText = "Download",
            //    DownloadLinkURL = "https://www.microsoft.com/en-us/download/details.aspx?id=27870",
            //    IsLicensed = false,
            //    IsInstalled = false,
            //    Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
            //    Category = Constants.CATEGORY_NAME_HDX_PREMIUM,
            //    Comments = ""
            //};
            //SysRequirements.oSysRequirements.Add(oSysReq13);


            var oSysReq10 = new SysRequirement()
            {
                ID = 10,
                DisplayName = "nVidia Graphic Drivers",
                DownloadLinkText = "Download",
                DownloadLinkURL = "https://www.nvidia.com/drivers",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                Comments = ""
            };
            SysRequirements.oSysRequirements.Add(oSysReq10);

            var oSysReq11 = new SysRequirement()
            {
                ID = 11,
                DisplayName = "GPU-Z Tool",
                DownloadLinkText = "Download",
                DownloadLinkURL = "https://www.techpowerup.com/downloads/SysInfo/GPU-Z/",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                Comments = ""
            };
            SysRequirements.oSysRequirements.Add(oSysReq11);

            var oSysReq12 = new SysRequirement()
            {
                ID = 12,
                DisplayName = "ITK Snap",
                DownloadLinkText = "Download",
                DownloadLinkURL = "http://www.itksnap.org/pmwiki/pmwiki.php?n=Downloads.SNAP3",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                Comments = ""
            };
            SysRequirements.oSysRequirements.Add(oSysReq12);

            var oSysReq13 = new SysRequirement()
            {
                ID = 13,
                DisplayName = "Google Earth",
                DownloadLinkText = "Download",
                DownloadLinkURL = "https://www.google.com/intl/en_in/earth/versions/",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                Comments = ""
            };
            SysRequirements.oSysRequirements.Add(oSysReq13);

            var oSysReq14 = new SysRequirement()
            {
                ID = 14,
                DisplayName = "3D XML Player",
                DownloadLinkText = "Download",
                //DownloadLinkURL = "http://www.3ds.com/products-services/3d-xml/1",
                DownloadLinkURL = "https://www.3ds.com/products-services/3d-xml/downloads/",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                Comments = ""
            };
            SysRequirements.oSysRequirements.Add(oSysReq14);

            var oSysReq15 = new SysRequirement()
            {
                ID = 15,
                DisplayName = "3D Connexion",
                DownloadLinkText = "Download",
                DownloadLinkURL = "http://www.3dconnexion.eu/products/spacemouse.html",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                Comments = ""
            };
            SysRequirements.oSysRequirements.Add(oSysReq15);

            /*var oSysReq16 = new SysRequirement()
            {
                ID = 16,
                DisplayName = "Unigine",
                DownloadLinkText = "Download",
                DownloadLinkURL = "https://assets.unigine.com/d/Unigine_Valley-1.0.exe",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                Comments = ""
            };
            SysRequirements.oSysRequirements.Add(oSysReq16);*/


           /* var oSysReq17 = new SysRequirement()
            {
                ID = 17,
                DisplayName = "Audacity",
                DownloadLinkText = "Download",
                DownloadLinkURL = "http://www.audacityteam.org/download/",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_PREMIUM,
                Comments = ""
            };
            SysRequirements.oSysRequirements.Add(oSysReq17);*/

            var oSysReq18 = new SysRequirement()
            {
                ID = 18,
                DisplayName = "Microsoft Teams",
                DownloadLinkText = "Download",
                DownloadLinkURL = "https://www.microsoft.com/en-in/microsoft-teams/download-app#desktopAppDownloadregion/",
                IsLicensed = false,
                IsInstalled = false,
                Status = Constants.PREREQUISITE_STATUS_NOT_CHECKED,
                Category = Constants.CATEGORY_NAME_HDX_PREMIUM,
                Comments = ""
            };
            SysRequirements.oSysRequirements.Add(oSysReq18);



        }
        #endregion

        #region - Check Devmanager is Open
        public static void killDevmgr()
        {
            try
            {
                Process[] currentProcess = Process.GetProcessesByName("mmc");
                if (currentProcess != null)
                {
                    foreach (Process ps in currentProcess)
                    {
                        if (ps != null)
                        {
                            ps.Kill();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region - Load System Informations
        public static void LoadSystemInformations()
        {
            string OSName = string.Empty;
            string OSArchitecture = string.Empty;
            string OSServicePack = string.Empty;
            string CPUName = string.Empty;
            string CPUFamily = string.Empty;
            string RAM = string.Empty;
            string GraphicCard = string.Empty;
            string HDXMonitorToolAvailable = string.Empty;

            ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem");
            ManagementObjectSearcher processors = new ManagementObjectSearcher("SELECT * FROM Win32_Processor");
            ManagementObjectSearcher mems = new ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMemory");
            ManagementObjectSearcher vids = new ManagementObjectSearcher("SELECT * FROM CIM_VideoController");

            HDXMonitorToolAvailable = "No";

            SysInformations.oSysInformations.Clear();

            try
            {
                ManagementObjectSearcher ctxFPS = new ManagementObjectSearcher(@"root\citrix\hdx", @"SELECT * FROM " + Constants.FPS_CLASS_NAME);
                if (ctxFPS.Get().Count > 0)
                {
                    HDXMonitorToolAvailable = "Yes";
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Invalid namespace"))
                {
                    HDXMonitorToolAvailable = "No";
                }
            }
            try
            {

                foreach (ManagementObject mo in mos.Get())
                {
                    if (mo["Caption"] != null) OSName = Convert.ToString(mo["Caption"]);
                    if (mo["OSArchitecture"] != null) OSArchitecture = Convert.ToString(mo["OSArchitecture"]);
                    if (mo["CSDVersion"] != null) OSServicePack = Convert.ToString(mo["CSDVersion"]);
                }

                if (OSServicePack != "")
                {
                    OSName = OSName + "(" + OSServicePack + ")";
                }

                foreach (ManagementObject mo in processors.Get())
                {
                    if (mo["Name"] != null) CPUName = Convert.ToString(mo["Name"]);
                    if (mo["Caption"] != null) CPUFamily = Convert.ToString(mo["Caption"]);
                }

                Double countRAM = 0;
                foreach (ManagementObject mo in mems.Get())
                {
                    if (mo["Capacity"] != null)
                    {
                        countRAM += Convert.ToUInt64(mo["Capacity"]);
                    }
                }
                RAM = String.Format("{0:0.00} GB", Math.Round(countRAM / (1024 * 1024 * 1024), 2));

                foreach (ManagementObject mo in vids.Get())
                {
                    if (mo["Caption"] != null)
                    {
                        GraphicCard = Convert.ToString(mo["Caption"]);
                        //Globals.InstalledPrograms.Add(GraphicCard.ToLower());
                    }

                }

                var oSysInfo1 = new SysInformation()
                {
                    ID = 1,
                    ConfigLabel = "Operating System: ",
                    ConfigValue = OSName.ToString(),
                    Comments = ""
                };
                SysInformations.oSysInformations.Add(oSysInfo1);

                var oSysInfo2 = new SysInformation()
                {
                    ID = 2,
                    ConfigLabel = "Architecture/Platform: ",
                    ConfigValue = OSArchitecture.ToString(),
                    Comments = ""
                };
                SysInformations.oSysInformations.Add(oSysInfo2);

                var oSysInfo3 = new SysInformation()
                {
                    ID = 3,
                    ConfigLabel = "Service Pack: ",
                    ConfigValue = (OSServicePack == "" ? "0" : OSServicePack.ToString()),
                    Comments = ""
                };
                SysInformations.oSysInformations.Add(oSysInfo3);

                var oSysInfo4 = new SysInformation()
                {
                    ID = 4,
                    ConfigLabel = "CPU Name: ",
                    ConfigValue = CPUName.ToString(),
                    Comments = ""
                };
                SysInformations.oSysInformations.Add(oSysInfo4);

                var oSysInfo5 = new SysInformation()
                {
                    ID = 5,
                    ConfigLabel = "CPU Family: ",
                    ConfigValue = CPUFamily.ToString(),
                    Comments = ""
                };
                SysInformations.oSysInformations.Add(oSysInfo5);

                var oSysInfo6 = new SysInformation()
                {
                    ID = 6,
                    ConfigLabel = "Total RAM: ",
                    ConfigValue = RAM.ToString(),
                    Comments = ""
                };
                SysInformations.oSysInformations.Add(oSysInfo6);

                var oSysInfo7 = new SysInformation()
                {
                    ID = 7,
                    ConfigLabel = "Graphics Card: ",
                    ConfigValue = GraphicCard.ToString(),
                    Comments = ""
                };
                SysInformations.oSysInformations.Add(oSysInfo7);



                var oSysInfo8 = new SysInformation()
                {
                    ID = 8,
                    ConfigLabel = "HDX Monitor Tool Available: ",
                    ConfigValue = HDXMonitorToolAvailable.ToString(),
                    Comments = ""
                };
                SysInformations.oSysInformations.Add(oSysInfo8);

                var oSysInfo13 = new SysInformation()
                {
                    ID = 13,
                    ConfigLabel = "Thin Client Verification Tool Version: ",
                    ConfigValue = Constants.Version_NUMBER,
                    Comments = ""
                };
                SysInformations.oSysInformations.Add(oSysInfo13);

                GetVDAversion();

                var oSysInfo14 = new SysInformation()
                {
                    ID = 14,
                    ConfigLabel = "VDA : ",
                    ConfigValue = Constants.VDAversion,
                    Comments = ""
                };
                SysInformations.oSysInformations.Add(oSysInfo14);

                //Log the Client System Details
                if (HDXMonitorToolAvailable == "Yes")
                {
                    string ClientName = "";
                    string ClientIPAddress = "";
                    string ClientPlatform = "";
                    string CitrixReceiverVersion = "";
                    string ClientVDAversion = "";

                    ManagementObjectSearcher ctxClientInfo = new ManagementObjectSearcher(@"root\Citrix\hdx", @"SELECT * FROM Citrix_Client_Enum");
                    foreach (ManagementObject mo in ctxClientInfo.Get())
                    {
                        {
                            if (mo["Name"] != null) ClientName = Convert.ToString(mo["Name"]);
                            if (mo["Address"] != null) ClientIPAddress = Convert.ToString(mo["Address"]);
                            if (mo["ProductId"] != null)
                            {
                                ClientPlatform = Convert.ToString(mo["ProductId"]);
                                Globals.ClientPlatformId = int.Parse(ClientPlatform).ToString("X");
                            }
                            if (mo["Version"] != null) CitrixReceiverVersion = Convert.ToString(mo["Version"]);
                        }
                    }

                    var oSysInfo9 = new SysInformation()
                    {
                        ID = 9,
                        ConfigLabel = "Client Name: ",
                        ConfigValue = ClientName.ToString(),
                        Comments = ""
                    };
                    SysInformations.oSysInformations.Add(oSysInfo9);

                    var oSysInfo10 = new SysInformation()
                    {
                        ID = 10,
                        ConfigLabel = "Client IP Address: ",
                        ConfigValue = ClientIPAddress.ToString(),
                        Comments = ""
                    };
                    SysInformations.oSysInformations.Add(oSysInfo10);

                    var oSysInfo11 = new SysInformation()
                    {
                        ID = 11,
                        ConfigLabel = "Client Platform: ",
                        ConfigValue = Globals.GetClientPlatform().Where(x => x.Id == Globals.ClientPlatformId).FirstOrDefault().Name,
                        Comments = ""
                    };
                    SysInformations.oSysInformations.Add(oSysInfo11);

                    var oSysInfo12 = new SysInformation()
                    {
                        ID = 12,
                        ConfigLabel = "Citrix Receiver Version: ",
                        ConfigValue = CitrixReceiverVersion.ToString(),
                        Comments = ""
                    };
                    SysInformations.oSysInformations.Add(oSysInfo12);

                    // String zz="",y="";
                    // ManagementObjectSearcher ctxClientInfo1 = new ManagementObjectSearcher(@"root\Citrix\hdx", @"SELECT * FROM Citrix_VirtualChannel_CDM_Enum");
                    // foreach (ManagementObject mo in ctxClientInfo1.Get())
                    //{
                    // {
                    //  if (mo["Component_VolumeLabel"] != null) zz = Convert.ToString(mo["Component_VolumeLabel"]);
                    // if (mo["Component_DriveType"] != null) y = Convert.ToString(mo["Component_DriveType"]);
                    //          }
                    // }

                }


            }
            catch (NullReferenceException ex)
            {
                StreamWriter logtxt = new StreamWriter(Constants.LOG_FILE_NAME, true);
                logtxt.WriteLine("null reference exception:" + ex.Message);
                if (ex.InnerException != null)
                {
                    logtxt.WriteLine("inner exception : " + ex.InnerException.Message);

                }
                logtxt.WriteLine(ex.StackTrace);
                logtxt.Close();
            }
            catch (Exception ex1)
            {
                StreamWriter logtxt = new StreamWriter(Constants.LOG_FILE_NAME, true);
                logtxt.WriteLine("exception :" + ex1.Message);
                if (ex1.InnerException != null)
                {
                    logtxt.WriteLine("exception : " + ex1.InnerException.Message);

                }
                logtxt.WriteLine(ex1.StackTrace);
                logtxt.Close();

            }




        }

        public static void GetExecutablePath(string softwareName)
        {
            switch (softwareName)
            {
                case "Google Earth": getGoogleEarthExe();
                    break;
                case "ITK-SNAP": getITKSnapExe();
                    break;

            }
        }

        private static void getITKSnapExe()
        {
            string REG_UNINSTALL_LOCALMACHINE = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            string REG_UNINSTALL_LOCALMACHINE_64 = @"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall";

            string value = "";



            #region - 32bit Registry values
            using (RegistryKey regKey = Registry.LocalMachine.OpenSubKey(REG_UNINSTALL_LOCALMACHINE))
            {
                foreach (string subKeyName in regKey.GetSubKeyNames())
                {
                    using (RegistryKey subKey = regKey.OpenSubKey(subKeyName))
                    {
                        try
                        {
                            if (subKey != null)
                            {


                                if (subKey.GetValue("DisplayName") != null)
                                {
                                    String displayname = subKey.GetValue("DisplayName") as string;

                                    if (displayname.ToLower().Contains(Constants.SOFTWARE_NAME_ITK_SNAP.ToLower()))
                                    {
                                        if (subKey.GetValue("UninstallString") != null)
                                        {
                                            String uninstallString = subKey.GetValue("UninstallString") as string;
                                            String directoryPath = Path.GetDirectoryName(uninstallString);
                                            String exePath = directoryPath + "\\ITK-Snap.exe";

                                            if (System.IO.File.Exists(exePath))
                                            {
                                                Constants.APP_EXECUTABLE_ITKSNAP = exePath;
                                            }
                                            else if (System.IO.File.Exists(directoryPath + "\\bin\\ITK-Snap.exe"))
                                            {
                                                Constants.APP_EXECUTABLE_ITKSNAP = directoryPath + "\\bin\\ITK-Snap.exe" ;

                                            }
                                        }

                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //Some registry keys contain null values 
                            throw (ex);

                        }
                    }
                }
            }
            #endregion - 32 bit

            #region - 64BIT Registry values

            RegistryKey regularx64View = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
            using (RegistryKey regKey = regularx64View.OpenSubKey(REG_UNINSTALL_LOCALMACHINE, RegistryKeyPermissionCheck.ReadSubTree))
            {
                foreach (string subKeyName in regKey.GetSubKeyNames())
                {
                    using (RegistryKey subKey = regKey.OpenSubKey(subKeyName))
                    {
                        try
                        {
                            if (subKey != null)
                            {

                                if (subKey.GetValue("DisplayName") != null)
                                {
                                    String displayname = subKey.GetValue("DisplayName") as string;

                                    if (displayname.ToLower().Contains(Constants.SOFTWARE_NAME_ITK_SNAP.ToLower()))
                                    {
                                        if (subKey.GetValue("UninstallString") != null)
                                        {
                                            String uninstallString = subKey.GetValue("UninstallString") as string;
                                            String directoryPath = Path.GetDirectoryName(uninstallString);
                                            String exePath = directoryPath + "\\ITK-Snap.exe";

                                            if (System.IO.File.Exists(exePath))
                                            {
                                                Constants.APP_EXECUTABLE_ITKSNAP = exePath;
                                            }
                                            else if (System.IO.File.Exists(directoryPath + "\\bin\\ITK-Snap.exe"))
                                            {
                                                Constants.APP_EXECUTABLE_ITKSNAP= directoryPath + "\\bin\\ITK-Snap.exe";

                                            }
                                        }

                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //Some registry keys contain null values 
                            Console.WriteLine(ex.Message);
                        }
                    }
                }
            }
            #endregion - 64 BIT

        }

        private static void getGoogleEarthExe()
        {
            Regex pathArgumentsRegex = new Regex(@"(%\d+)|(""%\d+"")", RegexOptions.ExplicitCapture);
            Regex pathArgumentsRegex2 = new Regex("\"?", RegexOptions.ExplicitCapture);
            Regex pathArgumentsRegex3 = new Regex("\\?", RegexOptions.ExplicitCapture);
            RegistryKey commandKey = Registry.ClassesRoot.OpenSubKey(@"GoogleEarth.kmlfile" + @"\shell\open\command");
            String exePath = "";
            if (commandKey != null)
            {
                object command = commandKey.GetValue(string.Empty);
                if (command != null)
                {
                    exePath = pathArgumentsRegex3.Replace(pathArgumentsRegex2.Replace(pathArgumentsRegex.Replace(command.ToString(), ""), ""), "", 1);

                }
            }
            if (!exePath.Equals("")) Constants.APP_EXECUTABLE_GOOGLEEARTH = exePath;
            Console.Write(exePath);
        }

        public static void GetVDAversion()
        {
            #region - 32bit Registry values
            using (RegistryKey regKey = Registry.LocalMachine.OpenSubKey(Constants.REG_UNINSTALL_LOCALMACHINE))
            {
                foreach (string subKeyName in regKey.GetSubKeyNames())
                {
                    using (RegistryKey subKey = regKey.OpenSubKey(subKeyName))
                    {
                        try
                        {
                            if (subKey != null)
                            {
                                String displayname = subKey.GetValue("DisplayName") as string;
                                if (subKey.GetValue("DisplayName") != null)
                                {
                                    if (displayname.ToLower().Contains(Constants.SOFTWARE_NAME_VDA.ToLower()))
                                    {
                                        //Globals.InstalledPrograms1.Add(subKey.GetValue("DisplayName").ToString().ToLower());
                                        Constants.VDAversion = subKey.GetValue("DisplayName") as string + " , VER. " + subKey.GetValue("DisplayVersion") as string;
                                        // Constants.VDAdisplayName = subKey.GetValue("DisplayName") as string;
                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //Some registry keys contain null values 
                            throw (ex);
                        }
                    }
                }
            }
            #endregion - 32 bit

            #region - 64BIT Registry values

            RegistryKey regularx64View = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
            using (RegistryKey regKey = regularx64View.OpenSubKey(Constants.REG_UNINSTALL_LOCALMACHINE, RegistryKeyPermissionCheck.ReadSubTree))
            {
                foreach (string subKeyName in regKey.GetSubKeyNames())
                {
                    using (RegistryKey subKey = regKey.OpenSubKey(subKeyName))
                    {
                        try
                        {
                            if (subKey != null)
                            {
                                String displayname = subKey.GetValue("DisplayName") as string;
                                if (subKey.GetValue("DisplayName") != null && displayname.ToLower().Contains(Constants.SOFTWARE_NAME_VDA.ToLower()))
                                {
                                    //Globals.InstalledPrograms1.Add(subKey.GetValue("DisplayName").ToString().ToLower());
                                    Constants.VDAversion = displayname + " , VER. " + subKey.GetValue("DisplayVersion") as string;
                                    // Constants.VDAdisplayName = displayname;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //Some registry keys contain null values 
                            Console.WriteLine(ex.Message);
                        }
                    }
                }
            }
            #endregion - 64 BIT

            if (Constants.VDAversion.Equals("")) { Constants.VDAversion = "VDA Version not found"; }
        }

        #region - Load Test Case Details
        public static void LoadTestCaseDetails()
        {
            if (TestCases.oTestCases.Count == 23)
            {
                Debug.WriteLine("Test Cases at already initialized - " + DateTime.Now.ToString());
                return;
            }

            Debug.WriteLine("Initializing the list of Test Cases - " + DateTime.Now.ToString());

            TestCases.oTestCases.Clear();

            #region - HDX Ready Test Case Details 

            var oTestCase1 = new TestCase()
            {
                ID = 1,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                ScenarioTitle = Constants.TESTCASE01_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase1);

            var oTestCase2 = new TestCase()
            {
                ID = 2,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                ScenarioTitle = Constants.TESTCASE02_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase2);

            var oTestCase3 = new TestCase()
            {
                ID = 3,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                ScenarioTitle = Constants.TESTCASE03_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase3);

            /*        var oTestCase4 = new TestCase()
                    {
                        ID = 4,
                        Category = Constants.CATEGORY_NAME_HDX_READY,
                        ScenarioTitle = Constants.TESTCASE04_NAME,
                        Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                        Result = "",
                        Comments = "",
                        DownloadLink = ""
                    };
                    TestCases.oTestCases.Add(oTestCase4);*/

            var oTestCase4 = new TestCase()
            {
                ID = 4,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                ScenarioTitle = Constants.TESTCASE04_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase4);

            var oTestCase5 = new TestCase()
            {
                ID = 5,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                ScenarioTitle = Constants.TESTCASE05_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase5);



            var oTestCase6 = new TestCase()
            {
                ID = 6,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                ScenarioTitle = Constants.TESTCASE06_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase6);

            var oTestCase7 = new TestCase()
            {
                ID = 7,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                ScenarioTitle = Constants.TESTCASE07_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = Constants.VideoDownloadLink

            };
            TestCases.oTestCases.Add(oTestCase7);

            var oTestCase8 = new TestCase()
            {
                ID = 8,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                ScenarioTitle = Constants.TESTCASE08_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase8);

            var oTestCase9 = new TestCase()
            {
                ID = 9,
                Category = Constants.CATEGORY_NAME_HDX_READY,
                ScenarioTitle = Constants.TESTCASE09_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase9);

            #endregion

            #region - HDX Standard 3D Test Case Details 

            var oTestCase10 = new TestCase()
            {
                ID = 10,
                Category = Constants.CATEGORY_NAME_HDX_3DPRO,
                ScenarioTitle = Constants.TESTCASE10_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase10);

            var oTestCase11 = new TestCase()
            {
                ID = 11,
                Category = Constants.CATEGORY_NAME_HDX_3DPRO,
                ScenarioTitle = Constants.TESTCASE11_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase11);

            var oTestCase12 = new TestCase()
            {
                ID = 12,
                Category = Constants.CATEGORY_NAME_HDX_3DPRO,
                ScenarioTitle = Constants.TESTCASE12_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase12);

            var oTestCase13 = new TestCase()
            {
                ID = 13,
                Category = Constants.CATEGORY_NAME_HDX_3DPRO,
                ScenarioTitle = Constants.TESTCASE13_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase13);

            var oTestCase14 = new TestCase()
            {
                ID = 14,
                Category = Constants.CATEGORY_NAME_HDX_3DPRO,
                ScenarioTitle = Constants.TESTCASE14_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase14);

            var oTestCase15 = new TestCase()
            {
                ID = 15,
                Category = Constants.CATEGORY_NAME_HDX_3DPRO,
                ScenarioTitle = Constants.TESTCASE15_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase15);

            var oTestCase21 = new TestCase()
            {
                ID = 16,
                Category = Constants.CATEGORY_NAME_HDX_PREMIUM,
                ScenarioTitle = Constants.TESTCASE21_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase21);


            #endregion

            #region - HDX Premium Test Case Details 


            var oTestCase16 = new TestCase()
            {
                ID = 17,
                Category = Constants.CATEGORY_NAME_HDX_3DPRO,
                ScenarioTitle = Constants.TESTCASE16_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase16);

            var oTestCase17 = new TestCase()
            {
                ID = 18,//17
                Category = Constants.CATEGORY_NAME_HDX_PREMIUM,
                ScenarioTitle = Constants.TESTCASE18_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase17);

            /*var oTestCase18 = new TestCase()
            {
                ID = 19,//18
                Category = Constants.CATEGORY_NAME_HDX_PREMIUM,
                ScenarioTitle = Constants.TESTCASE18_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase18);*/

            var oTestCase19 = new TestCase()
            {
                ID = 19,
                Category = Constants.CATEGORY_NAME_HDX_PREMIUM,
                ScenarioTitle = Constants.TESTCASE19_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase19);

            var oTestCase20 = new TestCase()
            {
                ID = 20,
                Category = Constants.CATEGORY_NAME_HDX_PREMIUM,
                ScenarioTitle = Constants.TESTCASE20_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase20);


            /*var oTestCase21 = new TestCase()
            {
                ID = 21,
                Category = Constants.CATEGORY_NAME_HDX_PREMIUM,
                ScenarioTitle = Constants.TESTCASE21_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase21);*/

            var oTestCase24 = new TestCase()
            {
                ID = 21,
                Category = Constants.CATEGORY_NAME_HDX_PREMIUM_Contd,
                ScenarioTitle = Constants.TESTCASE24_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase24);


            var oTestCase22 = new TestCase()
            {
                ID = 22,
                Category = Constants.CATEGORY_NAME_HDX_PREMIUM,
                ScenarioTitle = Constants.TESTCASE22_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase22);


            var oTestCase23 = new TestCase()
            {
                ID = 23,
                Category = Constants.CATEGORY_NAME_HDX_PREMIUM,
                ScenarioTitle = Constants.TESTCASE23_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase23);

           /* var oTestCase24 = new TestCase()
            {
                ID = 24,
                Category = Constants.CATEGORY_NAME_HDX_PREMIUM_Contd,
                ScenarioTitle = Constants.TESTCASE24_NAME,
                Status = Constants.VERIFICATION_STATUS_NOT_CHECKED,
                Result = "",
                Comments = "",
                DownloadLink = ""
            };
            TestCases.oTestCases.Add(oTestCase24);*/

            #endregion
        }
        #endregion

        #region - Windows Service Related Functions  
        public static bool CheckWindowsService(string serviceName)
        {
            bool returnValue = false;
            ServiceController[] services = ServiceController.GetServices();
            var service = services.FirstOrDefault(s => s.ServiceName == serviceName);
            if (service == null)
            {
                returnValue = false;
            }
            {
                returnValue = true;

                if (service.Status != ServiceControllerStatus.Running)
                {
                    returnValue = false;
                    //TimeSpan timeout = TimeSpan.FromMilliseconds(5000);
                    //service.Start();
                    //service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                    //returnValue = true;
                }
            }
            return returnValue;
        }

        #endregion 

        #region - Get Management Object Value 
        public static IEnumerable<object> GetManagementObject(string win32ClassName, string property)
        {
            return (from x in new ManagementObjectSearcher("SELECT * FROM " + win32ClassName).Get().OfType<ManagementObject>()
                    select x.GetPropertyValue(property));
        }
        #endregion 

        #region - Calculate FPS 
        public static string CalculateFPS(int TestCaseID, string Scenario, int fpsInterval, int fpsTimeout, CancellationToken ct)
        {

            StringBuilder sbFPSOutput = new StringBuilder("");
            StreamWriter fileFPSLogScenario = new StreamWriter(Constants.LOG_FILE_NAME, true);
            sbFPSOutput.AppendLine("FPS Calculation for Scenario # " + TestCaseID.ToString() + " - " + Scenario);
            fileFPSLogScenario.WriteLine("FPS Calculation for Scenario # " + TestCaseID.ToString() + " - " + Scenario);
            fileFPSLogScenario.Close();

            int fpsAverage = 0;
            int fpsCount = (int)(fpsTimeout / fpsInterval);
            int fpsValueSum = 0;

            try
            {
                int fpsCurrent = 0;
                StreamWriter fileFPSLogFPSCount = new StreamWriter(Constants.LOG_FILE_NAME, true);
                fileFPSLogFPSCount.WriteLine(Scenario + " fpsCount = " + fpsCount.ToString());
                fileFPSLogFPSCount.Close();
                System.Diagnostics.Stopwatch watcher = new System.Diagnostics.Stopwatch();
                for (int fpsIndex = 0; fpsIndex < fpsCount; fpsIndex++)
                {
                    string CurrentFPSInfo = "";

                    ManagementObjectSearcher ctxFPS = new ManagementObjectSearcher(@"root\Citrix\hdx", @"SELECT * FROM " + Constants.FPS_CLASS_NAME);
                    if (ctxFPS.Get().Count > 0)
                    {
                        foreach (ManagementObject mo in ctxFPS.Get())
                        {
                            if (mo[Constants.FPS_PARAMETER_NAME] != null)
                            {
                                fpsCurrent = Convert.ToInt32(mo[Constants.FPS_PARAMETER_NAME]);
                                fpsCurrent = Convert.ToInt32(mo[Constants.FPS_PARAMETER_NAME]);
                                CurrentFPSInfo = "FPS recording for " + Scenario + " at " + DateTime.Now.ToString() + ": " + Convert.ToString(fpsCurrent);
                                fpsValueSum = fpsValueSum + fpsCurrent;
                                break;
                            }
                        }
                        sbFPSOutput.AppendLine(CurrentFPSInfo);
                        StreamWriter fileFPSLog = new StreamWriter(Constants.LOG_FILE_NAME, true);
                        fileFPSLog.WriteLine(CurrentFPSInfo);
                        fileFPSLog.Close();
                        Thread.Sleep(Constants.FPS_SLEEP);

                        if (ct.IsCancellationRequested == true)
                        {
                            fileFPSLog.WriteLine("FPS calculation stopped.");
                            fileFPSLog.Close();
                            ct.ThrowIfCancellationRequested();
                        }
                    }
                    else
                    {
                        sbFPSOutput.AppendLine("HDX Monitor Tool has not been installed properly");
                    }
                }

                fpsAverage = fpsValueSum / fpsCount;

                sbFPSOutput.AppendLine("Average FPS - " + Scenario + ": " + fpsAverage.ToString());
                StreamWriter fileFPSLogClose = new StreamWriter(Constants.LOG_FILE_NAME, true);
                fileFPSLogClose.WriteLine("Average FPS: " + Scenario + ": " + fpsAverage.ToString());
                fileFPSLogClose.Close();
            }

            catch (Exception ex)
            {
                fpsAverage = fpsValueSum / fpsCount;

                sbFPSOutput.AppendLine("Average FPS - " + Scenario + ": " + fpsAverage.ToString());
                StreamWriter fileFPSLogError = new StreamWriter(Constants.LOG_FILE_NAME, true);
                fileFPSLogError.WriteLine("ERROR: An error has occured in CalculateFPS - " + Scenario + " - " + ex.Message);
                fileFPSLogError.Close();
                sbFPSOutput.AppendLine("ERROR: An error has occured in CalculateFPS - " + Scenario + " - " + ex.Message);
            }
            fileFPSLogScenario.Close();
            Globals.IsFPSTaskCompleted = true;
            return sbFPSOutput.ToString();
        }

        #endregion

        #region - get Value H264 
        public static string getValueH264(int TestCaseID, string Scenario)
        {

            StringBuilder sbFPSOutput = new StringBuilder("");
            StreamWriter fileFPSLogScenario = new StreamWriter(Constants.LOG_FILE_NAME, true);
            sbFPSOutput.AppendLine("VideoCodecTypeCurrent H264 Calculation for Scenario # " + TestCaseID.ToString() + " - " + Scenario);
            fileFPSLogScenario.WriteLine("FPS Calculation for Scenario # " + TestCaseID.ToString() + " - " + Scenario);
            fileFPSLogScenario.Close();

            try
            {
                object fpsCurrent = null;
                System.Diagnostics.Stopwatch watcher = new System.Diagnostics.Stopwatch();
                for (int fpsIndex = 0; fpsIndex < 2; fpsIndex++)
                {
                    string CurrentFPSInfo = "";

                    ManagementObjectSearcher ctxFPS = new ManagementObjectSearcher(@"root\Citrix\hdx", @"SELECT * FROM " + Constants.FPS_CLASS_NAME);
                    if (ctxFPS.Get().Count > 0)
                    {
                        foreach (ManagementObject mo in ctxFPS.Get())
                        {
                            if (mo[Constants.FPS_PARAMETER_H264] != null)
                            {
                                fpsCurrent = mo[Constants.FPS_PARAMETER_H264];
                                fpsCurrent = mo[Constants.FPS_PARAMETER_H264];
                                CurrentFPSInfo = "Component_Monitor_VideoCodecTypeCurrent for " + Scenario + " at " + DateTime.Now.ToString() + ": " + Convert.ToString(fpsCurrent);
                                
                                break;
                            }
                        }
                        sbFPSOutput.AppendLine(CurrentFPSInfo);
                        StreamWriter fileFPSLog = new StreamWriter(Constants.LOG_FILE_NAME, true);
                        fileFPSLog.WriteLine(CurrentFPSInfo);
                        fileFPSLog.Close();
                        Thread.Sleep(Constants.FPS_SLEEP);
                    }
                    else
                    {
                        sbFPSOutput.AppendLine("HDX Monitor Tool has not been installed properly");
                    }
                }

               

                sbFPSOutput.AppendLine("HDX Monitor -> Component_Monitor_VideoCodecTypeCurrent - " + Scenario + ": " + fpsCurrent.ToString());
                StreamWriter fileFPSLogClose = new StreamWriter(Constants.LOG_FILE_NAME, true);
                fileFPSLogClose.WriteLine("HDX Monitor -> Component_Monitor_VideoCodecTypeCurrent - " + Scenario + ": " + fpsCurrent.ToString());
                fileFPSLogClose.Close();
            }

            catch (Exception ex)
            {
                sbFPSOutput.AppendLine("ERROR : An error has occured in HDX Monitor -> Component_Monitor_VideoCodecTypeCurrent -" + Scenario);
                StreamWriter fileFPSLogError = new StreamWriter(Constants.LOG_FILE_NAME, true);
                fileFPSLogError.WriteLine("ERROR: An error has occured in Component_Monitor_VideoCodecTypeCurrent - " + Scenario + " - " + ex.Message);
                fileFPSLogError.Close();
                sbFPSOutput.AppendLine("ERROR: An error has occured in Component_Monitor_VideoCodecTypeCurrent - " + Scenario + " - " + ex.Message);
            }
            fileFPSLogScenario.Close();
            Globals.IsFPSTaskCompleted = true;
            return sbFPSOutput.ToString();
        }

        #endregion

        #region - Get details of Installed Programs under Registry Local Machine Hive 
        public static void GetInstalledProgramsFromLocalMachine()
        {
            try
            {
                #region - 32bit Registry values
                using (RegistryKey regKey = Registry.LocalMachine.OpenSubKey(Constants.REG_UNINSTALL_LOCALMACHINE))
                {
                    if (regKey != null)
                    {
                        foreach (string subKeyName in regKey.GetSubKeyNames())
                        {

                            using (RegistryKey subKey = regKey.OpenSubKey(subKeyName))
                            {
                                if (subKey != null)
                                {
                                    string subkeyDisplayName = Convert.ToString(subKey.GetValue("DisplayName"));
                                    if (!String.IsNullOrEmpty(subkeyDisplayName))
                                        Globals.InstalledPrograms.Add(subkeyDisplayName.ToLower());



                                }
                            }

                        }
                    }
                }
                #endregion - 32 bit

                #region - 64BIT Registry values

                RegistryKey regularx64View = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                using (RegistryKey regKey = regularx64View.OpenSubKey(Constants.REG_UNINSTALL_LOCALMACHINE, RegistryKeyPermissionCheck.ReadSubTree))
                {
                    if (regKey != null)
                    {
                        foreach (string subKeyName in regKey.GetSubKeyNames())
                        {
                            using (RegistryKey subKey = regKey.OpenSubKey(subKeyName))
                            {
                                if (subKey != null)
                                {
                                    string subkeyDisplayName = Convert.ToString(subKey.GetValue("DisplayName"));

                                    if (!String.IsNullOrEmpty(subkeyDisplayName))
                                        Globals.InstalledPrograms.Add(subkeyDisplayName.ToLower());

                                }
                            }
                        }
                    }
                }
                #endregion - 64 BIT
            }
            catch (Exception ex)
            {
                //Some registry keys contain null values 
                throw ex;
            }
        }
        #endregion 

        #region - Get details of Installed Programs under Registry Current User Hive 
        public static void GetInstalledProgramsFromCurrentUser()
        {
            //List<string> temp = new List<string>();
            try
            {
                using (RegistryKey regKey = Registry.CurrentUser.OpenSubKey(Constants.REG_UNINSTALL_LOCALMACHINE))
                {
                    if (regKey != null)
                    {
                        foreach (string subKeyName in regKey.GetSubKeyNames())
                        {
                            using (RegistryKey subKey = regKey.OpenSubKey(subKeyName))
                            {
                                if (subKey != null)
                                {
                                    string subkeyDisplayName = Convert.ToString(subKey.GetValue("DisplayName"));
                                    if (!String.IsNullOrEmpty(subkeyDisplayName))
                                        Globals.InstalledPrograms.Add(subkeyDisplayName.ToLower());

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Some registry keys are NULL
                throw (ex);
            }
        }
        #endregion 

        #region - Get details of Installed Programs which don't appear in Control Panel Add/Remove Programs
        public static void GetInstalledProgramsOutsideUninstallPrograms(string softwareName)
        {
            string uninstallKeyLocation = "";
            try
            {
                if (softwareName.ToLower() == Constants.SOFTWARE_NAME_NVIDIA_DRIVER.ToLower())
                {
                    ManagementObjectSearcher vids = new ManagementObjectSearcher("SELECT * FROM CIM_VideoController");
                    foreach (ManagementObject mo in vids.Get())
                    {
                        if (mo["Caption"] != null)
                        {
                            Globals.InstalledPrograms.Add(Convert.ToString(mo["Caption"]).ToLower());
                        }
                    }
                }

                if (softwareName.ToLower() == Constants.SOFTWARE_NAME_WINDOWS_MEDIA_PLAYER.ToLower())
                {
                    uninstallKeyLocation = @"SOFTWARE\Microsoft\MediaPlayer";
                    RegistryKey rk = Registry.LocalMachine.OpenSubKey(uninstallKeyLocation);
                    if (rk != null)
                    {
                        if (rk.GetValue("Installation Directory") != null)
                        {
                            Globals.InstalledPrograms.Add(softwareName.ToLower());
                        }
                    }
                }

                if (softwareName.ToLower() == Constants.SOFTWARE_NAME_LYNC.ToLower())
                {
                    RegistryKey rk = Registry.ClassesRoot.OpenSubKey("Applications");
                    RegistryKey rkLync = rk.OpenSubKey("lync.exe");

                    if (rkLync != null)
                    {
                        if (rkLync.ValueCount > 0)
                        {
                            Globals.InstalledPrograms.Add(softwareName.ToLower());
                        }
                    }
                }

                if (softwareName.ToLower() == Constants.SOFTWARE_NAME_MICROSOFT_WORD.ToLower())
                {
                    RegistryKey doc = Registry.ClassesRoot.OpenSubKey(".doc");
                    string docv = (doc == null) ? "" : Convert.ToString(doc.GetValue("Content Type"));

                    if (docv != "")
                    {
                        Globals.InstalledPrograms.Add(softwareName.ToLower());
                    }
                }

                if (softwareName.ToLower() == Constants.SOFTWARE_NAME_MICROSOFT_POWERPOINT.ToLower())
                {
                    RegistryKey ppt = Registry.ClassesRoot.OpenSubKey(".ppt");
                    string pptv = (ppt == null) ? "" : Convert.ToString(ppt.GetValue("Content Type"));

                    if (pptv != "")
                    {
                        Globals.InstalledPrograms.Add(softwareName.ToLower());
                    }
                }

                if (softwareName.ToLower() == Constants.SOFTWARE_NAME_ADOBE_FLASH_PLUGIN.ToLower())
                {
                    if (Convert.ToString((new WebBrowser().Version)).Substring(0, 2) == "11")
                    {
                        Globals.InstalledPrograms.Add(softwareName.ToLower());
                    }
                    else
                    {
                        string uninstallKey32bit = @"SOFTWARE\Macromedia\FlashPlayerPlugin";
                        string uninstallKey64bit = @"SOFTWARE\WOW6432Node\Macromedia\FlashPlayerPlugin";
                        RegistryKey rk32bit = Registry.LocalMachine.OpenSubKey(uninstallKey32bit);
                        RegistryKey rk64bit = Registry.LocalMachine.OpenSubKey(uninstallKey64bit);
                        string pathOne = (rk32bit == null) ? "" : Convert.ToString(rk32bit.GetValue("PlayerPath"));
                        string pathTwo = (rk64bit == null) ? "" : Convert.ToString(rk64bit.GetValue("PlayerPath"));
                        if (pathOne != "" || pathTwo != "")
                        {
                            Globals.InstalledPrograms.Add(softwareName.ToLower());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);

            }

        }
        #endregion

        #region - Get the version of Interner Explorer
        public static int GetInternetExplorerVersion()
        {
            int result;
            result = 0;
            try
            {
                RegistryKey key;
                key = Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Internet Explorer");
                if (key != null)
                {
                    object value;
                    value = key.GetValue("svcVersion", null) ?? key.GetValue("Version", null);
                    if (value != null)
                    {
                        string version;
                        int separator;

                        version = value.ToString();
                        separator = version.IndexOf('.');
                        if (separator != -1)
                        {
                            int.TryParse(version.Substring(0, separator), out result);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
        #endregion

        #region close all opened applications
        public static bool CheckOpenedApplications(string[] processeList)
        {
            bool temp = false;
            try
            {
                //string[] processeList = new string[] { "acrord32", "vlc", "powerpnt", "Winword", "wmplayer", "iexplore" };
                foreach (string process in processeList)
                {
                    Process[] ps = Process.GetProcessesByName(process);
                    if (ps.Length > 0)
                    {
                        temp = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return temp;
        }
        public static void CloseAllOpenedApplications(string processName)
        {
            try
            {
                Process[] ps = Process.GetProcessesByName(processName);
                foreach (Process process in ps)
                {
                    if (!process.HasExited)
                    {
                        process.Kill();
                        process.WaitForExit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region - Killing Unigine
        public static bool killUnigine()
        {
            try
            {
                Process[] currentProcess = Process.GetProcessesByName("Valley");
                foreach (Process ps in currentProcess)
                {
                    if (ps != null)
                    {
                        if (!ps.HasExited)
                        {
                            ps.Kill();
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                Process[] currentPrcs = Process.GetProcessesByName("browser_x86");
                foreach (Process ps in currentPrcs)
                {
                    if (ps != null)
                    {
                        if (!ps.HasExited)
                        {
                            ps.Kill();
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        #endregion



        #region - Download JSON from CSF
        public static async Task<string> DownloadJson()
        {
            string msgUser = "";
            try
            {
                Constants.sfClient = await PasswordAuthentication(Constants.Username, Constants.Password, Constants.Subdomain, Constants.ControlPlane, Constants.clientId, Constants.clientSecret);
                await StartSession(Constants.sfClient);
                var jsonFolderItems = await Constants.sfClient.Items.GetChildren(Constants.XMLFolderUri).ExecuteAsync();
                foreach (var item in jsonFolderItems.Feed)
                {
                    Constants.XMLitemsUri.Add(item.url);
                    //if (item.FileName.Equals(Constants.JSONfileName, StringComparison.OrdinalIgnoreCase))
                    //{
                    //    Constants.XMLitemsUri.Add(item.url);
                    //}
                }
                if (Constants.XMLitemsUri.Count > 0)
                {
                    for (int i = 0; i < Constants.XMLitemsUri.Count; i++)
                    {
                        var JsonItem = await Constants.sfClient.Items.Get(Constants.XMLitemsUri[i]).ExecuteAsync();
                        msgUser = await Download(Constants.sfClient, JsonItem);
                    }
                }
                else
                {
                    msgUser = "The application was unable to start correctly because of missing metadata file. Click OK to close the application." + "\n" + "\n" +
                        "Please reach out to us at CitrixReady@citrix.com.";
                    Console.Write("Json file is missing in CSF"); //TODO
                    return msgUser;
                }
            }
            catch (Exception ex)
            {
                Constants.errorWithDownload = true;
                Console.WriteLine(ex.Message);
                msgUser = "There was an error downloading the metadata file from Citrix Ready ShareFile. Click OK to close the application." + "\n" + "\n" +
                    "Please relaunch the application again." + "\n" + "\n" +
                    "If the problem persists, please reach out to us at CitrixReady@citrix.com";
                //msgUser = ex.Message;
                return msgUser;
            }
            return msgUser;
        }

        private static async Task<string> Download(ShareFileClient sfClient, Item itemToDownload)
        {
            var downloadDirectory = new DirectoryInfo(Environment.CurrentDirectory);
            if (!downloadDirectory.Exists)
            {
                downloadDirectory.Create();
            }
            string fileName = Environment.CurrentDirectory + @"\" + itemToDownload.FileName;
            var downloader = sfClient.GetAsyncFileDownloader(itemToDownload);

            var file = System.IO.File.Open(fileName, FileMode.OpenOrCreate);
            await downloader.DownloadToAsync(file);
            file.Close();
            Constants.isfileDownloaded = true;
            return "Success";
        }

        public static async Task<ShareFileClient> PasswordAuthentication(string Username, string Password, string Subdomain, string ControlPlane, string clientId, string clientSecret)
        {
            // Initialize ShareFileClient.
            var configuration = Configuration.Default();
            configuration.Logger = new DefaultLoggingProvider();

            var sfClient = new ShareFileClient("https://secure.sf-api.com/sf/v3/", configuration);
            //Added try and catch block

            try
            {
                //Added TLS1.2 security.
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var oauthService = new OAuthService(sfClient, clientId, clientSecret);

                // Perform a password grant request.  Will give us an OAuthToken
                var oauthToken = await oauthService.PasswordGrantAsync(Username, Password, Subdomain, ControlPlane);

                // Add credentials and update sfClient with new BaseUri
                sfClient.AddOAuthCredentials(oauthToken);
                sfClient.BaseUri = oauthToken.GetUri();

                return sfClient;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return sfClient;
            }

        }

        public static async Task StartSession(ShareFileClient sfClient)
        {
            var session = await sfClient.Sessions.Login().Expand("Principal").ExecuteAsync();
            Console.WriteLine("Authenticated as " + session.Principal.Email);
        }



        #endregion

        public class ClientPlatform
        {
            public string Id { get; set; }
            public string Name { get; set; }
        }

    }
}
#endregion