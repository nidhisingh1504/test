﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.Configuration;
using System.Diagnostics;
using ThinClient.Verification.Model;
using static ThinClient.Verification.Common.Utilities;

namespace ThinClient.Verification.Common
{
    public static class Globals
    {
        #region - Global Variables 

        public static string SampleFilesLocation = "";
        public static string ReportOutputLocation = "";
        public static string policytitle = "";

        public static string screenshotName = "";
        public static string ClientPlatformId = "";
        public static string errorMessage = "";
        public static Boolean isUserStartedFPS = false;

        public static Boolean IsApplicationLaunchedForFirstTime = true;
        public static Boolean isPremiumTestcasesValidatedFirstTime = true;
        public static Boolean isAudacityTestCaseCompleted = false;
        public static Boolean isUpload = false;

        public static Boolean isRTOPCompleted1 = false;
        public static Boolean isRTOPCompleted2 = false;

        public static StringBuilder PrerequisitesScreenResult = new StringBuilder();
        public static StringBuilder VerificationScreenResult = new StringBuilder();
        
        public static List<String> InstalledPrograms = new List<String>();


        public static bool IsFPSTaskCompleted = false;

        public static bool isITKSnapClosed = false;

        public static bool recordingMessage = false;
        public static bool recordingMessage2 = false;
        public static bool IsVideoRecorded = false;


        public static Boolean IsSystemRequirementsCheckCompletedForHDXReady = false;
        public static Boolean IsSystemRequirementsCheckCompletedForHDXPremium = false;
        public static Boolean IsSystemRequirementsCheckCompletedForHDX3DPro = true;

        public static byte Selected_HDX_Category = 1;
        public static byte Selected_Setup_Choice = Convert.ToByte(Setup_Choice.None);

        internal static int counter;

        public enum HDX_Category : byte
        {
            HDX_Ready = 1,
            HDX_Premium = 2,
            HDX_3DPro = 3,
        };

        public enum Setup_Choice : byte
        {
            None = 0,
            Cloud = 1,
            ON_Prim = 2,
        };

        public enum SoftwareNames : byte
        {
            MicrosoftWord = 1,
            MicrosoftPowerPoint = 2,
            WindowsMediaPlayer = 3,
            AdobeReader = 4,
            VLCMediaPlayer = 5,
            //AdobeFlashPlugin = 6,
            HDXMonitorTool = 6,
            Audacity = 8,
            AdobeFlashPlayer = 9,
            //GoToMeeting = 10,
            MicrosoftSkypeForBusiness = 10,
            nVidiaDriver = 11,
            GPUz = 12,
            ITKSnap = 13,
            GoogleEarth = 14,
            ThreeDXMLPlayer = 15,
            ThreeDConnexion = 16,
            Unigine = 17,
            Teams = 18,
        };
        #region- Client Platform Details
        public static List<ClientPlatform> GetClientPlatform()
        {
            List<ClientPlatform> lstClientPlatform = new List<ClientPlatform>();
            lstClientPlatform.Add(new ClientPlatform { Id = "1", Name = "DOS/WINDOWS" });
            lstClientPlatform.Add(new ClientPlatform { Id = "51", Name = "UNIX/LINUX" });
            lstClientPlatform.Add(new ClientPlatform { Id = "A", Name = "EPOC" });
            lstClientPlatform.Add(new ClientPlatform { Id = "B", Name = "OS/2" });
            lstClientPlatform.Add(new ClientPlatform { Id = "E", Name = "DOS32" });
            lstClientPlatform.Add(new ClientPlatform { Id = "52", Name = "Macintosh" });
            lstClientPlatform.Add(new ClientPlatform { Id = "53", Name = "iOS" });
            lstClientPlatform.Add(new ClientPlatform { Id = "54", Name = "Android" });
            lstClientPlatform.Add(new ClientPlatform { Id = "55", Name = "Blackberry" });
            lstClientPlatform.Add(new ClientPlatform { Id = "56", Name = "Windows Phone 8/WinRT" });
            lstClientPlatform.Add(new ClientPlatform { Id = "57", Name = "Windows Mobile" });
            lstClientPlatform.Add(new ClientPlatform { Id = "58", Name = "Blackberry Playbook" });
            lstClientPlatform.Add(new ClientPlatform { Id = "101", Name = "HTML5" });
            lstClientPlatform.Add(new ClientPlatform { Id = "105", Name = "Java" });
            lstClientPlatform.Add(new ClientPlatform { Id = "F09", Name = "Windows CE" });


            return lstClientPlatform;
        }
        #endregion
        public static string RTOP = "";
        public static string VOIP = "";

        #endregion


    }
}
