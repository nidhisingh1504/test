﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Management;
using System.IO;
using Microsoft.Win32;
using ThinClient.Verification.Common;
using ThinClient.Verification.Model;
using ThinClientVerification;
using System.Text.RegularExpressions;
using System.Threading;

namespace ThinClient.Verification.UserInterface
{
    public partial class frmPrerequisites : Form
    {
        public string msgUser = "";
        Stopwatch stopWatch = new Stopwatch();

        #region - Windows Forms - Inbuilt Events

        public frmPrerequisites()
        {
            InitializeComponent();
        }

        private void frmPrerequisites_Load(object sender, EventArgs e)
        {
            //MessageBox.Show("Prereq - Inside Load: " + Globals.IsApplicationLaunchedForFirstTime.ToString());
            lblBuildNumber.Text = Constants.BUILD_NUMBER;
            if (Globals.IsApplicationLaunchedForFirstTime)
            {
                Utilities.LoadSystemInformations();
                Utilities.LoadSystemRequirements();
                Utilities.LoadTestCaseDetails();
            }
            else
            {
                txtResult.Clear();
                txtResult.AppendText(Globals.PrerequisitesScreenResult.ToString());
            }

            if (Globals.Selected_HDX_Category == Convert.ToByte(Globals.HDX_Category.HDX_Ready))
            {
                tabTestCaseCategories.SelectedTab = tabHDXReady;
            }
            if (Globals.Selected_HDX_Category == Convert.ToByte(Globals.HDX_Category.HDX_Premium))
            {
                tabTestCaseCategories.SelectedTab = tabHDXPremium;
            }


        }

        private void frmPrerequisites_Activated(object sender, EventArgs e)
        {

        }

        private void frmPrerequisites_Shown(object sender, EventArgs e)
        {
            //if (!File.Exists(Constants.XMLPath2))
            //{
            //    if (!File.Exists(Constants.XMLPath1))
            //    {
            //        LoadingIcon icon = new LoadingIcon();
            //        icon.ShowDialog();
            //    }
            //}
            if (!File.Exists(Constants.XMLPath2) || !File.Exists(Constants.XMLPath2) || !File.Exists(Constants.GptFilePath1) || !File.Exists(Constants.GptFilePath2) || !File.Exists(Constants.GptFilePath3))
            {
                LoadingIcon icon = new LoadingIcon();
                icon.ShowDialog();
            }




            //MessageBox.Show("Prereq - Inside Shown: " + Globals.IsApplicationLaunchedForFirstTime.ToString());
            if (Globals.IsApplicationLaunchedForFirstTime)
            {
                ShowSystemInformation();
                Globals.IsApplicationLaunchedForFirstTime = false;
            }

            DisplayDownloadLinks();
            ShowPrerequisiteCheckStatus();

            SetNextButtonState();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Globals.Selected_Setup_Choice == Convert.ToByte(Globals.Setup_Choice.Cloud))
            {
                executeNextStep();
            }
            if (Constants.CitrixStudioIP != "" || Constants.CitrixStudioUserName != "" || Constants.CitrixStudioPassword != "")
            {
                CheckSoftwareRequirements();
                executeNextStep();
            }
        }

        private void executeNextStep()
        {
            if (tabTestCaseCategories.SelectedTab == tabHDXReady)
            {
                Globals.Selected_HDX_Category = Convert.ToByte(Globals.HDX_Category.HDX_Ready);

            }
            if (tabTestCaseCategories.SelectedTab == tabHDXPremium)
            {
                Globals.Selected_HDX_Category = Convert.ToByte(Globals.HDX_Category.HDX_Premium);
            }




            Globals.PrerequisitesScreenResult.Clear();
            for (int iCounter = 0; iCounter < txtResult.Lines.Count(); iCounter++)
            {
                Globals.PrerequisitesScreenResult.Append(txtResult.Lines[iCounter]);
                Globals.PrerequisitesScreenResult.AppendLine();
            }

            var verificationForm = new frmVerification();
            verificationForm.Show();
            this.Hide();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void btnRecheck_Click(object sender, EventArgs e)
        {
            btnExit.Enabled = false;


            stopWatch.Start();

            txtResult.AppendText("\n");
            txtResult.AppendText("\nSOFTWARE REQUIREMENTS CHECK: " + DateTime.Now.ToString() + "\n");

            Globals.InstalledPrograms.Clear();
            //Store the Installed Programs from Registry HKLM Hive 
            Utilities.GetInstalledProgramsFromLocalMachine();
            //Store the Installed Programs from Registry HKCU Hive 
            Utilities.GetInstalledProgramsFromCurrentUser();
            //Store the Installed Programs which are not available directly under Add/Remove programs 
            //Utilities.GetInstalledProgramsOutsideUninstallPrograms(Constants.SOFTWARE_NAME_ADOBE_FLASH_PLUGIN);
            Utilities.GetInstalledProgramsOutsideUninstallPrograms(Constants.SOFTWARE_NAME_WINDOWS_MEDIA_PLAYER);
            Utilities.GetInstalledProgramsOutsideUninstallPrograms(Constants.SOFTWARE_NAME_MICROSOFT_WORD);
            Utilities.GetInstalledProgramsOutsideUninstallPrograms(Constants.SOFTWARE_NAME_MICROSOFT_POWERPOINT);
            Utilities.GetInstalledProgramsOutsideUninstallPrograms(Constants.SOFTWARE_NAME_LYNC);
            Utilities.GetInstalledProgramsOutsideUninstallPrograms(Constants.SOFTWARE_NAME_NVIDIA_DRIVER);
            Utilities.GetInstalledProgramsOutsideUninstallPrograms(Constants.SOFTWARE_NAME_MSTEAM_FOR_BUSINESS);

            CheckSoftwareRequirements();

            stopWatch.Stop();
            var t = Globals.Selected_HDX_Category.ToString();
            // Policy Enhancement
            string domainName = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;
            Constants.CitrixStudioDomainName = domainName;
            //if (domainName == "")
            //{
            //    msgUser = "You are not in same domain";
            //    MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    return;
            //}
            //else
            //{
            if (Globals.Selected_Setup_Choice == Convert.ToByte(Globals.Setup_Choice.None))
            {

                frmPlatformVerification fPVerification = new frmPlatformVerification();
                DialogResult setUpDialogResult = fPVerification.ShowDialog();
                if (setUpDialogResult == DialogResult.No)
                {
                    Globals.Selected_Setup_Choice = Convert.ToByte(Globals.Setup_Choice.ON_Prim);
                }
                else if (setUpDialogResult == DialogResult.Yes)
                {
                    Globals.Selected_Setup_Choice = Convert.ToByte(Globals.Setup_Choice.Cloud);
                }
            }
            if (Globals.Selected_Setup_Choice == Convert.ToByte(Globals.Setup_Choice.ON_Prim))
            {
                if (Constants.CitrixStudioIP == "" || Constants.CitrixStudioUserName == "" || Constants.CitrixStudioPassword == "")
                {
                    frmCredential Cardential = new frmCredential();
                    Cardential.ShowDialog();
                }
            }
            else if (Globals.Selected_Setup_Choice == Convert.ToByte(Globals.Setup_Choice.Cloud))
            {
                if (tabTestCaseCategories.SelectedTab == tabHDXReady)
                    Globals.IsSystemRequirementsCheckCompletedForHDXReady = true;
                else if (tabTestCaseCategories.SelectedTab == tabHDXPremium)
                    Globals.IsSystemRequirementsCheckCompletedForHDXPremium = true;

            }
            //}

            SetNextButtonState();

            btnExit.Enabled = true;
            if (lblStatus9.Text == "Installed")
            {
                Constants.IsEncoder = true;
            }
        }

        public void Init()
        {
            frmPrerequisites.ActiveForm.Height = Constants.Form_Height;
            frmPrerequisites.ActiveForm.Width = Constants.Form_Width;
        }

        private void frmPrerequisites_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
            if (File.Exists(Constants.XMLPath1))
            {
                File.SetAttributes(Environment.CurrentDirectory, FileAttributes.Normal);
                File.Delete(Constants.XMLPath1);
            }
            if (File.Exists(Constants.XMLPath2))
            {
                File.SetAttributes(Environment.CurrentDirectory, FileAttributes.Normal);
                File.Delete(Constants.XMLPath2);
            }

            if (File.Exists(Constants.GptFilePath1))
            {
                File.SetAttributes(Environment.CurrentDirectory, FileAttributes.Normal);
                File.Delete(Constants.GptFilePath1);
            }
            if (File.Exists(Constants.GptFilePath2))
            {
                File.SetAttributes(Environment.CurrentDirectory, FileAttributes.Normal);
                File.Delete(Constants.GptFilePath2);
            }
            if (File.Exists(Constants.GptFilePath3))
            {
                File.SetAttributes(Environment.CurrentDirectory, FileAttributes.Normal);
                File.Delete(Constants.GptFilePath3);
            }


            Constants.CitrixStudioIP = "";
            Constants.CitrixStudioDomainName = "";
            Constants.CitrixStudioUserName = "";
            Constants.CitrixStudioPassword = "";
        }

        private void tabTestCaseCategories_Selected(object sender, TabControlEventArgs e)
        {
            SetNextButtonState();
        }

        #region - Download Links Click Events
        private void lnklblDownload1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload7_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload8_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload9_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload10_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload11_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload12_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload13_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload14_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload15_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        #endregion

        #endregion

        #region - Custom Functions

        #region - Next Button enable/disable logic
        public void SetNextButtonState()
        {
            if (tabTestCaseCategories.SelectedTab == tabHDXReady)
            {
                if (Globals.IsSystemRequirementsCheckCompletedForHDXReady)
                {
                    btnNext.Enabled = true;
                    btnNext.BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_BUTTON_ENABLED);
                }
                else
                {
                    btnNext.Enabled = false;
                    btnNext.BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_BUTTON_DISABLED);
                }
            }

            if (tabTestCaseCategories.SelectedTab == tabHDXPremium)
            {
                if (Globals.IsSystemRequirementsCheckCompletedForHDXPremium)
                {
                    btnNext.Enabled = true;
                    btnNext.BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_BUTTON_ENABLED);
                }
                else
                {
                    btnNext.Enabled = false;
                    btnNext.BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_BUTTON_DISABLED);
                }
            }


        }

        #endregion

        #region - Check Software Requirements
        public void CheckSoftwareRequirements()
        {
            try
            {
                if (tabTestCaseCategories.SelectedTab == tabHDXReady)
                {
                    txtResult.AppendText("HDX Standard - PREREQUISITE VALIDATION" + " \n");
                    CheckSoftware(Constants.SOFTWARE_NAME_MICROSOFT_WORD, Constants.SOFTWARE_NUM1_MICROSOFT_WORD);
                    CheckSoftware(Constants.SOFTWARE_NAME_MICROSOFT_POWERPOINT, Constants.SOFTWARE_NUM2_MICROSOFT_POWERPOINT);
                    CheckSoftware(Constants.SOFTWARE_NAME_WINDOWS_MEDIA_PLAYER, Constants.SOFTWARE_NUM3_WINDOWS_MEDIA_PLAYER);
                    CheckSoftwareAdobe(Constants.SOFTWARE_NAME_ADOBE_READER, Constants.SOFTWARE_NUM4_ADOBE_READER);
                    CheckSoftware(Constants.SOFTWARE_NAME_VLC_MEDIA_PLAYER, Constants.SOFTWARE_NUM5_VLC_MEDIA_PLAYER);
                    //CheckSoftware(Constants.SOFTWARE_NAME_ADOBE_FLASH_PLUGIN, Constants.SOFTWARE_NUM6_ADOBE_FLASH_PLUGIN);
                    CheckSoftware(Constants.SOFTWARE_NAME_HDX_MONITOR, Constants.SOFTWARE_NUM7_HDX_MONITOR);
                    CheckSoftware(Constants.SOFTWARE_NAME_LYNC, Constants.SOFTWARE_NUM11_SKYPE_FOR_BUSINESS2);
                    CheckSoftware(Constants.SOFTWARE_NAME_EXPRESSION_ENCODER, Constants.SOFTWARE_NUM9_EXPRESSION_ENCODER);
                    CheckSoftware(Constants.SOFTWARE_NAME_NVIDIA_DRIVER, Constants.SOFTWARE_NUM10_NVIDIA_DRIVER);
                    CheckSoftware(Constants.SOFTWARE_NAME_GPUZ, Constants.SOFTWARE_NUM11_GPUZ);
                    CheckSoftware(Constants.SOFTWARE_NAME_ITK_SNAP, Constants.SOFTWARE_NUM12_ITK_SNAP);
                    CheckSoftware(Constants.SOFTWARE_NAME_GOOGLE_EARTH, Constants.SOFTWARE_NUM13_GOOGLE_EARTH);
                    CheckSoftware(Constants.SOFTWARE_NAME_3D_XML_PLAYER, Constants.SOFTWARE_NUM14_3D_XML_PLAYER);
                    CheckSoftware(Constants.SOFTWARE_NAME_3D_CONNEXION, Constants.SOFTWARE_NUM15_3D_CONNEXION);
                    CheckSoftware(Constants.SOFTWARE_NAME_UNIGINE, Constants.SOFTWARE_NUM16_UNIGINE);

                    Globals.IsSystemRequirementsCheckCompletedForHDXReady = true;
                    txtResult.AppendText("Time taken for HDX Standard - Prerequisite Validation: " +
                        Convert.ToString(stopWatch.ElapsedMilliseconds) + " ms (" +
                        Convert.ToString((stopWatch.ElapsedMilliseconds / (decimal)1000)) + " sec)");

                }
                if (tabTestCaseCategories.SelectedTab == tabHDXPremium)
                {
                    if (Globals.IsSystemRequirementsCheckCompletedForHDXReady)
                    {
                        txtResult.AppendText("HDX PREMIUM - PREREQUISITE VALIDATION" + " \n");
                        //CheckSoftware(Constants.SOFTWARE_NAME_AUDACITY, Constants.SOFTWARE_NUM17_AUDACITY);
                        //CheckSoftware(Constants.SOFTWARE_NAME_MSTEAM_FOR_BUSINESS, Constants.SOFTWARE_NUM18_TEAM);
                        CheckSoftwareMSTeam(Constants.SOFTWARE_NAME_MSTEAM_FOR_BUSINESS, Constants.SOFTWARE_NUM18_TEAM);
                        //CheckSoftware(Constants.SOFTWARE_NAME_MSTEAM_FOR_BUSINESS, Constants.SOFTWARE_NUM18_TEAM);
                        //CheckSoftware(Constants.SOFTWARE_NAME_ADOBE_FLASH_PLAYER, Constants.SOFTWARE_NUM9_ADOBE_FLASH_PLAYER);

                        ////Both Skype for Business and Lync are some now
                        //CheckSoftware(Constants.SOFTWARE_NAME_LYNC, Constants.SOFTWARE_NUM11_SKYPE_FOR_BUSINESS);
                        //CheckSoftware(Constants.SOFTWARE_NAME_EXPRESSION_ENCODER, Constants.SOFTWARE_NUM9_EXPRESSION_ENCODER2);


                        Globals.IsSystemRequirementsCheckCompletedForHDXPremium = true;
                        txtResult.AppendText("Time taken for HDX Premium - Prerequisite Validation: " +
                            Convert.ToString(stopWatch.ElapsedMilliseconds) + " ms (" +
                            Convert.ToString((stopWatch.ElapsedMilliseconds / (decimal)1000)) + " sec)");
                    }
                    else
                    {
                        msgUser = "You have not completed the HDX Standard System Requirements Check.";
                        MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        tabTestCaseCategories.SelectedTab = tabHDXReady;
                    }
                }
                //if (tabTestCaseCategories.SelectedTab == tabHDX3DPro)
                //{
                //    if (Globals.IsSystemRequirementsCheckCompletedForHDXReady)
                //    {
                //        txtResult.AppendText("HDX 3D PRO - PREREQUISITE VALIDATION" + " \n");
                //        CheckSoftware(Constants.SOFTWARE_NAME_NVIDIA_DRIVER, Constants.SOFTWARE_NUM13_NVIDIA_DRIVER);
                //        CheckSoftware(Constants.SOFTWARE_NAME_GPUZ, Constants.SOFTWARE_NUM14_GPUZ);
                //        CheckSoftware(Constants.SOFTWARE_NAME_ITK_SNAP, Constants.SOFTWARE_NUM15_ITK_SNAP);
                //        CheckSoftware(Constants.SOFTWARE_NAME_GOOGLE_EARTH, Constants.SOFTWARE_NUM16_GOOGLE_EARTH);
                //        CheckSoftware(Constants.SOFTWARE_NAME_3D_XML_PLAYER, Constants.SOFTWARE_NUM17_3D_XML_PLAYER);
                //        CheckSoftware(Constants.SOFTWARE_NAME_3D_CONNEXION, Constants.SOFTWARE_NUM18_3D_CONNEXION);
                //        CheckSoftware(Constants.SOFTWARE_NAME_UNIGINE, Constants.SOFTWARE_NUM19_UNIGINE);
                //        CheckSoftware(Constants.SOFTWARE_NAME_EXPRESSION_ENCODER, Constants.SOFTWARE_NUM9_EXPRESSION_ENCODER3);


                //        Globals.IsSystemRequirementsCheckCompletedForHDX3DPro = true;
                //        txtResult.AppendText("Time taken for HDX Ready - Prerequisite Validation: " +
                //            Convert.ToString(stopWatch.ElapsedMilliseconds) + " ms (" +
                //            Convert.ToString((stopWatch.ElapsedMilliseconds / (decimal)1000)) + " sec)");
                //    }
                //    else
                //    {
                //        if (!Globals.IsSystemRequirementsCheckCompletedForHDXReady)
                //        {
                //            msgUser = "You have not completed the HDX Ready System Requirements Check.";
                //            MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //            tabTestCaseCategories.SelectedTab = tabHDXReady;
                //            return;
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region - CheckSoftware Function
        public void CheckSoftware(string softwareName, int ID)
        {
            bool isInstalled = false;
            string result = "";

            var oSysReq = SysRequirements.oSysRequirements[ID - 1];

           /* if (softwareName == Constants.SOFTWARE_NAME_HDX_MONITOR)
            {
                oSysReq = SysRequirements.oSysRequirements[ID - 2];
            }*/
            if (softwareName == Constants.SOFTWARE_NAME_UNIGINE)
            {
                oSysReq = SysRequirements.oSysRequirements[ID - 11];
            }


            string strStatusControlID = "lblStatus" + Convert.ToString(ID);
            string strDownloadLinkControlID = "lnklblDownload" + Convert.ToString(ID);

            Label lblStatus = this.Controls.Find(strStatusControlID, true).FirstOrDefault() as Label;

            if (Globals.InstalledPrograms.Where(str => str.Contains(softwareName.ToLower())).Count() > 0)
            {
                isInstalled = true;
                oSysReq.IsInstalled = true;
                result = oSysReq.DisplayName + " is installed";
                if (softwareName == Constants.SOFTWARE_NAME_LYNC)
                {
                    Constants.isLynPresent = isInstalled;
                }

                if (softwareName == Constants.SOFTWARE_NAME_GOOGLE_EARTH || softwareName == Constants.SOFTWARE_NAME_ITK_SNAP)
                {
                    Utilities.GetExecutablePath(softwareName);
                }
            }
            else
            {
                isInstalled = false;
                oSysReq.IsInstalled = false;
                result = oSysReq.DisplayName + " is not installed, please download using the above links";
                if (softwareName == Constants.SOFTWARE_NAME_LYNC)
                {
                    Constants.isLynPresent = isInstalled;
                }
            }
            lblStatus.Text = (isInstalled == true ? Constants.PREREQUISITE_STATUS_INSTALLED : Constants.PREREQUISITE_STATUS_NOT_INSTALLED);
            oSysReq.Status = lblStatus.Text;
            lblStatus.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_NORMAL);
            if (lblStatus.Text == Constants.PREREQUISITE_STATUS_INSTALLED)
            {
                lblStatus.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_GREEN);
            }
            if (lblStatus.Text == Constants.PREREQUISITE_STATUS_NOT_INSTALLED)
            {
                lblStatus.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_RED);
            }
            txtResult.AppendText("- " + result + " \n");
        }

        public void CheckSoftwareAdobe(string softwareName, int ID)
        {
            bool isInstalled = false;
            string result = "";
            string[] names = softwareName.Split(new char[] { ' ' });
            var oSysReq = SysRequirements.oSysRequirements[ID - 1];

            string strStatusControlID = "lblStatus" + Convert.ToString(ID);
            string strDownloadLinkControlID = "lnklblDownload" + Convert.ToString(ID);

            Label lblStatus = this.Controls.Find(strStatusControlID, true).FirstOrDefault() as Label;

            int cnt = names.Count();
            if (names.Count() > 1)
            {
                if (Globals.InstalledPrograms.Where(str => (str.Contains(names[0].ToLower()) && str.Contains(names[1].ToLower()))).Count() > 0)
                {
                    isInstalled = true;
                    oSysReq.IsInstalled = true;
                    result = oSysReq.DisplayName + " is installed";
                }
            }
            else
            {
                isInstalled = false;
                oSysReq.IsInstalled = false;
                result = oSysReq.DisplayName + " is not installed, please download using the above links";

            }
            lblStatus.Text = (isInstalled == true ? Constants.PREREQUISITE_STATUS_INSTALLED : Constants.PREREQUISITE_STATUS_NOT_INSTALLED);
            oSysReq.Status = lblStatus.Text;
            lblStatus.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_NORMAL);
            if (lblStatus.Text == Constants.PREREQUISITE_STATUS_INSTALLED)
            {
                lblStatus.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_GREEN);
            }
            if (lblStatus.Text == Constants.PREREQUISITE_STATUS_NOT_INSTALLED)
            {
                lblStatus.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_RED);
            }
            txtResult.AppendText("- " + result + " \n");
        }

        public void CheckSoftwareMSTeam(string softwareName, int ID)
        {
            softwareName = "microsoft teams";
            bool isInstalled = false;
            string result = "";
            string[] names = softwareName.Split(new char[] { ' ' });
            var oSysReq = SysRequirements.oSysRequirements[ID - 1-2];

            string strStatusControlID = "label5";
            string strDownloadLinkControlID = "linkLabel2";

            Label lblStatus = this.Controls.Find(strStatusControlID, true).FirstOrDefault() as Label;

            int cnt = names.Count();
            if (names.Count() > 1)
            {
                if (Globals.InstalledPrograms.Where(str => (str.Contains(names[0].ToLower()) && str.Contains(names[1].ToLower()))).Count() > 0)
                {
                    isInstalled = true;
                    Constants.isTeamPresent = isInstalled;
                    oSysReq.IsInstalled = true;
                    result = oSysReq.DisplayName + " is installed ";
                }
            }
            else
            {
                isInstalled = false;
                Constants.isTeamPresent = false;
                oSysReq.IsInstalled = false;
                result = oSysReq.DisplayName + " is not installed, please download using the above links.";

            }
            lblStatus.Text = (isInstalled == true ? Constants.PREREQUISITE_STATUS_INSTALLED : Constants.PREREQUISITE_STATUS_NOT_INSTALLED);
            oSysReq.Status = lblStatus.Text;
            lblStatus.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_NORMAL);
            if (lblStatus.Text == Constants.PREREQUISITE_STATUS_INSTALLED)
            {
                lblStatus.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_GREEN);
            }
            if (lblStatus.Text == Constants.PREREQUISITE_STATUS_NOT_INSTALLED)
            {
                lblStatus.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_RED);
            }
            txtResult.AppendText("- " + result + " \n");
        }
        #endregion

        #region - Show System Information
        public void ShowSystemInformation()
        {
            txtResult.AppendText("\nSYSTEM INFORMATION" + " \n");
            foreach (var oSysInfo in SysInformations.oSysInformations)
            {
                if (oSysInfo.ID != 3)   // Do not show the Service Pack line 
                {
                    txtResult.AppendText(oSysInfo.ConfigLabel + oSysInfo.ConfigValue + " \n");
                }
            }
        }
        #endregion

        #region - Display Download Links
        public void DisplayDownloadLinks()
        {
            foreach (var oSysReq in SysRequirements.oSysRequirements)
            {
                string strDownloadLinkControlID = "lnklblDownload" + Convert.ToString(oSysReq.ID);
                //lnklblDownload1.Text = oSysReq.DownloadLinkText;

                LinkLabel lnklblDownload = this.Controls.Find(strDownloadLinkControlID, true).FirstOrDefault() as LinkLabel;
                if (lnklblDownload != null)
                {
                    lnklblDownload.Links.Clear();

                    if (!oSysReq.IsLicensed)
                    {
                        lnklblDownload.Text = oSysReq.DownloadLinkText;
                        LinkLabel.Link link = new LinkLabel.Link();
                        link.LinkData = oSysReq.DownloadLinkURL;
                        lnklblDownload.Links.Add(link);
                        lnklblDownload.Visible = true;
                    }
                }

            }
        }
        #endregion

        #region - Show Prerequisite Check Status
        public void ShowPrerequisiteCheckStatus()
        {
            foreach (var oSysReq in SysRequirements.oSysRequirements)
            {
                string strStatusLabelControlID = "lblStatus" + Convert.ToString(oSysReq.ID);

                Label lblStatusControl = this.Controls.Find(strStatusLabelControlID, true).FirstOrDefault() as Label;

                if (lblStatusControl != null)
                {
                    lblStatusControl.Text = oSysReq.Status.ToString();

                    lblStatusControl.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_NORMAL);

                    if (lblStatusControl.Text == Constants.PREREQUISITE_STATUS_INSTALLED)
                    {
                        lblStatusControl.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_GREEN);
                    }
                    if (lblStatusControl.Text == Constants.PREREQUISITE_STATUS_NOT_INSTALLED)
                    {
                        lblStatusControl.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_RED);
                    }

                }

            }
        }
        #endregion

        #endregion - Custom Functions

        private void lnklblDownload16_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload17_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload18_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload19_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload8_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload8_LinkClicked_2(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload9_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload13_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload21_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tlpSystemRequirements_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lnklblDownload14_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload15_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload16_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload18_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }
        private void lnklblDownload23_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            e.Link.LinkData = "https://www.microsoft.com/en-in/microsoft-teams/download-app#desktopAppDownloadregi";
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload17_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload19_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lnklblDownload20_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lblSlNum9_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void lblSoftwareComponent6_Click(object sender, EventArgs e)
        {

        }

        private void lblStatus6_Click(object sender, EventArgs e)
        {

        }
    }
}
