﻿namespace ThinClient.Verification.UserInterface
{
    partial class frmPrerequisites
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrerequisites));
            this.lblHeading = new System.Windows.Forms.Label();
            this.btnRecheck = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.tabTestCaseCategories = new System.Windows.Forms.TabControl();
            this.tabHDXReady = new System.Windows.Forms.TabPage();
            this.tlpSystemRequirements = new System.Windows.Forms.TableLayoutPanel();
            this.lblSlNum20 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblnum9 = new System.Windows.Forms.Label();
            this.lblSlNum8 = new System.Windows.Forms.Label();
            this.lblColHead2 = new System.Windows.Forms.Label();
            this.lblSlNum1 = new System.Windows.Forms.Label();
            this.lblSlNum2 = new System.Windows.Forms.Label();
            this.lblSlNum3 = new System.Windows.Forms.Label();
            this.lblSlNum4 = new System.Windows.Forms.Label();
            this.lblSlNum5 = new System.Windows.Forms.Label();
            //this.lblSlNum6 = new System.Windows.Forms.Label();
            this.lblSlNum7 = new System.Windows.Forms.Label();
            this.lblSoftwareComponent1 = new System.Windows.Forms.Label();
            this.lblSoftwareComponent2 = new System.Windows.Forms.Label();
            this.lblSoftwareComponent3 = new System.Windows.Forms.Label();
            this.lblSoftwareComponent4 = new System.Windows.Forms.Label();
            this.lblSoftwareComponent5 = new System.Windows.Forms.Label();
            this.lblSoftwareComponent6 = new System.Windows.Forms.Label();
            this.lblSoftwareComponent7 = new System.Windows.Forms.Label();
            this.lblColHead3 = new System.Windows.Forms.Label();
            this.lblStatus1 = new System.Windows.Forms.Label();
            this.lblStatus2 = new System.Windows.Forms.Label();
            this.lblStatus3 = new System.Windows.Forms.Label();
            this.lblStatus4 = new System.Windows.Forms.Label();
            this.lblStatus5 = new System.Windows.Forms.Label();
            //this.lblStatus6 = new System.Windows.Forms.Label();
            this.lblStatus7 = new System.Windows.Forms.Label();
            this.lblColHead4 = new System.Windows.Forms.Label();
            this.lnklblDownload1 = new System.Windows.Forms.LinkLabel();
            this.lnklblDownload2 = new System.Windows.Forms.LinkLabel();
            this.lnklblDownload3 = new System.Windows.Forms.LinkLabel();
            this.lnklblDownload4 = new System.Windows.Forms.LinkLabel();
            this.lnklblDownload5 = new System.Windows.Forms.LinkLabel();
            //this.lnklblDownload6 = new System.Windows.Forms.LinkLabel();
            this.lnklblDownload7 = new System.Windows.Forms.LinkLabel();
            this.lblColHead1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblStatus8 = new System.Windows.Forms.Label();
            this.lnklblDownload8 = new System.Windows.Forms.LinkLabel();
            this.lnklblDownload9 = new System.Windows.Forms.LinkLabel();
            this.lblStatus9 = new System.Windows.Forms.Label();
            this.lblSlNum14 = new System.Windows.Forms.Label();
            this.lblSoftwareComponent11 = new System.Windows.Forms.Label();
            this.lblStatus10 = new System.Windows.Forms.Label();
            this.lnklblDownload10 = new System.Windows.Forms.LinkLabel();
            this.lblSlNum15 = new System.Windows.Forms.Label();
            this.lblSoftwareComponent12 = new System.Windows.Forms.Label();
            this.lblStatus11 = new System.Windows.Forms.Label();
            this.lnklblDownload11 = new System.Windows.Forms.LinkLabel();
            this.lblSlNum16 = new System.Windows.Forms.Label();
            this.lblSoftwareComponent13 = new System.Windows.Forms.Label();
            this.lblStatus12 = new System.Windows.Forms.Label();
            this.lnklblDownload12 = new System.Windows.Forms.LinkLabel();
            this.lblSlNum17 = new System.Windows.Forms.Label();
            this.lblStatus13 = new System.Windows.Forms.Label();
            this.lblSlNum18 = new System.Windows.Forms.Label();
            this.lblSoftwareComponent15 = new System.Windows.Forms.Label();
            this.lblStatus14 = new System.Windows.Forms.Label();
            this.lblSlNum19 = new System.Windows.Forms.Label();
            this.lblSoftwareComponent16 = new System.Windows.Forms.Label();
            this.lblStatus15 = new System.Windows.Forms.Label();
            this.lnklblDownload15 = new System.Windows.Forms.LinkLabel();
            this.lblSoftwareComponent17 = new System.Windows.Forms.Label();
            this.lblStatus16 = new System.Windows.Forms.Label();
            this.lnklblDownload16 = new System.Windows.Forms.LinkLabel();
            this.lnklblDownload13 = new System.Windows.Forms.LinkLabel();
            this.lblSoftwareComponent14 = new System.Windows.Forms.Label();
            this.lnklblDownload14 = new System.Windows.Forms.LinkLabel();
            this.tabHDXPremium = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblSlNum9 = new System.Windows.Forms.Label();
            this.lblSoftwareComponent8 = new System.Windows.Forms.Label();
            this.lblColHead3_HDXPremium = new System.Windows.Forms.Label();
            this.lblStatus17 = new System.Windows.Forms.Label();
            this.lblColHead4_HDXPremium = new System.Windows.Forms.Label();
            this.lnklblDownload17 = new System.Windows.Forms.LinkLabel();
            this.lblColHead1_HDXPremium = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.lnklblDownload23 = new System.Windows.Forms.LinkLabel();
            this.lblBuildNumber = new System.Windows.Forms.Label();
            this.lblSeperatorLine1 = new System.Windows.Forms.Label();
            this.picboxTopLogo = new System.Windows.Forms.PictureBox();
            this.tabTestCaseCategories.SuspendLayout();
            this.tabHDXReady.SuspendLayout();
            this.tlpSystemRequirements.SuspendLayout();
            this.tabHDXPremium.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTopLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.Location = new System.Drawing.Point(12, 13);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(112, 24);
            this.lblHeading.TabIndex = 0;
            this.lblHeading.Text = "Prerequisite";
            // 
            // btnRecheck
            // 
            this.btnRecheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.btnRecheck.FlatAppearance.BorderSize = 0;
            this.btnRecheck.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRecheck.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecheck.ForeColor = System.Drawing.Color.White;
            this.btnRecheck.Location = new System.Drawing.Point(417, 503);
            this.btnRecheck.Name = "btnRecheck";
            this.btnRecheck.Size = new System.Drawing.Size(100, 29);
            this.btnRecheck.TabIndex = 1;
            this.btnRecheck.Text = "Validate";
            this.btnRecheck.UseVisualStyleBackColor = false;
            this.btnRecheck.Click += new System.EventHandler(this.btnRecheck_Click);
            // 
            // btnNext
            // 
            this.btnNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(206)))));
            this.btnNext.FlatAppearance.BorderSize = 0;
            this.btnNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNext.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.ForeColor = System.Drawing.Color.White;
            this.btnNext.Location = new System.Drawing.Point(532, 503);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(100, 29);
            this.btnNext.TabIndex = 2;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = false;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(646, 503);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(100, 29);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // txtResult
            // 
            this.txtResult.BackColor = System.Drawing.Color.White;
            this.txtResult.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResult.Location = new System.Drawing.Point(17, 394);
            this.txtResult.Multiline = true;
            this.txtResult.Name = "txtResult";
            this.txtResult.ReadOnly = true;
            this.txtResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtResult.Size = new System.Drawing.Size(731, 103);
            this.txtResult.TabIndex = 4;
            // 
            // tabTestCaseCategories
            // 
            this.tabTestCaseCategories.Controls.Add(this.tabHDXReady);
            this.tabTestCaseCategories.Controls.Add(this.tabHDXPremium);
            this.tabTestCaseCategories.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabTestCaseCategories.Location = new System.Drawing.Point(17, 59);
            this.tabTestCaseCategories.Name = "tabTestCaseCategories";
            this.tabTestCaseCategories.SelectedIndex = 0;
            this.tabTestCaseCategories.Size = new System.Drawing.Size(731, 333);
            this.tabTestCaseCategories.TabIndex = 10;
            this.tabTestCaseCategories.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabTestCaseCategories_Selected);
            // 
            // tabHDXReady
            // 
            this.tabHDXReady.Controls.Add(this.tlpSystemRequirements);
            this.tabHDXReady.Location = new System.Drawing.Point(4, 28);
            this.tabHDXReady.Name = "tabHDXReady";
            this.tabHDXReady.Padding = new System.Windows.Forms.Padding(3);
            this.tabHDXReady.Size = new System.Drawing.Size(723, 301);
            this.tabHDXReady.TabIndex = 0;
            this.tabHDXReady.Text = "HDX Standard";
            this.tabHDXReady.UseVisualStyleBackColor = true;
            // 
            // tlpSystemRequirements
            // 
            this.tlpSystemRequirements.AutoScroll = true;
            this.tlpSystemRequirements.ColumnCount = 4;
            this.tlpSystemRequirements.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.877927F));
            this.tlpSystemRequirements.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.0022F));
            this.tlpSystemRequirements.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.07193F));
            this.tlpSystemRequirements.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.04794F));
            this.tlpSystemRequirements.Controls.Add(this.lblSlNum20, 0, 6);
            this.tlpSystemRequirements.Controls.Add(this.label1, 0, 9);
            this.tlpSystemRequirements.Controls.Add(this.lblnum9, 0, 9);
            this.tlpSystemRequirements.Controls.Add(this.lblSlNum8, 0, 8);
            this.tlpSystemRequirements.Controls.Add(this.lblColHead2, 1, 0);
            this.tlpSystemRequirements.Controls.Add(this.lblSlNum1, 0, 1);
            this.tlpSystemRequirements.Controls.Add(this.lblSlNum2, 0, 2);
            this.tlpSystemRequirements.Controls.Add(this.lblSlNum3, 0, 3);
            this.tlpSystemRequirements.Controls.Add(this.lblSlNum4, 0, 4);
            this.tlpSystemRequirements.Controls.Add(this.lblSlNum5, 0, 5);
            //this.tlpSystemRequirements.Controls.Add(this.lblSlNum6, 0, 6);
            this.tlpSystemRequirements.Controls.Add(this.lblSlNum7, 0, 7);
            this.tlpSystemRequirements.Controls.Add(this.lblSoftwareComponent1, 1, 1);
            this.tlpSystemRequirements.Controls.Add(this.lblSoftwareComponent2, 1, 2);
            this.tlpSystemRequirements.Controls.Add(this.lblSoftwareComponent3, 1, 3);
            this.tlpSystemRequirements.Controls.Add(this.lblSoftwareComponent4, 1, 4);
            this.tlpSystemRequirements.Controls.Add(this.lblSoftwareComponent5, 1, 5);
            this.tlpSystemRequirements.Controls.Add(this.lblSoftwareComponent7, 1, 7);
            this.tlpSystemRequirements.Controls.Add(this.lblColHead3, 2, 0);
            this.tlpSystemRequirements.Controls.Add(this.lblStatus1, 2, 1);
            this.tlpSystemRequirements.Controls.Add(this.lblStatus2, 2, 2);
            this.tlpSystemRequirements.Controls.Add(this.lblStatus3, 2, 3);
            this.tlpSystemRequirements.Controls.Add(this.lblStatus4, 2, 4);
            this.tlpSystemRequirements.Controls.Add(this.lblStatus5, 2, 5);
            //this.tlpSystemRequirements.Controls.Add(this.lblStatus6, 2, 6);
            this.tlpSystemRequirements.Controls.Add(this.lblStatus7, 2, 7);
            this.tlpSystemRequirements.Controls.Add(this.lblColHead4, 3, 0);
            this.tlpSystemRequirements.Controls.Add(this.lnklblDownload1, 3, 1);
            this.tlpSystemRequirements.Controls.Add(this.lnklblDownload2, 3, 2);
            this.tlpSystemRequirements.Controls.Add(this.lnklblDownload3, 3, 3);
            this.tlpSystemRequirements.Controls.Add(this.lnklblDownload4, 3, 4);
            this.tlpSystemRequirements.Controls.Add(this.lnklblDownload5, 3, 5);
            //this.tlpSystemRequirements.Controls.Add(this.lnklblDownload6, 3, 6);
            this.tlpSystemRequirements.Controls.Add(this.lnklblDownload7, 3, 7);
            this.tlpSystemRequirements.Controls.Add(this.lblColHead1, 0, 0);
            this.tlpSystemRequirements.Controls.Add(this.label2, 1, 8);
            this.tlpSystemRequirements.Controls.Add(this.lblStatus8, 2, 8);
            this.tlpSystemRequirements.Controls.Add(this.lnklblDownload8, 3, 8);
            this.tlpSystemRequirements.Controls.Add(this.lnklblDownload9, 3, 9);
            this.tlpSystemRequirements.Controls.Add(this.lblStatus9, 2, 9);
            this.tlpSystemRequirements.Controls.Add(this.lblSlNum14, 0, 10);
            this.tlpSystemRequirements.Controls.Add(this.lblSoftwareComponent11, 1, 10);
            this.tlpSystemRequirements.Controls.Add(this.lblStatus10, 2, 10);
            this.tlpSystemRequirements.Controls.Add(this.lnklblDownload10, 3, 10);
            this.tlpSystemRequirements.Controls.Add(this.lblSlNum15, 0, 11);
            this.tlpSystemRequirements.Controls.Add(this.lblSoftwareComponent12, 1, 11);
            this.tlpSystemRequirements.Controls.Add(this.lblStatus11, 2, 11);
            this.tlpSystemRequirements.Controls.Add(this.lnklblDownload11, 3, 11);
            this.tlpSystemRequirements.Controls.Add(this.lblSlNum16, 0, 12);
            this.tlpSystemRequirements.Controls.Add(this.lblSoftwareComponent13, 1, 12);
            this.tlpSystemRequirements.Controls.Add(this.lblStatus12, 2, 12);
            this.tlpSystemRequirements.Controls.Add(this.lnklblDownload12, 3, 12);
            this.tlpSystemRequirements.Controls.Add(this.lblSlNum17, 0, 13);
            this.tlpSystemRequirements.Controls.Add(this.lblStatus13, 2, 13);
            this.tlpSystemRequirements.Controls.Add(this.lblSlNum18, 0, 14);
            this.tlpSystemRequirements.Controls.Add(this.lblSoftwareComponent15, 1, 14);
            this.tlpSystemRequirements.Controls.Add(this.lblStatus14, 2, 14);
            this.tlpSystemRequirements.Controls.Add(this.lblSlNum19, 0, 15);
            this.tlpSystemRequirements.Controls.Add(this.lblSoftwareComponent16, 1, 15);
            this.tlpSystemRequirements.Controls.Add(this.lblStatus15, 2, 15);
            this.tlpSystemRequirements.Controls.Add(this.lnklblDownload15, 3, 15);
            this.tlpSystemRequirements.Controls.Add(this.lblSoftwareComponent17, 1, 6);
            this.tlpSystemRequirements.Controls.Add(this.lblStatus16, 2, 6);
            this.tlpSystemRequirements.Controls.Add(this.lnklblDownload16, 3, 6);
            this.tlpSystemRequirements.Controls.Add(this.lnklblDownload13, 3, 13);
            this.tlpSystemRequirements.Controls.Add(this.lblSoftwareComponent14, 1, 13);
            this.tlpSystemRequirements.Controls.Add(this.lnklblDownload14, 3, 14);
            //this.tlpSystemRequirements.Controls.Add(this.lblSoftwareComponent6, 1, 6);
            this.tlpSystemRequirements.Location = new System.Drawing.Point(18, 10);
            this.tlpSystemRequirements.Name = "tlpSystemRequirements";
            this.tlpSystemRequirements.RowCount = 17;
            this.tlpSystemRequirements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpSystemRequirements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpSystemRequirements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpSystemRequirements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpSystemRequirements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpSystemRequirements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpSystemRequirements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpSystemRequirements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpSystemRequirements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpSystemRequirements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpSystemRequirements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpSystemRequirements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpSystemRequirements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpSystemRequirements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpSystemRequirements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpSystemRequirements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpSystemRequirements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpSystemRequirements.Size = new System.Drawing.Size(699, 285);
            this.tlpSystemRequirements.TabIndex = 6;
            this.tlpSystemRequirements.Paint += new System.Windows.Forms.PaintEventHandler(this.tlpSystemRequirements_Paint);
            // 
            // lblSlNum20
            // 
            /*this.lblSlNum20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum20.AutoSize = true;
            this.lblSlNum20.Location = new System.Drawing.Point(11, 485);
            this.lblSlNum20.Name = "lblSlNum20";
            this.lblSlNum20.Size = new System.Drawing.Size(25, 19);
            this.lblSlNum20.TabIndex = 36;
            this.lblSlNum20.Text = "6";*/
            this.lblSlNum20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum20.AutoSize = true;
            this.lblSlNum20.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum20.ForeColor = System.Drawing.Color.Black;
            this.lblSlNum20.Location = new System.Drawing.Point(16, 186);
            this.lblSlNum20.Name = "lblSlNum20";
            this.lblSlNum20.Size = new System.Drawing.Size(15, 18);
            this.lblSlNum20.TabIndex = 36;
            this.lblSlNum20.Text = "6";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.label1.Location = new System.Drawing.Point(3, 270);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 30);
            this.label1.TabIndex = 11;
            this.label1.Text = "Microsoft Expression Encoder";
            // 
            // lblnum9
            // 
            this.lblnum9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblnum9.AutoSize = true;
            this.lblnum9.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnum9.ForeColor = System.Drawing.Color.Black;
            this.lblnum9.Location = new System.Drawing.Point(208, 276);
            this.lblnum9.Name = "lblnum9";
            this.lblnum9.Size = new System.Drawing.Size(15, 18);
            this.lblnum9.TabIndex = 8;
            this.lblnum9.Text = "9";
            // 
            // lblSlNum8
            // 
            this.lblSlNum8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum8.AutoSize = true;
            this.lblSlNum8.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum8.ForeColor = System.Drawing.Color.Black;
            this.lblSlNum8.Location = new System.Drawing.Point(16, 246);
            this.lblSlNum8.Name = "lblSlNum8";
            this.lblSlNum8.Size = new System.Drawing.Size(15, 18);
            this.lblSlNum8.TabIndex = 4;
            this.lblSlNum8.Text = "8";
            // 
            // lblColHead2
            // 
            this.lblColHead2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblColHead2.AutoSize = true;
            this.lblColHead2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColHead2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead2.Location = new System.Drawing.Point(51, 5);
            this.lblColHead2.Name = "lblColHead2";
            this.lblColHead2.Size = new System.Drawing.Size(70, 19);
            this.lblColHead2.TabIndex = 0;
            this.lblColHead2.Text = "Software";
            // 
            // lblSlNum1
            // 
            this.lblSlNum1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum1.AutoSize = true;
            this.lblSlNum1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum1.ForeColor = System.Drawing.Color.Black;
            this.lblSlNum1.Location = new System.Drawing.Point(16, 36);
            this.lblSlNum1.Name = "lblSlNum1";
            this.lblSlNum1.Size = new System.Drawing.Size(15, 18);
            this.lblSlNum1.TabIndex = 1;
            this.lblSlNum1.Text = "1";
            // 
            // lblSlNum2
            // 
            this.lblSlNum2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum2.AutoSize = true;
            this.lblSlNum2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum2.ForeColor = System.Drawing.Color.Black;
            this.lblSlNum2.Location = new System.Drawing.Point(16, 66);
            this.lblSlNum2.Name = "lblSlNum2";
            this.lblSlNum2.Size = new System.Drawing.Size(15, 18);
            this.lblSlNum2.TabIndex = 1;
            this.lblSlNum2.Text = "2";
            // 
            // lblSlNum3
            // 
            this.lblSlNum3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum3.AutoSize = true;
            this.lblSlNum3.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum3.ForeColor = System.Drawing.Color.Black;
            this.lblSlNum3.Location = new System.Drawing.Point(16, 96);
            this.lblSlNum3.Name = "lblSlNum3";
            this.lblSlNum3.Size = new System.Drawing.Size(15, 18);
            this.lblSlNum3.TabIndex = 1;
            this.lblSlNum3.Text = "3";
            // 
            // lblSlNum4
            // 
            this.lblSlNum4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum4.AutoSize = true;
            this.lblSlNum4.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum4.ForeColor = System.Drawing.Color.Black;
            this.lblSlNum4.Location = new System.Drawing.Point(16, 126);
            this.lblSlNum4.Name = "lblSlNum4";
            this.lblSlNum4.Size = new System.Drawing.Size(15, 18);
            this.lblSlNum4.TabIndex = 1;
            this.lblSlNum4.Text = "4";
            // 
            // lblSlNum5
            // 
            this.lblSlNum5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum5.AutoSize = true;
            this.lblSlNum5.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum5.ForeColor = System.Drawing.Color.Black;
            this.lblSlNum5.Location = new System.Drawing.Point(16, 156);
            this.lblSlNum5.Name = "lblSlNum5";
            this.lblSlNum5.Size = new System.Drawing.Size(15, 18);
            this.lblSlNum5.TabIndex = 1;
            this.lblSlNum5.Text = "5";
            // 
           /* // lblSlNum6
            // 
            this.lblSlNum6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum6.AutoSize = true;
            this.lblSlNum6.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum6.ForeColor = System.Drawing.Color.Black;
            this.lblSlNum6.Location = new System.Drawing.Point(16, 186);
            this.lblSlNum6.Name = "lblSlNum6";
            this.lblSlNum6.Size = new System.Drawing.Size(15, 18);
            this.lblSlNum6.TabIndex = 1;
            this.lblSlNum6.Text = "6";*/
            // 
            // lblSlNum7
            // 
            this.lblSlNum7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum7.AutoSize = true;
            this.lblSlNum7.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum7.ForeColor = System.Drawing.Color.Black;
            this.lblSlNum7.Location = new System.Drawing.Point(16, 216);
            this.lblSlNum7.Name = "lblSlNum7";
            this.lblSlNum7.Size = new System.Drawing.Size(15, 18);
            this.lblSlNum7.TabIndex = 1;
            this.lblSlNum7.Text = "7";
            // 
            // lblSoftwareComponent1
            // 
            this.lblSoftwareComponent1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSoftwareComponent1.AutoSize = true;
            this.lblSoftwareComponent1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoftwareComponent1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblSoftwareComponent1.Location = new System.Drawing.Point(51, 36);
            this.lblSoftwareComponent1.Name = "lblSoftwareComponent1";
            this.lblSoftwareComponent1.Size = new System.Drawing.Size(102, 18);
            this.lblSoftwareComponent1.TabIndex = 1;
            this.lblSoftwareComponent1.Text = "Microsoft Word";
            // 
            // lblSoftwareComponent2
            // 
            this.lblSoftwareComponent2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSoftwareComponent2.AutoSize = true;
            this.lblSoftwareComponent2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoftwareComponent2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblSoftwareComponent2.Location = new System.Drawing.Point(51, 66);
            this.lblSoftwareComponent2.Name = "lblSoftwareComponent2";
            this.lblSoftwareComponent2.Size = new System.Drawing.Size(142, 18);
            this.lblSoftwareComponent2.TabIndex = 1;
            this.lblSoftwareComponent2.Text = "Microsoft PowerPoint";
            // 
            // lblSoftwareComponent3
            // 
            this.lblSoftwareComponent3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSoftwareComponent3.AutoSize = true;
            this.lblSoftwareComponent3.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoftwareComponent3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblSoftwareComponent3.Location = new System.Drawing.Point(51, 96);
            this.lblSoftwareComponent3.Name = "lblSoftwareComponent3";
            this.lblSoftwareComponent3.Size = new System.Drawing.Size(214, 18);
            this.lblSoftwareComponent3.TabIndex = 1;
            this.lblSoftwareComponent3.Text = "Microsoft Windows Media Player ";
            // 
            // lblSoftwareComponent4
            // 
            this.lblSoftwareComponent4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSoftwareComponent4.AutoSize = true;
            this.lblSoftwareComponent4.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoftwareComponent4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblSoftwareComponent4.Location = new System.Drawing.Point(51, 126);
            this.lblSoftwareComponent4.Name = "lblSoftwareComponent4";
            this.lblSoftwareComponent4.Size = new System.Drawing.Size(96, 18);
            this.lblSoftwareComponent4.TabIndex = 1;
            this.lblSoftwareComponent4.Text = "Adobe Reader";
            // 
            // lblSoftwareComponent5
            // 
            this.lblSoftwareComponent5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSoftwareComponent5.AutoSize = true;
            this.lblSoftwareComponent5.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoftwareComponent5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblSoftwareComponent5.Location = new System.Drawing.Point(51, 156);
            this.lblSoftwareComponent5.Name = "lblSoftwareComponent5";
            this.lblSoftwareComponent5.Size = new System.Drawing.Size(115, 18);
            this.lblSoftwareComponent5.TabIndex = 1;
            this.lblSoftwareComponent5.Text = "VLC Media Player";
            // 
            // lblSoftwareComponent6
            // 
           /* this.lblSoftwareComponent6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSoftwareComponent6.AutoSize = true;
            this.lblSoftwareComponent6.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoftwareComponent6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblSoftwareComponent6.Location = new System.Drawing.Point(51, 186);
            this.lblSoftwareComponent6.Name = "lblSoftwareComponent6";
            this.lblSoftwareComponent6.Size = new System.Drawing.Size(217, 18);
            this.lblSoftwareComponent6.TabIndex = 1;
            this.lblSoftwareComponent6.Text = "Flash Plug-in for Internet Explorer";
            this.lblSoftwareComponent6.Click += new System.EventHandler(this.lblSoftwareComponent6_Click);*/
            // 
            // lblSoftwareComponent7
            // 
            this.lblSoftwareComponent7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSoftwareComponent7.AutoSize = true;
            this.lblSoftwareComponent7.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoftwareComponent7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblSoftwareComponent7.Location = new System.Drawing.Point(51, 216);
            this.lblSoftwareComponent7.Name = "lblSoftwareComponent7";
            this.lblSoftwareComponent7.Size = new System.Drawing.Size(116, 18);
            this.lblSoftwareComponent7.TabIndex = 1;
            this.lblSoftwareComponent7.Text = "HDX Monitor Tool";
            // 
            // lblColHead3
            // 
            this.lblColHead3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblColHead3.AutoSize = true;
            this.lblColHead3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColHead3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead3.Location = new System.Drawing.Point(386, 5);
            this.lblColHead3.Name = "lblColHead3";
            this.lblColHead3.Size = new System.Drawing.Size(52, 19);
            this.lblColHead3.TabIndex = 0;
            this.lblColHead3.Text = "Status";
            // 
            // lblStatus1
            // 
            this.lblStatus1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus1.AutoSize = true;
            this.lblStatus1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus1.Location = new System.Drawing.Point(386, 36);
            this.lblStatus1.Name = "lblStatus1";
            this.lblStatus1.Size = new System.Drawing.Size(87, 18);
            this.lblStatus1.TabIndex = 1;
            this.lblStatus1.Text = "Not Checked";
            // 
            // lblStatus2
            // 
            this.lblStatus2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus2.AutoSize = true;
            this.lblStatus2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus2.Location = new System.Drawing.Point(386, 66);
            this.lblStatus2.Name = "lblStatus2";
            this.lblStatus2.Size = new System.Drawing.Size(87, 18);
            this.lblStatus2.TabIndex = 1;
            this.lblStatus2.Text = "Not Checked";
            // 
            // lblStatus3
            // 
            this.lblStatus3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus3.AutoSize = true;
            this.lblStatus3.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus3.Location = new System.Drawing.Point(386, 96);
            this.lblStatus3.Name = "lblStatus3";
            this.lblStatus3.Size = new System.Drawing.Size(87, 18);
            this.lblStatus3.TabIndex = 1;
            this.lblStatus3.Text = "Not Checked";
            // 
            // lblStatus4
            // 
            this.lblStatus4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus4.AutoSize = true;
            this.lblStatus4.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus4.Location = new System.Drawing.Point(386, 126);
            this.lblStatus4.Name = "lblStatus4";
            this.lblStatus4.Size = new System.Drawing.Size(87, 18);
            this.lblStatus4.TabIndex = 1;
            this.lblStatus4.Text = "Not Checked";
            // 
            // lblStatus5
            // 
            this.lblStatus5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus5.AutoSize = true;
            this.lblStatus5.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus5.Location = new System.Drawing.Point(386, 156);
            this.lblStatus5.Name = "lblStatus5";
            this.lblStatus5.Size = new System.Drawing.Size(87, 18);
            this.lblStatus5.TabIndex = 1;
            this.lblStatus5.Text = "Not Checked";
            // 
           /* // lblStatus6
            // 
            this.lblStatus6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus6.AutoSize = true;
            this.lblStatus6.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus6.Location = new System.Drawing.Point(386, 186);
            this.lblStatus6.Name = "lblStatus6";
            this.lblStatus6.Size = new System.Drawing.Size(87, 18);
            this.lblStatus6.TabIndex = 1;
            this.lblStatus6.Text = "Not Checked";
            this.lblStatus6.Click += new System.EventHandler(this.lblStatus6_Click);*/
            // 
            // lblStatus7
            // 
            this.lblStatus7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus7.AutoSize = true;
            this.lblStatus7.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus7.Location = new System.Drawing.Point(386, 216);
            this.lblStatus7.Name = "lblStatus7";
            this.lblStatus7.Size = new System.Drawing.Size(87, 18);
            this.lblStatus7.TabIndex = 1;
            this.lblStatus7.Text = "Not Checked";
            // 
            // lblColHead4
            // 
            this.lblColHead4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblColHead4.AutoSize = true;
            this.lblColHead4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColHead4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead4.Location = new System.Drawing.Point(491, 5);
            this.lblColHead4.Name = "lblColHead4";
            this.lblColHead4.Size = new System.Drawing.Size(79, 19);
            this.lblColHead4.TabIndex = 0;
            this.lblColHead4.Text = "Download";
            // 
            // lnklblDownload1
            // 
            this.lnklblDownload1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lnklblDownload1.AutoSize = true;
            this.lnklblDownload1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnklblDownload1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lnklblDownload1.Location = new System.Drawing.Point(491, 36);
            this.lnklblDownload1.Name = "lnklblDownload1";
            this.lnklblDownload1.Size = new System.Drawing.Size(81, 18);
            this.lnklblDownload1.TabIndex = 2;
            this.lnklblDownload1.TabStop = true;
            this.lnklblDownload1.Text = "Unavailable";
            this.lnklblDownload1.Visible = false;
            this.lnklblDownload1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblDownload1_LinkClicked);
            // 
            // lnklblDownload2
            // 
            this.lnklblDownload2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lnklblDownload2.AutoSize = true;
            this.lnklblDownload2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnklblDownload2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lnklblDownload2.Location = new System.Drawing.Point(491, 66);
            this.lnklblDownload2.Name = "lnklblDownload2";
            this.lnklblDownload2.Size = new System.Drawing.Size(81, 18);
            this.lnklblDownload2.TabIndex = 2;
            this.lnklblDownload2.TabStop = true;
            this.lnklblDownload2.Text = "Unavailable";
            this.lnklblDownload2.Visible = false;
            this.lnklblDownload2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblDownload2_LinkClicked);
            // 
            // lnklblDownload3
            // 
            this.lnklblDownload3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lnklblDownload3.AutoSize = true;
            this.lnklblDownload3.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnklblDownload3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lnklblDownload3.Location = new System.Drawing.Point(491, 96);
            this.lnklblDownload3.Name = "lnklblDownload3";
            this.lnklblDownload3.Size = new System.Drawing.Size(81, 18);
            this.lnklblDownload3.TabIndex = 2;
            this.lnklblDownload3.TabStop = true;
            this.lnklblDownload3.Text = "Unavailable";
            this.lnklblDownload3.Visible = false;
            this.lnklblDownload3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblDownload3_LinkClicked);
            // 
            // lnklblDownload4
            // 
            this.lnklblDownload4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lnklblDownload4.AutoSize = true;
            this.lnklblDownload4.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnklblDownload4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lnklblDownload4.Location = new System.Drawing.Point(491, 126);
            this.lnklblDownload4.Name = "lnklblDownload4";
            this.lnklblDownload4.Size = new System.Drawing.Size(81, 18);
            this.lnklblDownload4.TabIndex = 2;
            this.lnklblDownload4.TabStop = true;
            this.lnklblDownload4.Text = "Unavailable";
            this.lnklblDownload4.Visible = false;
            this.lnklblDownload4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblDownload4_LinkClicked);
            // 
            // lnklblDownload5
            // 
            this.lnklblDownload5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lnklblDownload5.AutoSize = true;
            this.lnklblDownload5.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnklblDownload5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lnklblDownload5.Location = new System.Drawing.Point(491, 156);
            this.lnklblDownload5.Name = "lnklblDownload5";
            this.lnklblDownload5.Size = new System.Drawing.Size(81, 18);
            this.lnklblDownload5.TabIndex = 2;
            this.lnklblDownload5.TabStop = true;
            this.lnklblDownload5.Text = "Unavailable";
            this.lnklblDownload5.Visible = false;
            this.lnklblDownload5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblDownload5_LinkClicked);
            // 
            // lnklblDownload6
            // 
            /*this.lnklblDownload6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lnklblDownload6.AutoSize = true;
            this.lnklblDownload6.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnklblDownload6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lnklblDownload6.Location = new System.Drawing.Point(491, 186);
            this.lnklblDownload6.Name = "lnklblDownload6";
            this.lnklblDownload6.Size = new System.Drawing.Size(81, 18);
            this.lnklblDownload6.TabIndex = 2;
            this.lnklblDownload6.TabStop = true;
            this.lnklblDownload6.Text = "Unavailable";
            this.lnklblDownload6.Visible = false;
            this.lnklblDownload6.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblDownload6_LinkClicked);*/
            // 
            // lnklblDownload7
            // 
            this.lnklblDownload7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lnklblDownload7.AutoSize = true;
            this.lnklblDownload7.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnklblDownload7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lnklblDownload7.Location = new System.Drawing.Point(491, 216);
            this.lnklblDownload7.Name = "lnklblDownload7";
            this.lnklblDownload7.Size = new System.Drawing.Size(81, 18);
            this.lnklblDownload7.TabIndex = 2;
            this.lnklblDownload7.TabStop = true;
            this.lnklblDownload7.Text = "Unavailable";
            this.lnklblDownload7.Visible = false;
            this.lnklblDownload7.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblDownload7_LinkClicked);
            // 
            // lblColHead1
            // 
            this.lblColHead1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblColHead1.AutoSize = true;
            this.lblColHead1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColHead1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead1.Location = new System.Drawing.Point(6, 5);
            this.lblColHead1.Name = "lblColHead1";
            this.lblColHead1.Size = new System.Drawing.Size(35, 19);
            this.lblColHead1.TabIndex = 3;
            this.lblColHead1.Text = "S/N";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.label2.Location = new System.Drawing.Point(51, 246);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(184, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "Microsoft Skype for Business";
            // 
            // lblStatus8
            // 
            this.lblStatus8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus8.AutoSize = true;
            this.lblStatus8.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus8.Location = new System.Drawing.Point(386, 246);
            this.lblStatus8.Name = "lblStatus8";
            this.lblStatus8.Size = new System.Drawing.Size(87, 18);
            this.lblStatus8.TabIndex = 6;
            this.lblStatus8.Text = "Not Checked";
            // 
            // lnklblDownload8
            // 
            this.lnklblDownload8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lnklblDownload8.AutoSize = true;
            this.lnklblDownload8.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnklblDownload8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lnklblDownload8.Location = new System.Drawing.Point(491, 246);
            this.lnklblDownload8.Name = "lnklblDownload8";
            this.lnklblDownload8.Size = new System.Drawing.Size(81, 18);
            this.lnklblDownload8.TabIndex = 7;
            this.lnklblDownload8.TabStop = true;
            this.lnklblDownload8.Text = "Unavailable";
            this.lnklblDownload8.Visible = false;
            this.lnklblDownload8.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblDownload8_LinkClicked_2);
            // 
            // lnklblDownload9
            // 
            this.lnklblDownload9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lnklblDownload9.AutoSize = true;
            this.lnklblDownload9.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnklblDownload9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lnklblDownload9.Location = new System.Drawing.Point(491, 276);
            this.lnklblDownload9.Name = "lnklblDownload9";
            this.lnklblDownload9.Size = new System.Drawing.Size(81, 18);
            this.lnklblDownload9.TabIndex = 9;
            this.lnklblDownload9.TabStop = true;
            this.lnklblDownload9.Text = "Unavailable";
            this.lnklblDownload9.Visible = false;
            this.lnklblDownload9.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblDownload9_LinkClicked_1);
            // 
            // lblStatus9
            // 
            this.lblStatus9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus9.AutoSize = true;
            this.lblStatus9.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus9.Location = new System.Drawing.Point(386, 276);
            this.lblStatus9.Name = "lblStatus9";
            this.lblStatus9.Size = new System.Drawing.Size(87, 18);
            this.lblStatus9.TabIndex = 10;
            this.lblStatus9.Text = "Not Checked";
            // 
            // lblSlNum14
            // 
            this.lblSlNum14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum14.AutoSize = true;
            this.lblSlNum14.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum14.Location = new System.Drawing.Point(13, 306);
            this.lblSlNum14.Name = "lblSlNum14";
            this.lblSlNum14.Size = new System.Drawing.Size(22, 18);
            this.lblSlNum14.TabIndex = 12;
            this.lblSlNum14.Text = "10";
            // 
            // lblSoftwareComponent11
            // 
            this.lblSoftwareComponent11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSoftwareComponent11.AutoSize = true;
            this.lblSoftwareComponent11.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoftwareComponent11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblSoftwareComponent11.Location = new System.Drawing.Point(51, 306);
            this.lblSoftwareComponent11.Name = "lblSoftwareComponent11";
            this.lblSoftwareComponent11.Size = new System.Drawing.Size(151, 18);
            this.lblSoftwareComponent11.TabIndex = 13;
            this.lblSoftwareComponent11.Text = "nVidia Graphics Drivers";
            // 
            // lblStatus10
            // 
            this.lblStatus10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus10.AutoSize = true;
            this.lblStatus10.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus10.Location = new System.Drawing.Point(386, 306);
            this.lblStatus10.Name = "lblStatus10";
            this.lblStatus10.Size = new System.Drawing.Size(87, 18);
            this.lblStatus10.TabIndex = 14;
            this.lblStatus10.Text = "Not Checked";
            // 
            // lnklblDownload10
            // 
            this.lnklblDownload10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lnklblDownload10.AutoSize = true;
            this.lnklblDownload10.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnklblDownload10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lnklblDownload10.Location = new System.Drawing.Point(491, 306);
            this.lnklblDownload10.Name = "lnklblDownload10";
            this.lnklblDownload10.Size = new System.Drawing.Size(81, 18);
            this.lnklblDownload10.TabIndex = 15;
            this.lnklblDownload10.TabStop = true;
            this.lnklblDownload10.Text = "Unavailable";
            this.lnklblDownload10.Visible = false;
            this.lnklblDownload10.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblDownload14_LinkClicked_1);
            // 
            // lblSlNum15
            // 
            this.lblSlNum15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum15.AutoSize = true;
            this.lblSlNum15.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum15.Location = new System.Drawing.Point(13, 336);
            this.lblSlNum15.Name = "lblSlNum15";
            this.lblSlNum15.Size = new System.Drawing.Size(22, 18);
            this.lblSlNum15.TabIndex = 16;
            this.lblSlNum15.Text = "11";
            // 
            // lblSoftwareComponent12
            // 
            this.lblSoftwareComponent12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSoftwareComponent12.AutoSize = true;
            this.lblSoftwareComponent12.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoftwareComponent12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblSoftwareComponent12.Location = new System.Drawing.Point(51, 336);
            this.lblSoftwareComponent12.Name = "lblSoftwareComponent12";
            this.lblSoftwareComponent12.Size = new System.Drawing.Size(75, 18);
            this.lblSoftwareComponent12.TabIndex = 17;
            this.lblSoftwareComponent12.Text = "GPU-Z Tool";
            // 
            // lblStatus11
            // 
            this.lblStatus11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus11.AutoSize = true;
            this.lblStatus11.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus11.Location = new System.Drawing.Point(386, 336);
            this.lblStatus11.Name = "lblStatus11";
            this.lblStatus11.Size = new System.Drawing.Size(87, 18);
            this.lblStatus11.TabIndex = 18;
            this.lblStatus11.Text = "Not Checked";
            // 
            // lnklblDownload11
            // 
            this.lnklblDownload11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lnklblDownload11.AutoSize = true;
            this.lnklblDownload11.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnklblDownload11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lnklblDownload11.Location = new System.Drawing.Point(491, 336);
            this.lnklblDownload11.Name = "lnklblDownload11";
            this.lnklblDownload11.Size = new System.Drawing.Size(81, 18);
            this.lnklblDownload11.TabIndex = 19;
            this.lnklblDownload11.TabStop = true;
            this.lnklblDownload11.Text = "Unavailable";
            this.lnklblDownload11.Visible = false;
            this.lnklblDownload11.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblDownload15_LinkClicked_1);
            // 
            // lblSlNum16
            // 
            this.lblSlNum16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum16.AutoSize = true;
            this.lblSlNum16.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum16.Location = new System.Drawing.Point(13, 366);
            this.lblSlNum16.Name = "lblSlNum16";
            this.lblSlNum16.Size = new System.Drawing.Size(22, 18);
            this.lblSlNum16.TabIndex = 20;
            this.lblSlNum16.Text = "12";
            // 
            // lblSoftwareComponent13
            // 
            this.lblSoftwareComponent13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSoftwareComponent13.AutoSize = true;
            this.lblSoftwareComponent13.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoftwareComponent13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblSoftwareComponent13.Location = new System.Drawing.Point(51, 366);
            this.lblSoftwareComponent13.Name = "lblSoftwareComponent13";
            this.lblSoftwareComponent13.Size = new System.Drawing.Size(98, 18);
            this.lblSoftwareComponent13.TabIndex = 21;
            this.lblSoftwareComponent13.Text = "ITK-SNAP 3.4.0";
            // 
            // lblStatus12
            // 
            this.lblStatus12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus12.AutoSize = true;
            this.lblStatus12.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus12.Location = new System.Drawing.Point(386, 366);
            this.lblStatus12.Name = "lblStatus12";
            this.lblStatus12.Size = new System.Drawing.Size(87, 18);
            this.lblStatus12.TabIndex = 22;
            this.lblStatus12.Text = "Not Checked";
            // 
            // lnklblDownload12
            // 
            this.lnklblDownload12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lnklblDownload12.AutoSize = true;
            this.lnklblDownload12.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnklblDownload12.Location = new System.Drawing.Point(491, 366);
            this.lnklblDownload12.Name = "lnklblDownload12";
            this.lnklblDownload12.Size = new System.Drawing.Size(81, 18);
            this.lnklblDownload12.TabIndex = 23;
            this.lnklblDownload12.TabStop = true;
            this.lnklblDownload12.Text = "Unavailable";
            this.lnklblDownload12.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblDownload16_LinkClicked_1);
            // 
            // lblSlNum17
            // 
            this.lblSlNum17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum17.AutoSize = true;
            this.lblSlNum17.Location = new System.Drawing.Point(11, 395);
            this.lblSlNum17.Name = "lblSlNum17";
            this.lblSlNum17.Size = new System.Drawing.Size(25, 19);
            this.lblSlNum17.TabIndex = 24;
            this.lblSlNum17.Text = "13";
            // 
            // lblStatus13
            // 
            this.lblStatus13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus13.AutoSize = true;
            this.lblStatus13.Font = new System.Drawing.Font("Calibri", 11F);
            this.lblStatus13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus13.Location = new System.Drawing.Point(386, 396);
            this.lblStatus13.Name = "lblStatus13";
            this.lblStatus13.Size = new System.Drawing.Size(87, 18);
            this.lblStatus13.TabIndex = 26;
            this.lblStatus13.Text = "Not Checked";
            // 
            // lblSlNum18
            // 
            this.lblSlNum18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum18.AutoSize = true;
            this.lblSlNum18.Location = new System.Drawing.Point(11, 425);
            this.lblSlNum18.Name = "lblSlNum18";
            this.lblSlNum18.Size = new System.Drawing.Size(25, 19);
            this.lblSlNum18.TabIndex = 28;
            this.lblSlNum18.Text = "14";
            // 
            // lblSoftwareComponent15
            // 
            this.lblSoftwareComponent15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSoftwareComponent15.AutoSize = true;
            this.lblSoftwareComponent15.Font = new System.Drawing.Font("Calibri", 11F);
            this.lblSoftwareComponent15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblSoftwareComponent15.Location = new System.Drawing.Point(51, 426);
            this.lblSoftwareComponent15.Name = "lblSoftwareComponent15";
            this.lblSoftwareComponent15.Size = new System.Drawing.Size(95, 18);
            this.lblSoftwareComponent15.TabIndex = 29;
            this.lblSoftwareComponent15.Text = "3D XML Player";
            // 
            // lblStatus14
            // 
            this.lblStatus14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus14.AutoSize = true;
            this.lblStatus14.Font = new System.Drawing.Font("Calibri", 11F);
            this.lblStatus14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus14.Location = new System.Drawing.Point(386, 426);
            this.lblStatus14.Name = "lblStatus14";
            this.lblStatus14.Size = new System.Drawing.Size(87, 18);
            this.lblStatus14.TabIndex = 30;
            this.lblStatus14.Text = "Not Checked";
            // 
            // lblSlNum19
            // 
            this.lblSlNum19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum19.AutoSize = true;
            this.lblSlNum19.Location = new System.Drawing.Point(11, 455);
            this.lblSlNum19.Name = "lblSlNum19";
            this.lblSlNum19.Size = new System.Drawing.Size(25, 19);
            this.lblSlNum19.TabIndex = 32;
            this.lblSlNum19.Text = "15";
            // 
            // lblSoftwareComponent16
            // 
            this.lblSoftwareComponent16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSoftwareComponent16.AutoSize = true;
            this.lblSoftwareComponent16.Font = new System.Drawing.Font("Calibri", 11F);
            this.lblSoftwareComponent16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblSoftwareComponent16.Location = new System.Drawing.Point(51, 456);
            this.lblSoftwareComponent16.Name = "lblSoftwareComponent16";
            this.lblSoftwareComponent16.Size = new System.Drawing.Size(94, 18);
            this.lblSoftwareComponent16.TabIndex = 33;
            this.lblSoftwareComponent16.Text = "3D Connexion";
            // 
            // lblStatus15
            // 
            this.lblStatus15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus15.AutoSize = true;
            this.lblStatus15.Font = new System.Drawing.Font("Calibri", 11F);
            this.lblStatus15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus15.Location = new System.Drawing.Point(386, 456);
            this.lblStatus15.Name = "lblStatus15";
            this.lblStatus15.Size = new System.Drawing.Size(87, 18);
            this.lblStatus15.TabIndex = 34;
            this.lblStatus15.Text = "Not Checked";
            // 
            // lnklblDownload15
            // 
            this.lnklblDownload15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lnklblDownload15.AutoSize = true;
            this.lnklblDownload15.Font = new System.Drawing.Font("Calibri", 11F);
            this.lnklblDownload15.Location = new System.Drawing.Point(491, 456);
            this.lnklblDownload15.Name = "lnklblDownload15";
            this.lnklblDownload15.Size = new System.Drawing.Size(81, 18);
            this.lnklblDownload15.TabIndex = 35;
            this.lnklblDownload15.TabStop = true;
            this.lnklblDownload15.Text = "Unavailable";
            this.lnklblDownload15.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblDownload19_LinkClicked_1);
            // 
            // lblSoftwareComponent17
            // 
            /*this.lblSoftwareComponent17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSoftwareComponent17.AutoSize = true;
            this.lblSoftwareComponent17.Font = new System.Drawing.Font("Calibri", 11F);
            this.lblSoftwareComponent17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblSoftwareComponent17.Location = new System.Drawing.Point(51, 486);
            this.lblSoftwareComponent17.Name = "lblSoftwareComponent17";
            this.lblSoftwareComponent17.Size = new System.Drawing.Size(56, 18);
            this.lblSoftwareComponent17.TabIndex = 37;
            this.lblSoftwareComponent17.Text = "Unigine";*/
            this.lblSoftwareComponent17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSoftwareComponent17.AutoSize = true;
            this.lblSoftwareComponent17.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoftwareComponent17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblSoftwareComponent17.Location = new System.Drawing.Point(51, 186);
            this.lblSoftwareComponent17.Name = "lblSoftwareComponent17";
            this.lblSoftwareComponent17.Size = new System.Drawing.Size(217, 18);
            this.lblSoftwareComponent17.TabIndex = 36;
            this.lblSoftwareComponent17.Text = "Unigine                        ";
            // 
            // lblStatus16
            // 
            this.lblStatus16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus16.AutoSize = true;
            this.lblStatus16.Font = new System.Drawing.Font("Calibri", 11F);
            this.lblStatus16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus16.Location = new System.Drawing.Point(386, 486);
            this.lblStatus16.Name = "lblStatus16";
            this.lblStatus16.Size = new System.Drawing.Size(87, 18);
            this.lblStatus16.TabIndex = 38;
            this.lblStatus16.Text = "Not Checked";
            // 
            // lnklblDownload16
            // 
            this.lnklblDownload16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lnklblDownload16.AutoSize = true;
            this.lnklblDownload16.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnklblDownload16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            //this.lnklblDownload16.Font = new System.Drawing.Font("Calibri", 11F);
            this.lnklblDownload16.Location = new System.Drawing.Point(491, 486);
            this.lnklblDownload16.Name = "lnklblDownload16";
            this.lnklblDownload16.Size = new System.Drawing.Size(81, 18);
            this.lnklblDownload16.TabIndex = 39;
            this.lnklblDownload16.TabStop = true;
            this.lnklblDownload16.Text = "Unavailable";
            this.lnklblDownload16.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblDownload20_LinkClicked);
            // 
            // lnklblDownload13
            // 
            this.lnklblDownload13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lnklblDownload13.AutoSize = true;
            this.lnklblDownload13.Font = new System.Drawing.Font("Calibri", 11F);
            this.lnklblDownload13.Location = new System.Drawing.Point(491, 396);
            this.lnklblDownload13.Name = "lnklblDownload13";
            this.lnklblDownload13.Size = new System.Drawing.Size(81, 18);
            this.lnklblDownload13.TabIndex = 27;
            this.lnklblDownload13.TabStop = true;
            this.lnklblDownload13.Text = "Unavailable";
            this.lnklblDownload13.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblDownload17_LinkClicked_1);
            // 
            // lblSoftwareComponent14
            // 
            this.lblSoftwareComponent14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSoftwareComponent14.AutoSize = true;
            this.lblSoftwareComponent14.Font = new System.Drawing.Font("Calibri", 11F);
            this.lblSoftwareComponent14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblSoftwareComponent14.Location = new System.Drawing.Point(51, 396);
            this.lblSoftwareComponent14.Name = "lblSoftwareComponent14";
            this.lblSoftwareComponent14.Size = new System.Drawing.Size(87, 18);
            this.lblSoftwareComponent14.TabIndex = 25;
            this.lblSoftwareComponent14.Text = "Google Earth";
            // 
            // lnklblDownload14
            // 
            this.lnklblDownload14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lnklblDownload14.AutoSize = true;
            this.lnklblDownload14.Font = new System.Drawing.Font("Calibri", 11F);
            this.lnklblDownload14.Location = new System.Drawing.Point(491, 426);
            this.lnklblDownload14.Name = "lnklblDownload14";
            this.lnklblDownload14.Size = new System.Drawing.Size(81, 18);
            this.lnklblDownload14.TabIndex = 31;
            this.lnklblDownload14.TabStop = true;
            this.lnklblDownload14.Text = "Unavailable";
            this.lnklblDownload14.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblDownload18_LinkClicked_1);
            // 
            // tabHDXPremium
            // 
            this.tabHDXPremium.Controls.Add(this.tableLayoutPanel1);
            this.tabHDXPremium.Location = new System.Drawing.Point(4, 28);
            this.tabHDXPremium.Name = "tabHDXPremium";
            this.tabHDXPremium.Padding = new System.Windows.Forms.Padding(3);
            this.tabHDXPremium.Size = new System.Drawing.Size(723, 301);
            this.tabHDXPremium.TabIndex = 1;
            this.tabHDXPremium.Text = "HDX Premium";
            this.tabHDXPremium.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.877927F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.0022F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.07193F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.04794F));
            //this.tableLayoutPanel1.Controls.Add(this.lblSlNum9, 0, 1);
            //this.tableLayoutPanel1.Controls.Add(this.lblSoftwareComponent8, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblColHead3_HDXPremium, 2, 0);
            //this.tableLayoutPanel1.Controls.Add(this.lblStatus17, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblColHead4_HDXPremium, 3, 0);
            //this.tableLayoutPanel1.Controls.Add(this.lnklblDownload17, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblColHead1_HDXPremium, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.linkLabel2, 3, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(18, 6);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 14;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(699, 240);
            this.tableLayoutPanel1.TabIndex = 7;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // lblSlNum9
            // 
            this.lblSlNum9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum9.AutoSize = true;
            this.lblSlNum9.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum9.ForeColor = System.Drawing.Color.Black;
            this.lblSlNum9.Location = new System.Drawing.Point(16, 36);
            this.lblSlNum9.Name = "lblSlNum9";
            this.lblSlNum9.Size = new System.Drawing.Size(15, 18);
            this.lblSlNum9.TabIndex = 1;
            this.lblSlNum9.Text = "1";
            this.lblSlNum9.Click += new System.EventHandler(this.lblSlNum9_Click);
            // 
            // lblSoftwareComponent8
            // 
            this.lblSoftwareComponent8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSoftwareComponent8.AutoSize = true;
            this.lblSoftwareComponent8.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoftwareComponent8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblSoftwareComponent8.Location = new System.Drawing.Point(51, 36);
            this.lblSoftwareComponent8.Name = "lblSoftwareComponent8";
            this.lblSoftwareComponent8.Size = new System.Drawing.Size(62, 18);
            this.lblSoftwareComponent8.TabIndex = 1;
            this.lblSoftwareComponent8.Text = "Audacity";
            // 
            // lblColHead3_HDXPremium
            // 
            this.lblColHead3_HDXPremium.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblColHead3_HDXPremium.AutoSize = true;
            this.lblColHead3_HDXPremium.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColHead3_HDXPremium.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead3_HDXPremium.Location = new System.Drawing.Point(386, 5);
            this.lblColHead3_HDXPremium.Name = "lblColHead3_HDXPremium";
            this.lblColHead3_HDXPremium.Size = new System.Drawing.Size(52, 19);
            this.lblColHead3_HDXPremium.TabIndex = 0;
            this.lblColHead3_HDXPremium.Text = "Status";
            // 
            // lblStatus17
            // 
            this.lblStatus17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus17.AutoSize = true;
            this.lblStatus17.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus17.Location = new System.Drawing.Point(386, 36);
            this.lblStatus17.Name = "lblStatus17";
            this.lblStatus17.Size = new System.Drawing.Size(87, 18);
            this.lblStatus17.TabIndex = 1;
            this.lblStatus17.Text = "Not Checked";
            // 
            // lblColHead4_HDXPremium
            // 
            this.lblColHead4_HDXPremium.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblColHead4_HDXPremium.AutoSize = true;
            this.lblColHead4_HDXPremium.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColHead4_HDXPremium.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead4_HDXPremium.Location = new System.Drawing.Point(491, 5);
            this.lblColHead4_HDXPremium.Name = "lblColHead4_HDXPremium";
            this.lblColHead4_HDXPremium.Size = new System.Drawing.Size(79, 19);
            this.lblColHead4_HDXPremium.TabIndex = 0;
            this.lblColHead4_HDXPremium.Text = "Download";
            // 
            // lnklblDownload17
            // 
            this.lnklblDownload17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lnklblDownload17.AutoSize = true;
            this.lnklblDownload17.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnklblDownload17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lnklblDownload17.Location = new System.Drawing.Point(491, 36);
            this.lnklblDownload17.Name = "lnklblDownload17";
            this.lnklblDownload17.Size = new System.Drawing.Size(81, 18);
            this.lnklblDownload17.TabIndex = 2;
            this.lnklblDownload17.TabStop = true;
            this.lnklblDownload17.Text = "Unavailable";
            this.lnklblDownload17.Visible = false;
            this.lnklblDownload17.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblDownload8_LinkClicked);
            // 
            // lblColHead1_HDXPremium
            // 
            this.lblColHead1_HDXPremium.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblColHead1_HDXPremium.AutoSize = true;
            this.lblColHead1_HDXPremium.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.lblColHead1_HDXPremium.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead1_HDXPremium.Location = new System.Drawing.Point(6, 5);
            this.lblColHead1_HDXPremium.Name = "lblColHead1_HDXPremium";
            this.lblColHead1_HDXPremium.Size = new System.Drawing.Size(35, 19);
            this.lblColHead1_HDXPremium.TabIndex = 3;
            this.lblColHead1_HDXPremium.Text = "S/N";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.label4.Location = new System.Drawing.Point(51, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 18);
            this.label4.TabIndex = 5;
            this.label4.Text = "MS TEAM";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(16, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "1";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.label5.Location = new System.Drawing.Point(386, 75);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 18);
            this.label5.TabIndex = 6;
            this.label5.Text = "Not Checked";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(3, 340);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 20);
            this.label6.TabIndex = 7;
            // 
            // linkLabel2
            // 
            this.linkLabel2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.linkLabel2.Location = new System.Drawing.Point(491, 75);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(71, 18);
            this.linkLabel2.TabIndex = 7;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Download";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblDownload23_LinkClicked_1);
            // 
            // lnklblDownload23
            // 
            this.lnklblDownload23.Location = new System.Drawing.Point(0, 0);
            this.lnklblDownload23.Name = "lnklblDownload23";
            this.lnklblDownload23.Size = new System.Drawing.Size(100, 23);
            this.lnklblDownload23.TabIndex = 0;
            // 
            // lblBuildNumber
            // 
            this.lblBuildNumber.AutoSize = true;
            this.lblBuildNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblBuildNumber.Location = new System.Drawing.Point(5, 535);
            this.lblBuildNumber.Name = "lblBuildNumber";
            this.lblBuildNumber.Size = new System.Drawing.Size(71, 13);
            this.lblBuildNumber.TabIndex = 12;
            this.lblBuildNumber.Text = "Build Number";
            // 
            // lblSeperatorLine1
            // 
            this.lblSeperatorLine1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeperatorLine1.Location = new System.Drawing.Point(-3, 52);
            this.lblSeperatorLine1.Name = "lblSeperatorLine1";
            this.lblSeperatorLine1.Size = new System.Drawing.Size(800, 2);
            this.lblSeperatorLine1.TabIndex = 6;
            // 
            // picboxTopLogo
            // 
            this.picboxTopLogo.BackColor = System.Drawing.SystemColors.Control;
            this.picboxTopLogo.Image = global::ThinClientVerification.Properties.Resources.Screen_1_800x800;
            this.picboxTopLogo.Location = new System.Drawing.Point(-1, -14);
            this.picboxTopLogo.Name = "picboxTopLogo";
            this.picboxTopLogo.Size = new System.Drawing.Size(786, 129);
            this.picboxTopLogo.TabIndex = 11;
            this.picboxTopLogo.TabStop = false;
            // 
            // frmPrerequisites
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.lblBuildNumber);
            this.Controls.Add(this.tabTestCaseCategories);
            this.Controls.Add(this.lblSeperatorLine1);
            this.Controls.Add(this.txtResult);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnRecheck);
            this.Controls.Add(this.lblHeading);
            this.Controls.Add(this.picboxTopLogo);
            this.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmPrerequisites";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Citrix Ready - Thin Client Verification";
            this.Activated += new System.EventHandler(this.frmPrerequisites_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmPrerequisites_FormClosed);
            this.Load += new System.EventHandler(this.frmPrerequisites_Load);
            this.Shown += new System.EventHandler(this.frmPrerequisites_Shown);
            this.tabTestCaseCategories.ResumeLayout(false);
            this.tabHDXReady.ResumeLayout(false);
            this.tlpSystemRequirements.ResumeLayout(false);
            this.tlpSystemRequirements.PerformLayout();
            this.tabHDXPremium.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTopLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.Button btnRecheck;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.TabControl tabTestCaseCategories;
        private System.Windows.Forms.TabPage tabHDXReady;
        private System.Windows.Forms.TabPage tabHDXPremium;
        private System.Windows.Forms.TableLayoutPanel tlpSystemRequirements;
        private System.Windows.Forms.Label lblColHead2;
        private System.Windows.Forms.Label lblSlNum1;
        private System.Windows.Forms.Label lblSlNum2;
        private System.Windows.Forms.Label lblSlNum3;
        private System.Windows.Forms.Label lblSlNum4;
        private System.Windows.Forms.Label lblSlNum5;
        private System.Windows.Forms.Label lblSlNum6;
        private System.Windows.Forms.Label lblSlNum7;
        private System.Windows.Forms.Label lblSoftwareComponent1;
        private System.Windows.Forms.Label lblSoftwareComponent2;
        private System.Windows.Forms.Label lblSoftwareComponent3;
        private System.Windows.Forms.Label lblSoftwareComponent4;
        private System.Windows.Forms.Label lblSoftwareComponent5;
        private System.Windows.Forms.Label lblSoftwareComponent6;
        private System.Windows.Forms.Label lblSoftwareComponent7;
        private System.Windows.Forms.Label lblColHead3;
        private System.Windows.Forms.Label lblStatus1;
        private System.Windows.Forms.Label lblStatus2;
        private System.Windows.Forms.Label lblStatus3;
        private System.Windows.Forms.Label lblStatus4;
        private System.Windows.Forms.Label lblStatus5;
        private System.Windows.Forms.Label lblStatus6;
        private System.Windows.Forms.Label lblStatus7;
        private System.Windows.Forms.Label lblColHead4;
        private System.Windows.Forms.LinkLabel lnklblDownload1;
        private System.Windows.Forms.LinkLabel lnklblDownload2;
        private System.Windows.Forms.LinkLabel lnklblDownload3;
        private System.Windows.Forms.LinkLabel lnklblDownload4;
        private System.Windows.Forms.LinkLabel lnklblDownload5;
        private System.Windows.Forms.LinkLabel lnklblDownload6;
        private System.Windows.Forms.LinkLabel lnklblDownload7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblSlNum9;
        private System.Windows.Forms.Label lblSoftwareComponent8;
        private System.Windows.Forms.Label lblColHead3_HDXPremium;
        private System.Windows.Forms.Label lblStatus17;
        private System.Windows.Forms.Label lblColHead4_HDXPremium;
        private System.Windows.Forms.LinkLabel lnklblDownload17;
        private System.Windows.Forms.PictureBox picboxTopLogo;
        private System.Windows.Forms.Label lblColHead1;
        private System.Windows.Forms.Label lblColHead1_HDXPremium;
        private System.Windows.Forms.Label lblBuildNumber;
        private System.Windows.Forms.Label lblSlNum8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblStatus8;
        private System.Windows.Forms.LinkLabel lnklblDownload8;
        private System.Windows.Forms.Label lblnum9;
        private System.Windows.Forms.LinkLabel lnklblDownload9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblStatus9;
        private System.Windows.Forms.Label lblSeperatorLine1;
        private System.Windows.Forms.Label lblSlNum14;
        private System.Windows.Forms.Label lblSoftwareComponent11;
        private System.Windows.Forms.Label lblStatus10;
        private System.Windows.Forms.LinkLabel lnklblDownload10;
        private System.Windows.Forms.Label lblSlNum15;
        private System.Windows.Forms.Label lblSoftwareComponent12;
        private System.Windows.Forms.Label lblStatus11;
        private System.Windows.Forms.LinkLabel lnklblDownload11;
        private System.Windows.Forms.Label lblSlNum16;
        private System.Windows.Forms.Label lblSoftwareComponent13;
        private System.Windows.Forms.Label lblStatus12;
        private System.Windows.Forms.LinkLabel lnklblDownload12;
        private System.Windows.Forms.Label lblSlNum17;
        private System.Windows.Forms.Label lblSoftwareComponent14;
        private System.Windows.Forms.Label lblStatus13;
        private System.Windows.Forms.LinkLabel lnklblDownload13;
        private System.Windows.Forms.Label lblSlNum18;
        private System.Windows.Forms.Label lblSoftwareComponent15;
        private System.Windows.Forms.Label lblStatus14;
        private System.Windows.Forms.LinkLabel lnklblDownload14;
        private System.Windows.Forms.Label lblSlNum19;
        private System.Windows.Forms.Label lblSoftwareComponent16;
        private System.Windows.Forms.Label lblStatus15;
        private System.Windows.Forms.LinkLabel lnklblDownload15;
        private System.Windows.Forms.Label lblSlNum20;
        private System.Windows.Forms.Label lblSoftwareComponent17;
        private System.Windows.Forms.Label lblStatus16;
        private System.Windows.Forms.LinkLabel lnklblDownload16;
        private System.Windows.Forms.Label lblSoftwareComponent23;
        private System.Windows.Forms.Label lblStatus23;
        private System.Windows.Forms.LinkLabel lnklblDownload23;
        private System.Windows.Forms.Label lblSlNum23;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.LinkLabel linkLabel2;
    }
}