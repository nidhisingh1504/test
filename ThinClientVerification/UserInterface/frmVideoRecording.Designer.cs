﻿namespace ThinClientVerification.UserInterface
{
    partial class frmVideoRecording
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVideoRecording));
            this.lblWatermark = new System.Windows.Forms.Label();
            this.lblTimer = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnRecord = new System.Windows.Forms.Button();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.SuspendLayout();

            // 
            // lblWatermark
            // 
            this.lblWatermark.AutoSize = true;
            this.lblWatermark.Location = new System.Drawing.Point(27, 69);
            this.lblWatermark.Name = "lblWatermark";
            this.lblWatermark.Size = new System.Drawing.Size(59, 13);
            this.lblWatermark.TabIndex = 5;
            this.lblWatermark.Text = "Watermark";
            this.toolTip1.SetToolTip(this.lblWatermark, "Watermark");
            // 
            // lblTimer
            // 
            this.lblTimer.AutoSize = true;
            this.lblTimer.Location = new System.Drawing.Point(154, 71);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(49, 13);
            this.lblTimer.TabIndex = 3;
            this.lblTimer.Text = "00:00:00";
            this.toolTip1.SetToolTip(this.lblTimer, "Watermark");
            // 
            // btnRecord
            // 
            this.btnRecord.Image = global::ThinClientVerification.Properties.Resources.Media_Record;
            this.btnRecord.Location = new System.Drawing.Point(32, 12);
            this.btnRecord.Name = "btnRecord";
            this.btnRecord.Size = new System.Drawing.Size(35, 35);
            this.btnRecord.TabIndex = 7;
            this.toolTip1.SetToolTip(this.btnRecord, "Start Recording");
            this.btnRecord.UseVisualStyleBackColor = true;
            this.btnRecord.Click += new System.EventHandler(this.btnRecord_Click);
            // 
            // btnPause
            // 
            this.btnPause.Image = global::ThinClientVerification.Properties.Resources.player_pause;
            this.btnPause.Location = new System.Drawing.Point(134, 12);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(35, 35);
            this.btnPause.TabIndex = 8;
            this.toolTip1.SetToolTip(this.btnPause, "Pause Recording");
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnStop
            // 
            this.btnStop.Image = global::ThinClientVerification.Properties.Resources.button_stop_red;
            this.btnStop.Location = new System.Drawing.Point(84, 12);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(35, 35);
            this.btnStop.TabIndex = 9;
            this.toolTip1.SetToolTip(this.btnStop, "Stop Recording");
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.Enabled = false;
            this.btnPlay.Image = global::ThinClientVerification.Properties.Resources.button_play_green;
            this.btnPlay.Location = new System.Drawing.Point(185, 12);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(35, 35);
            this.btnPlay.TabIndex = 10;
            this.toolTip1.SetToolTip(this.btnPlay, "Play Recording");
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // frmVideoRecording
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(254, 93);
            this.Controls.Add(this.btnRecord);
            this.Controls.Add(this.btnPause);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnPlay);
            this.Controls.Add(this.lblTimer);
            this.Controls.Add(this.lblWatermark);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmVideoRecording";
            this.Text = "Recorder";
            this.Load += new System.EventHandler(this.frmVideoRecording_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblWatermark;
        private System.Windows.Forms.Label lblTimer;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnRecord;
        private System.Windows.Forms.TextBox txtResult;
    }
}