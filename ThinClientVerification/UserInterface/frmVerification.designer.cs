﻿namespace ThinClient.Verification.UserInterface
{
    partial class frmVerification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVerification));
            this.btnExit = new System.Windows.Forms.Button();
            this.btnViewReport = new System.Windows.Forms.Button();
            this.lblHeading = new System.Windows.Forms.Label();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.lblSeperatorLine1 = new System.Windows.Forms.Label();
            this.tabTestCases = new System.Windows.Forms.TabControl();
            this.tabHDXReady = new System.Windows.Forms.TabPage();
            this.tlpHDXReady = new System.Windows.Forms.TableLayoutPanel();
            this.chkTestCase7 = new System.Windows.Forms.CheckBox();
            this.lblNumNetScaler = new System.Windows.Forms.Label();
            this.lblRequiredNetScaler = new System.Windows.Forms.Label();
            this.lblColHead4 = new System.Windows.Forms.Label();
            this.lblStatus1 = new System.Windows.Forms.Label();
            this.lblStatus2 = new System.Windows.Forms.Label();
            this.lblStatus3 = new System.Windows.Forms.Label();
            this.lblStatus4 = new System.Windows.Forms.Label();
            this.lblStatus5 = new System.Windows.Forms.Label();
            this.lblStatus6 = new System.Windows.Forms.Label();
            this.lblRequired1 = new System.Windows.Forms.Label();
            this.lblRequired2 = new System.Windows.Forms.Label();
            this.lblStatus7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlTestCase7 = new System.Windows.Forms.Panel();
            this.picboxTestCase7 = new System.Windows.Forms.PictureBox();
            this.lblTestCase7 = new System.Windows.Forms.Label();
            this.lblRequired3 = new System.Windows.Forms.Label();
            this.lblRequired4 = new System.Windows.Forms.Label();
            this.lblRequired5 = new System.Windows.Forms.Label();
            this.lblRequired6 = new System.Windows.Forms.Label();
            this.lblColHead2 = new System.Windows.Forms.Label();
            this.lblSlNum1 = new System.Windows.Forms.Label();
            this.lblSlNum2 = new System.Windows.Forms.Label();
            this.lblSlNum3 = new System.Windows.Forms.Label();
            this.lblSlNum4 = new System.Windows.Forms.Label();
            this.lblSlNum5 = new System.Windows.Forms.Label();
            this.lblSlNum6 = new System.Windows.Forms.Label();
            this.lblColHead1 = new System.Windows.Forms.Label();
            this.chkTestCase1 = new System.Windows.Forms.CheckBox();
            this.chkTestCase2 = new System.Windows.Forms.CheckBox();
            this.chkTestCase3 = new System.Windows.Forms.CheckBox();
            this.chkTestCase5 = new System.Windows.Forms.CheckBox();
            this.chkTestCase6 = new System.Windows.Forms.CheckBox();
            this.chkTestCaseHDXReady = new System.Windows.Forms.CheckBox();
            this.pnlTestCase1 = new System.Windows.Forms.Panel();
            this.lblprocessing = new System.Windows.Forms.Label();
            this.picboxTestCase1 = new System.Windows.Forms.PictureBox();
            this.lblTestCase1 = new System.Windows.Forms.Label();
            this.pnlTestCase2 = new System.Windows.Forms.Panel();
            this.picboxTestCase2 = new System.Windows.Forms.PictureBox();
            this.lblTestCase2 = new System.Windows.Forms.Label();
            this.lblColHead3 = new System.Windows.Forms.Label();
            this.pnlTestCase3 = new System.Windows.Forms.Panel();
            this.picboxTestCase3 = new System.Windows.Forms.PictureBox();
            this.lblTestCase3 = new System.Windows.Forms.Label();
            this.pnlTestCase5 = new System.Windows.Forms.Panel();
            this.picboxTestCase5 = new System.Windows.Forms.PictureBox();
            this.lblTestCase5 = new System.Windows.Forms.Label();
            this.pnlTestCase6 = new System.Windows.Forms.Panel();
            this.picboxTestCase6 = new System.Windows.Forms.PictureBox();
            this.lblTestCase6 = new System.Windows.Forms.Label();
            this.lblSlNum7 = new System.Windows.Forms.Label();
            this.pnlTestCase8 = new System.Windows.Forms.Panel();
            this.picboxTestCase8 = new System.Windows.Forms.PictureBox();
            this.lblTestCase8 = new System.Windows.Forms.Label();
            this.chkTestCase8 = new System.Windows.Forms.CheckBox();
            this.lblStatus8 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.pnlTestCase4 = new System.Windows.Forms.Panel();
            this.picboxTestCase4 = new System.Windows.Forms.PictureBox();
            this.lblTestCase4 = new System.Windows.Forms.Label();
            this.tabHDX3DPro = new System.Windows.Forms.TabPage();
            this.tlpHDX3DPro = new System.Windows.Forms.TableLayoutPanel();
            this.pnlTestCase15 = new System.Windows.Forms.Panel();
            this.picboxTestCase15 = new System.Windows.Forms.PictureBox();
            this.lblTestCase15 = new System.Windows.Forms.Label();
            this.pnlTestCase14 = new System.Windows.Forms.Panel();
            this.picboxTestCase14 = new System.Windows.Forms.PictureBox();
            this.lblTestCase14 = new System.Windows.Forms.Label();
            this.pnlTestCase13 = new System.Windows.Forms.Panel();
            this.lblTestCase13 = new System.Windows.Forms.Label();
            this.picboxTestCase13 = new System.Windows.Forms.PictureBox();
            this.lblStatus15 = new System.Windows.Forms.Label();
            this.lblStatus14 = new System.Windows.Forms.Label();
            this.lblStatus13 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblColHead4HDX3DPro = new System.Windows.Forms.Label();
            this.lblStatus9 = new System.Windows.Forms.Label();
            this.lblStatus10 = new System.Windows.Forms.Label();
            this.lblStatus11 = new System.Windows.Forms.Label();
            this.lblStatus12 = new System.Windows.Forms.Label();
            this.lblRequired15 = new System.Windows.Forms.Label();
            this.lblRequired16 = new System.Windows.Forms.Label();
            this.lblRequired17 = new System.Windows.Forms.Label();
            this.lblRequired18 = new System.Windows.Forms.Label();
            this.lblColHead2HDX3DPro = new System.Windows.Forms.Label();
            this.lblColHead1HDX3DPro = new System.Windows.Forms.Label();
            this.lblSlNum15 = new System.Windows.Forms.Label();
            this.lblSlNum16 = new System.Windows.Forms.Label();
            this.lblSlNum17 = new System.Windows.Forms.Label();
            this.lblSlNum18 = new System.Windows.Forms.Label();
            this.chkTestCase9 = new System.Windows.Forms.CheckBox();
            this.chkTestCase10 = new System.Windows.Forms.CheckBox();
            this.chkTestCase11 = new System.Windows.Forms.CheckBox();
            this.chkTestCase12 = new System.Windows.Forms.CheckBox();
            this.chkTestCaseHDX3DPro = new System.Windows.Forms.CheckBox();
            this.lblColHead3HDX3DPro = new System.Windows.Forms.Label();
            this.pnlTestCase9 = new System.Windows.Forms.Panel();
            this.picboxTestCase9 = new System.Windows.Forms.PictureBox();
            this.lblTestCase9 = new System.Windows.Forms.Label();
            this.pnlTestCase10 = new System.Windows.Forms.Panel();
            this.picboxTestCase10 = new System.Windows.Forms.PictureBox();
            this.lblTestCase10 = new System.Windows.Forms.Label();
            this.pnlTestCase11 = new System.Windows.Forms.Panel();
            this.picboxTestCase11 = new System.Windows.Forms.PictureBox();
            this.lblTestCase11 = new System.Windows.Forms.Label();
            this.pnlTestCase12 = new System.Windows.Forms.Panel();
            this.picboxTestCase12 = new System.Windows.Forms.PictureBox();
            this.lblTestCase12 = new System.Windows.Forms.Label();
            this.chkTestCase13 = new System.Windows.Forms.CheckBox();
            this.chkTestCase14 = new System.Windows.Forms.CheckBox();
            this.chkTestCase15 = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.chkTestCase21 = new System.Windows.Forms.CheckBox();
            this.lblSlNum13 = new System.Windows.Forms.Label();
            this.lblRequired13 = new System.Windows.Forms.Label();
            this.lblStatus21 = new System.Windows.Forms.Label();
            this.pnlTestCase21 = new System.Windows.Forms.Panel();
            this.picboxTestCase21 = new System.Windows.Forms.PictureBox();
            this.lblTestCase21 = new System.Windows.Forms.Label();
            this.tabHDXPremium = new System.Windows.Forms.TabPage();
            this.tlpHDXPremium = new System.Windows.Forms.TableLayoutPanel();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.lblStatus23 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.chkTestCase23 = new System.Windows.Forms.CheckBox();
            this.lblColHead4HDXPremium = new System.Windows.Forms.Label();
            this.lblColHead2HDXPremium = new System.Windows.Forms.Label();
            this.lblColHead1HDXPremium = new System.Windows.Forms.Label();
            this.chkTestCaseHDXPremium = new System.Windows.Forms.CheckBox();
            this.lblColHead3HDXPremium = new System.Windows.Forms.Label();
            this.chkTestCase16 = new System.Windows.Forms.CheckBox();
            this.chkTestCase19 = new System.Windows.Forms.CheckBox();
            this.chkTestCase20 = new System.Windows.Forms.CheckBox();
            this.chkTestCase22 = new System.Windows.Forms.CheckBox();
            this.lblSlNum8 = new System.Windows.Forms.Label();
            this.lblSlNum9 = new System.Windows.Forms.Label();
            this.lblSlNum19 = new System.Windows.Forms.Label();
            this.lblSlNum12 = new System.Windows.Forms.Label();
            this.lblSlNum14 = new System.Windows.Forms.Label();
            this.lblRequired8 = new System.Windows.Forms.Label();
            this.lblRequired9 = new System.Windows.Forms.Label();
            this.lblRequired11 = new System.Windows.Forms.Label();
            this.lblRequired12 = new System.Windows.Forms.Label();
            this.lblRequired14 = new System.Windows.Forms.Label();
            this.lblStatus16 = new System.Windows.Forms.Label();
            this.lblStatus17 = new System.Windows.Forms.Label();
            this.lblStatus19 = new System.Windows.Forms.Label();
            this.lblStatus20 = new System.Windows.Forms.Label();
            this.lblStatus22 = new System.Windows.Forms.Label();
            this.pnlTestCase16 = new System.Windows.Forms.Panel();
            this.picboxTestCase16 = new System.Windows.Forms.PictureBox();
            this.lblTestCase16 = new System.Windows.Forms.Label();
            this.pnlTestCase17 = new System.Windows.Forms.Panel();
            this.picboxTestCase17 = new System.Windows.Forms.PictureBox();
            this.lblTestCase17 = new System.Windows.Forms.Label();
            this.pnlTestCase19 = new System.Windows.Forms.Panel();
            this.picboxTestCase19 = new System.Windows.Forms.PictureBox();
            this.lblTestCase19 = new System.Windows.Forms.Label();
            this.pnlTestCase20 = new System.Windows.Forms.Panel();
            this.picboxTestCase20 = new System.Windows.Forms.PictureBox();
            this.lblTestCase20 = new System.Windows.Forms.Label();
            this.pnlTestCase22 = new System.Windows.Forms.Panel();
            this.picboxTestCase22 = new System.Windows.Forms.PictureBox();
            this.lblTestCase22 = new System.Windows.Forms.Label();
            this.pnlTestCase23 = new System.Windows.Forms.Panel();
            this.picboxTestCase23 = new System.Windows.Forms.PictureBox();
            this.lblTestCase23 = new System.Windows.Forms.Label();
            this.chkTestCase24 = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.chkTestCase4 = new System.Windows.Forms.CheckBox();
            this.chkTestCase17 = new System.Windows.Forms.CheckBox();
            this.chkTestCase18 = new System.Windows.Forms.CheckBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnValidate = new System.Windows.Forms.Button();
            this.ttHelp = new System.Windows.Forms.ToolTip(this.components);
            this.lblSampleFileLocation = new System.Windows.Forms.Label();
            this.btnSampleFilesLocation = new System.Windows.Forms.Button();
            this.lblSampleFileLocationValue = new System.Windows.Forms.Label();
            this.folderBrowserDialogSampleFileLocation = new System.Windows.Forms.FolderBrowserDialog();
            this.lblBuildNumber = new System.Windows.Forms.Label();
            this.browseFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.picboxTopLogo = new System.Windows.Forms.PictureBox();
            this.tabTestCases.SuspendLayout();
            this.tabHDXReady.SuspendLayout();
            this.tlpHDXReady.SuspendLayout();
            this.pnlTestCase7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase7)).BeginInit();
            this.pnlTestCase1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase1)).BeginInit();
            this.pnlTestCase2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase2)).BeginInit();
            this.pnlTestCase3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase3)).BeginInit();
            this.pnlTestCase5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase5)).BeginInit();
            this.pnlTestCase6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase6)).BeginInit();
            this.pnlTestCase8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase8)).BeginInit();
            this.pnlTestCase4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase4)).BeginInit();
            this.tabHDX3DPro.SuspendLayout();
            this.tlpHDX3DPro.SuspendLayout();
            this.pnlTestCase15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase15)).BeginInit();
            this.pnlTestCase14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase14)).BeginInit();
            this.pnlTestCase13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase13)).BeginInit();
            this.pnlTestCase9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase9)).BeginInit();
            this.pnlTestCase10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase10)).BeginInit();
            this.pnlTestCase11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase11)).BeginInit();
            this.pnlTestCase12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase12)).BeginInit();
            this.pnlTestCase21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase21)).BeginInit();
            this.tabHDXPremium.SuspendLayout();
            this.tlpHDXPremium.SuspendLayout();
            this.pnlTestCase16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase16)).BeginInit();
            this.pnlTestCase17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase17)).BeginInit();
            this.pnlTestCase19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase19)).BeginInit();
            this.pnlTestCase20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase20)).BeginInit();
            this.pnlTestCase22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase22)).BeginInit();
            this.pnlTestCase23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase23)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTopLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(634, 521);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(130, 29);
            this.btnExit.TabIndex = 0;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnViewReport
            // 
            this.btnViewReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.btnViewReport.FlatAppearance.BorderSize = 0;
            this.btnViewReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewReport.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewReport.ForeColor = System.Drawing.Color.White;
            this.btnViewReport.Location = new System.Drawing.Point(493, 521);
            this.btnViewReport.Name = "btnViewReport";
            this.btnViewReport.Size = new System.Drawing.Size(130, 29);
            this.btnViewReport.TabIndex = 1;
            this.btnViewReport.Text = "Generate Report";
            this.btnViewReport.UseVisualStyleBackColor = false;
            this.btnViewReport.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.Location = new System.Drawing.Point(12, 14);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(127, 24);
            this.lblHeading.TabIndex = 2;
            this.lblHeading.Text = "Test Scenarios";
            // 
            // txtResult
            // 
            this.txtResult.BackColor = System.Drawing.Color.White;
            this.txtResult.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResult.Location = new System.Drawing.Point(9, 445);
            this.txtResult.Multiline = true;
            this.txtResult.Name = "txtResult";
            this.txtResult.ReadOnly = true;
            this.txtResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtResult.Size = new System.Drawing.Size(751, 70);
            this.txtResult.TabIndex = 5;
            // 
            // lblSeperatorLine1
            // 
            this.lblSeperatorLine1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeperatorLine1.Location = new System.Drawing.Point(-8, 52);
            this.lblSeperatorLine1.Name = "lblSeperatorLine1";
            this.lblSeperatorLine1.Size = new System.Drawing.Size(800, 2);
            this.lblSeperatorLine1.TabIndex = 7;
            // 
            // tabTestCases
            // 
            this.tabTestCases.Controls.Add(this.tabHDXReady);
            this.tabTestCases.Controls.Add(this.tabHDX3DPro);
            this.tabTestCases.Controls.Add(this.tabHDXPremium);
            this.tabTestCases.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabTestCases.Location = new System.Drawing.Point(9, 81);
            this.tabTestCases.Name = "tabTestCases";
            this.tabTestCases.SelectedIndex = 0;
            this.tabTestCases.Size = new System.Drawing.Size(755, 362);
            this.tabTestCases.TabIndex = 9;
            this.tabTestCases.SelectedIndexChanged += new System.EventHandler(this.tabTestCases_SelectedIndexChanged);
            // 
            // tabHDXReady
            // 
            this.tabHDXReady.BackColor = System.Drawing.Color.White;
            this.tabHDXReady.Controls.Add(this.tlpHDXReady);
            this.tabHDXReady.Location = new System.Drawing.Point(4, 28);
            this.tabHDXReady.Name = "tabHDXReady";
            this.tabHDXReady.Padding = new System.Windows.Forms.Padding(3);
            this.tabHDXReady.Size = new System.Drawing.Size(747, 330);
            this.tabHDXReady.TabIndex = 0;
            this.tabHDXReady.Text = " Standard ";
            // 
            // tlpHDXReady
            // 
            this.tlpHDXReady.ColumnCount = 5;
            this.tlpHDXReady.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.tlpHDXReady.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7F));
            this.tlpHDXReady.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51F));
            this.tlpHDXReady.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24F));
            this.tlpHDXReady.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14F));
            this.tlpHDXReady.Controls.Add(this.chkTestCase7, 0, 7);
            this.tlpHDXReady.Controls.Add(this.lblNumNetScaler, 1, 8);
            this.tlpHDXReady.Controls.Add(this.lblRequiredNetScaler, 3, 8);
            this.tlpHDXReady.Controls.Add(this.lblColHead4, 4, 0);
            this.tlpHDXReady.Controls.Add(this.lblStatus1, 4, 1);
            this.tlpHDXReady.Controls.Add(this.lblStatus2, 4, 2);
            this.tlpHDXReady.Controls.Add(this.lblStatus3, 4, 3);
            this.tlpHDXReady.Controls.Add(this.lblStatus4, 4, 4);
            this.tlpHDXReady.Controls.Add(this.lblStatus5, 4, 5);
            this.tlpHDXReady.Controls.Add(this.lblStatus6, 4, 6);
            this.tlpHDXReady.Controls.Add(this.lblRequired1, 3, 1);
            this.tlpHDXReady.Controls.Add(this.lblRequired2, 3, 2);
            this.tlpHDXReady.Controls.Add(this.lblStatus7, 4, 7);
            this.tlpHDXReady.Controls.Add(this.label3, 3, 7);
            this.tlpHDXReady.Controls.Add(this.pnlTestCase7, 2, 7);
            this.tlpHDXReady.Controls.Add(this.lblRequired3, 3, 3);
            this.tlpHDXReady.Controls.Add(this.lblRequired4, 3, 4);
            this.tlpHDXReady.Controls.Add(this.lblRequired5, 3, 5);
            this.tlpHDXReady.Controls.Add(this.lblRequired6, 3, 6);
            this.tlpHDXReady.Controls.Add(this.lblColHead2, 2, 0);
            this.tlpHDXReady.Controls.Add(this.lblSlNum1, 1, 1);
            this.tlpHDXReady.Controls.Add(this.lblSlNum2, 1, 2);
            this.tlpHDXReady.Controls.Add(this.lblSlNum3, 1, 3);
            this.tlpHDXReady.Controls.Add(this.lblSlNum4, 1, 4);
            this.tlpHDXReady.Controls.Add(this.lblSlNum5, 1, 5);
            this.tlpHDXReady.Controls.Add(this.lblSlNum6, 1, 6);
            this.tlpHDXReady.Controls.Add(this.lblColHead1, 1, 0);
            this.tlpHDXReady.Controls.Add(this.chkTestCase1, 0, 1);
            this.tlpHDXReady.Controls.Add(this.chkTestCase2, 0, 2);
            this.tlpHDXReady.Controls.Add(this.chkTestCase3, 0, 3);
            this.tlpHDXReady.Controls.Add(this.chkTestCase5, 0, 5);
            this.tlpHDXReady.Controls.Add(this.chkTestCase6, 0, 6);
            this.tlpHDXReady.Controls.Add(this.chkTestCaseHDXReady, 0, 0);
            this.tlpHDXReady.Controls.Add(this.pnlTestCase1, 2, 1);
            this.tlpHDXReady.Controls.Add(this.pnlTestCase2, 2, 2);
            this.tlpHDXReady.Controls.Add(this.lblColHead3, 3, 0);
            this.tlpHDXReady.Controls.Add(this.pnlTestCase3, 2, 3);
            this.tlpHDXReady.Controls.Add(this.pnlTestCase5, 2, 5);
            this.tlpHDXReady.Controls.Add(this.pnlTestCase6, 2, 6);
            this.tlpHDXReady.Controls.Add(this.lblSlNum7, 1, 7);
            this.tlpHDXReady.Controls.Add(this.pnlTestCase8, 2, 8);
            this.tlpHDXReady.Controls.Add(this.chkTestCase8, 0, 8);
            this.tlpHDXReady.Controls.Add(this.lblStatus8, 4, 8);
            this.tlpHDXReady.Controls.Add(this.checkBox1, 0, 4);
            this.tlpHDXReady.Controls.Add(this.pnlTestCase4, 2, 4);
            this.tlpHDXReady.Location = new System.Drawing.Point(6, 24);
            this.tlpHDXReady.Name = "tlpHDXReady";
            this.tlpHDXReady.RowCount = 10;
            this.tlpHDXReady.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpHDXReady.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tlpHDXReady.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tlpHDXReady.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpHDXReady.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpHDXReady.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpHDXReady.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tlpHDXReady.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tlpHDXReady.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tlpHDXReady.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 11F));
            this.tlpHDXReady.Size = new System.Drawing.Size(745, 310);
            this.tlpHDXReady.TabIndex = 9;
            // 
            // chkTestCase7
            // 
            this.chkTestCase7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase7.AutoSize = true;
            this.chkTestCase7.Checked = true;
            this.chkTestCase7.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTestCase7.Location = new System.Drawing.Point(11, 246);
            this.chkTestCase7.Name = "chkTestCase7";
            this.chkTestCase7.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase7.TabIndex = 22;
            this.chkTestCase7.UseVisualStyleBackColor = true;
            // 
            // lblNumNetScaler
            // 
            this.lblNumNetScaler.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNumNetScaler.AutoSize = true;
            this.lblNumNetScaler.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumNetScaler.Location = new System.Drawing.Point(47, 275);
            this.lblNumNetScaler.Name = "lblNumNetScaler";
            this.lblNumNetScaler.Size = new System.Drawing.Size(15, 18);
            this.lblNumNetScaler.TabIndex = 17;
            this.lblNumNetScaler.Text = "8";
            // 
            // lblRequiredNetScaler
            // 
            this.lblRequiredNetScaler.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRequiredNetScaler.AutoSize = true;
            this.lblRequiredNetScaler.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequiredNetScaler.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblRequiredNetScaler.Location = new System.Drawing.Point(463, 275);
            this.lblRequiredNetScaler.Name = "lblRequiredNetScaler";
            this.lblRequiredNetScaler.Size = new System.Drawing.Size(75, 18);
            this.lblRequiredNetScaler.TabIndex = 19;
            this.lblRequiredNetScaler.Text = "Mandatory";
            // 
            // lblColHead4
            // 
            this.lblColHead4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblColHead4.AutoSize = true;
            this.lblColHead4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColHead4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead4.Location = new System.Drawing.Point(641, 5);
            this.lblColHead4.Name = "lblColHead4";
            this.lblColHead4.Size = new System.Drawing.Size(52, 19);
            this.lblColHead4.TabIndex = 0;
            this.lblColHead4.Text = "Status";
            // 
            // lblStatus1
            // 
            this.lblStatus1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus1.AutoSize = true;
            this.lblStatus1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus1.Location = new System.Drawing.Point(641, 41);
            this.lblStatus1.Name = "lblStatus1";
            this.lblStatus1.Size = new System.Drawing.Size(87, 18);
            this.lblStatus1.TabIndex = 1;
            this.lblStatus1.Text = "Not Checked";
            // 
            // lblStatus2
            // 
            this.lblStatus2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus2.AutoSize = true;
            this.lblStatus2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus2.Location = new System.Drawing.Point(641, 78);
            this.lblStatus2.Name = "lblStatus2";
            this.lblStatus2.Size = new System.Drawing.Size(87, 18);
            this.lblStatus2.TabIndex = 1;
            this.lblStatus2.Text = "Not Checked";
            // 
            // lblStatus3
            // 
            this.lblStatus3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus3.AutoSize = true;
            this.lblStatus3.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus3.Location = new System.Drawing.Point(641, 111);
            this.lblStatus3.Name = "lblStatus3";
            this.lblStatus3.Size = new System.Drawing.Size(87, 18);
            this.lblStatus3.TabIndex = 1;
            this.lblStatus3.Text = "Not Checked";
            // 
            // lblStatus4
            // 
            this.lblStatus4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus4.AutoSize = true;
            this.lblStatus4.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus4.Location = new System.Drawing.Point(641, 144);
            this.lblStatus4.Name = "lblStatus4";
            this.lblStatus4.Size = new System.Drawing.Size(87, 18);
            this.lblStatus4.TabIndex = 1;
            this.lblStatus4.Text = "Not Checked";
            // 
            // lblStatus5
            // 
            this.lblStatus5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus5.AutoSize = true;
            this.lblStatus5.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus5.Location = new System.Drawing.Point(641, 174);
            this.lblStatus5.Name = "lblStatus5";
            this.lblStatus5.Size = new System.Drawing.Size(87, 18);
            this.lblStatus5.TabIndex = 1;
            this.lblStatus5.Text = "Not Checked";
            // 
            // lblStatus6
            // 
            this.lblStatus6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus6.AutoSize = true;
            this.lblStatus6.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus6.Location = new System.Drawing.Point(641, 207);
            this.lblStatus6.Name = "lblStatus6";
            this.lblStatus6.Size = new System.Drawing.Size(87, 18);
            this.lblStatus6.TabIndex = 1;
            this.lblStatus6.Text = "Not Checked";
            // 
            // lblRequired1
            // 
            this.lblRequired1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRequired1.AutoSize = true;
            this.lblRequired1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequired1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblRequired1.Location = new System.Drawing.Point(463, 41);
            this.lblRequired1.Name = "lblRequired1";
            this.lblRequired1.Size = new System.Drawing.Size(75, 18);
            this.lblRequired1.TabIndex = 2;
            this.lblRequired1.Text = "Mandatory";
            // 
            // lblRequired2
            // 
            this.lblRequired2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRequired2.AutoSize = true;
            this.lblRequired2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequired2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblRequired2.Location = new System.Drawing.Point(463, 78);
            this.lblRequired2.Name = "lblRequired2";
            this.lblRequired2.Size = new System.Drawing.Size(75, 18);
            this.lblRequired2.TabIndex = 2;
            this.lblRequired2.Text = "Mandatory";
            // 
            // lblStatus7
            // 
            this.lblStatus7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus7.AutoSize = true;
            this.lblStatus7.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus7.Location = new System.Drawing.Point(641, 244);
            this.lblStatus7.Name = "lblStatus7";
            this.lblStatus7.Size = new System.Drawing.Size(87, 18);
            this.lblStatus7.TabIndex = 26;
            this.lblStatus7.Text = "Not Checked";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.label3.Location = new System.Drawing.Point(463, 244);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 18);
            this.label3.TabIndex = 25;
            this.label3.Text = "Mandatory";
            // 
            // pnlTestCase7
            // 
            this.pnlTestCase7.Controls.Add(this.picboxTestCase7);
            this.pnlTestCase7.Controls.Add(this.lblTestCase7);
            this.pnlTestCase7.Location = new System.Drawing.Point(84, 238);
            this.pnlTestCase7.Name = "pnlTestCase7";
            this.pnlTestCase7.Size = new System.Drawing.Size(368, 30);
            this.pnlTestCase7.TabIndex = 24;
            // 
            // picboxTestCase7
            // 
            this.picboxTestCase7.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase7.Image")));
            this.picboxTestCase7.Location = new System.Drawing.Point(348, 4);
            this.picboxTestCase7.Name = "picboxTestCase7";
            this.picboxTestCase7.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase7.TabIndex = 1;
            this.picboxTestCase7.TabStop = false;
            // 
            // lblTestCase7
            // 
            this.lblTestCase7.AutoSize = true;
            this.lblTestCase7.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase7.Location = new System.Drawing.Point(2, 3);
            this.lblTestCase7.Name = "lblTestCase7";
            this.lblTestCase7.Size = new System.Drawing.Size(340, 18);
            this.lblTestCase7.TabIndex = 12;
            this.lblTestCase7.Text = "Client Printer Test Using Citrix Universal Printer Driver";
            // 
            // lblRequired3
            // 
            this.lblRequired3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRequired3.AutoSize = true;
            this.lblRequired3.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequired3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblRequired3.Location = new System.Drawing.Point(463, 111);
            this.lblRequired3.Name = "lblRequired3";
            this.lblRequired3.Size = new System.Drawing.Size(75, 18);
            this.lblRequired3.TabIndex = 2;
            this.lblRequired3.Text = "Mandatory";
            // 
            // lblRequired4
            // 
            this.lblRequired4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRequired4.AutoSize = true;
            this.lblRequired4.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequired4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblRequired4.Location = new System.Drawing.Point(463, 144);
            this.lblRequired4.Name = "lblRequired4";
            this.lblRequired4.Size = new System.Drawing.Size(75, 18);
            this.lblRequired4.TabIndex = 2;
            this.lblRequired4.Text = "Mandatory";
            // 
            // lblRequired5
            // 
            this.lblRequired5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRequired5.AutoSize = true;
            this.lblRequired5.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequired5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblRequired5.Location = new System.Drawing.Point(463, 174);
            this.lblRequired5.Name = "lblRequired5";
            this.lblRequired5.Size = new System.Drawing.Size(75, 18);
            this.lblRequired5.TabIndex = 2;
            this.lblRequired5.Text = "Mandatory";
            // 
            // lblRequired6
            // 
            this.lblRequired6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRequired6.AutoSize = true;
            this.lblRequired6.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequired6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblRequired6.Location = new System.Drawing.Point(463, 207);
            this.lblRequired6.Name = "lblRequired6";
            this.lblRequired6.Size = new System.Drawing.Size(75, 18);
            this.lblRequired6.TabIndex = 2;
            this.lblRequired6.Text = "Mandatory";
            // 
            // lblColHead2
            // 
            this.lblColHead2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblColHead2.AutoSize = true;
            this.lblColHead2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColHead2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead2.Location = new System.Drawing.Point(84, 5);
            this.lblColHead2.Name = "lblColHead2";
            this.lblColHead2.Size = new System.Drawing.Size(105, 19);
            this.lblColHead2.TabIndex = 0;
            this.lblColHead2.Text = "Test Scenarios";
            // 
            // lblSlNum1
            // 
            this.lblSlNum1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum1.AutoSize = true;
            this.lblSlNum1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum1.Location = new System.Drawing.Point(47, 41);
            this.lblSlNum1.Name = "lblSlNum1";
            this.lblSlNum1.Size = new System.Drawing.Size(15, 18);
            this.lblSlNum1.TabIndex = 1;
            this.lblSlNum1.Text = "1";
            // 
            // lblSlNum2
            // 
            this.lblSlNum2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum2.AutoSize = true;
            this.lblSlNum2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum2.Location = new System.Drawing.Point(47, 78);
            this.lblSlNum2.Name = "lblSlNum2";
            this.lblSlNum2.Size = new System.Drawing.Size(15, 18);
            this.lblSlNum2.TabIndex = 1;
            this.lblSlNum2.Text = "2";
            // 
            // lblSlNum3
            // 
            this.lblSlNum3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum3.AutoSize = true;
            this.lblSlNum3.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum3.Location = new System.Drawing.Point(47, 111);
            this.lblSlNum3.Name = "lblSlNum3";
            this.lblSlNum3.Size = new System.Drawing.Size(15, 18);
            this.lblSlNum3.TabIndex = 1;
            this.lblSlNum3.Text = "3";
            // 
            // lblSlNum4
            // 
            this.lblSlNum4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum4.AutoSize = true;
            this.lblSlNum4.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum4.Location = new System.Drawing.Point(47, 144);
            this.lblSlNum4.Name = "lblSlNum4";
            this.lblSlNum4.Size = new System.Drawing.Size(15, 18);
            this.lblSlNum4.TabIndex = 1;
            this.lblSlNum4.Text = "4";
            // 
            // lblSlNum5
            // 
            this.lblSlNum5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum5.AutoSize = true;
            this.lblSlNum5.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum5.Location = new System.Drawing.Point(47, 174);
            this.lblSlNum5.Name = "lblSlNum5";
            this.lblSlNum5.Size = new System.Drawing.Size(15, 18);
            this.lblSlNum5.TabIndex = 1;
            this.lblSlNum5.Text = "5";
            // 
            // lblSlNum6
            // 
            this.lblSlNum6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum6.AutoSize = true;
            this.lblSlNum6.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum6.Location = new System.Drawing.Point(47, 207);
            this.lblSlNum6.Name = "lblSlNum6";
            this.lblSlNum6.Size = new System.Drawing.Size(15, 18);
            this.lblSlNum6.TabIndex = 1;
            this.lblSlNum6.Text = "6";
            // 
            // lblColHead1
            // 
            this.lblColHead1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblColHead1.AutoSize = true;
            this.lblColHead1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColHead1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead1.Location = new System.Drawing.Point(32, 5);
            this.lblColHead1.Name = "lblColHead1";
            this.lblColHead1.Size = new System.Drawing.Size(35, 19);
            this.lblColHead1.TabIndex = 0;
            this.lblColHead1.Text = "S/N";
            // 
            // chkTestCase1
            // 
            this.chkTestCase1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase1.AutoSize = true;
            this.chkTestCase1.Checked = true;
            this.chkTestCase1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTestCase1.Location = new System.Drawing.Point(11, 43);
            this.chkTestCase1.Name = "chkTestCase1";
            this.chkTestCase1.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase1.TabIndex = 3;
            this.chkTestCase1.UseVisualStyleBackColor = true;
            // 
            // chkTestCase2
            // 
            this.chkTestCase2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase2.AutoSize = true;
            this.chkTestCase2.Checked = true;
            this.chkTestCase2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTestCase2.Location = new System.Drawing.Point(11, 80);
            this.chkTestCase2.Name = "chkTestCase2";
            this.chkTestCase2.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase2.TabIndex = 3;
            this.chkTestCase2.UseVisualStyleBackColor = true;
            // 
            // chkTestCase3
            // 
            this.chkTestCase3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase3.AutoSize = true;
            this.chkTestCase3.Checked = true;
            this.chkTestCase3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTestCase3.Location = new System.Drawing.Point(11, 113);
            this.chkTestCase3.Name = "chkTestCase3";
            this.chkTestCase3.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase3.TabIndex = 3;
            this.chkTestCase3.UseVisualStyleBackColor = true;
            // 
            // chkTestCase5
            // 
            this.chkTestCase5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase5.AutoSize = true;
            this.chkTestCase5.Checked = true;
            this.chkTestCase5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTestCase5.Location = new System.Drawing.Point(11, 176);
            this.chkTestCase5.Name = "chkTestCase5";
            this.chkTestCase5.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase5.TabIndex = 3;
            this.chkTestCase5.UseVisualStyleBackColor = true;
            // 
            // chkTestCase6
            // 
            this.chkTestCase6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase6.AutoSize = true;
            this.chkTestCase6.Checked = true;
            this.chkTestCase6.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTestCase6.Location = new System.Drawing.Point(11, 209);
            this.chkTestCase6.Name = "chkTestCase6";
            this.chkTestCase6.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase6.TabIndex = 3;
            this.chkTestCase6.UseVisualStyleBackColor = true;
            // 
            // chkTestCaseHDXReady
            // 
            this.chkTestCaseHDXReady.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCaseHDXReady.AutoSize = true;
            this.chkTestCaseHDXReady.Checked = true;
            this.chkTestCaseHDXReady.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTestCaseHDXReady.Location = new System.Drawing.Point(11, 8);
            this.chkTestCaseHDXReady.Name = "chkTestCaseHDXReady";
            this.chkTestCaseHDXReady.Size = new System.Drawing.Size(15, 14);
            this.chkTestCaseHDXReady.TabIndex = 3;
            this.chkTestCaseHDXReady.UseVisualStyleBackColor = true;
            this.chkTestCaseHDXReady.CheckedChanged += new System.EventHandler(this.chkTestCaseHDXReady_CheckedChanged);
            // 
            // pnlTestCase1
            // 
            this.pnlTestCase1.Controls.Add(this.lblprocessing);
            this.pnlTestCase1.Controls.Add(this.picboxTestCase1);
            this.pnlTestCase1.Controls.Add(this.lblTestCase1);
            this.pnlTestCase1.Location = new System.Drawing.Point(84, 33);
            this.pnlTestCase1.Name = "pnlTestCase1";
            this.pnlTestCase1.Size = new System.Drawing.Size(373, 31);
            this.pnlTestCase1.TabIndex = 4;
            // 
            // lblprocessing
            // 
            this.lblprocessing.BackColor = System.Drawing.SystemColors.HighlightText;
            this.lblprocessing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblprocessing.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblprocessing.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblprocessing.Location = new System.Drawing.Point(-6, -8);
            this.lblprocessing.Margin = new System.Windows.Forms.Padding(13, 10, 13, 10);
            this.lblprocessing.Name = "lblprocessing";
            this.lblprocessing.Size = new System.Drawing.Size(595, 45);
            this.lblprocessing.TabIndex = 0;
            this.lblprocessing.Text = "label2";
            this.lblprocessing.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picboxTestCase1
            // 
            this.picboxTestCase1.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase1.Image")));
            this.picboxTestCase1.Location = new System.Drawing.Point(284, 3);
            this.picboxTestCase1.Name = "picboxTestCase1";
            this.picboxTestCase1.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase1.TabIndex = 2;
            this.picboxTestCase1.TabStop = false;
            // 
            // lblTestCase1
            // 
            this.lblTestCase1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase1.AutoSize = true;
            this.lblTestCase1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase1.Location = new System.Drawing.Point(4, -1);
            this.lblTestCase1.Name = "lblTestCase1";
            this.lblTestCase1.Size = new System.Drawing.Size(305, 36);
            this.lblTestCase1.TabIndex = 1;
            this.lblTestCase1.Text = "Integration and support of Citrix Cloud Services, \r\nCitrix Workspace app and Citr" +
    "ix Workspace";
            // 
            // pnlTestCase2
            // 
            this.pnlTestCase2.Controls.Add(this.picboxTestCase2);
            this.pnlTestCase2.Controls.Add(this.lblTestCase2);
            this.pnlTestCase2.Location = new System.Drawing.Point(84, 74);
            this.pnlTestCase2.Name = "pnlTestCase2";
            this.pnlTestCase2.Size = new System.Drawing.Size(368, 20);
            this.pnlTestCase2.TabIndex = 5;
            // 
            // picboxTestCase2
            // 
            this.picboxTestCase2.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase2.Image")));
            this.picboxTestCase2.Location = new System.Drawing.Point(182, 3);
            this.picboxTestCase2.Name = "picboxTestCase2";
            this.picboxTestCase2.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase2.TabIndex = 2;
            this.picboxTestCase2.TabStop = false;
            // 
            // lblTestCase2
            // 
            this.lblTestCase2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase2.AutoSize = true;
            this.lblTestCase2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase2.Location = new System.Drawing.Point(4, 6);
            this.lblTestCase2.Name = "lblTestCase2";
            this.lblTestCase2.Size = new System.Drawing.Size(259, 18);
            this.lblTestCase2.TabIndex = 1;
            this.lblTestCase2.Text = "Citrix File access and Experience Support";
            // 
            // lblColHead3
            // 
            this.lblColHead3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblColHead3.AutoSize = true;
            this.lblColHead3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColHead3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead3.Location = new System.Drawing.Point(463, 5);
            this.lblColHead3.Name = "lblColHead3";
            this.lblColHead3.Size = new System.Drawing.Size(160, 19);
            this.lblColHead3.TabIndex = 0;
            this.lblColHead3.Text = "Mandatory / Optional";
            // 
            // pnlTestCase3
            // 
            this.pnlTestCase3.Controls.Add(this.picboxTestCase3);
            this.pnlTestCase3.Controls.Add(this.lblTestCase3);
            this.pnlTestCase3.Location = new System.Drawing.Point(84, 106);
            this.pnlTestCase3.Name = "pnlTestCase3";
            this.pnlTestCase3.Size = new System.Drawing.Size(368, 22);
            this.pnlTestCase3.TabIndex = 6;
            // 
            // picboxTestCase3
            // 
            this.picboxTestCase3.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase3.Image")));
            this.picboxTestCase3.Location = new System.Drawing.Point(242, 3);
            this.picboxTestCase3.Name = "picboxTestCase3";
            this.picboxTestCase3.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase3.TabIndex = 2;
            this.picboxTestCase3.TabStop = false;
            // 
            // lblTestCase3
            // 
            this.lblTestCase3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase3.AutoSize = true;
            this.lblTestCase3.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase3.Location = new System.Drawing.Point(4, 7);
            this.lblTestCase3.Name = "lblTestCase3";
            this.lblTestCase3.Size = new System.Drawing.Size(313, 18);
            this.lblTestCase3.TabIndex = 1;
            this.lblTestCase3.Text = "2D Graphics - Server Rendered Virtual Application";
            this.ttHelp.SetToolTip(this.lblTestCase3, "This test case will verify your endpoint’s support for 2D Graphics - Server Rende" +
        "red Virtual Application.");
            // 
            // pnlTestCase5
            // 
            this.pnlTestCase5.Controls.Add(this.picboxTestCase5);
            this.pnlTestCase5.Controls.Add(this.lblTestCase5);
            this.pnlTestCase5.Location = new System.Drawing.Point(84, 171);
            this.pnlTestCase5.Name = "pnlTestCase5";
            this.pnlTestCase5.Size = new System.Drawing.Size(368, 24);
            this.pnlTestCase5.TabIndex = 8;
            // 
            // picboxTestCase5
            // 
            this.picboxTestCase5.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase5.Image")));
            this.picboxTestCase5.Location = new System.Drawing.Point(326, 5);
            this.picboxTestCase5.Name = "picboxTestCase5";
            this.picboxTestCase5.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase5.TabIndex = 3;
            this.picboxTestCase5.TabStop = false;
            // 
            // lblTestCase5
            // 
            this.lblTestCase5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase5.AutoSize = true;
            this.lblTestCase5.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase5.Location = new System.Drawing.Point(3, 0);
            this.lblTestCase5.Name = "lblTestCase5";
            this.lblTestCase5.Size = new System.Drawing.Size(245, 18);
            this.lblTestCase5.TabIndex = 1;
            this.lblTestCase5.Text = "Audio Recording and Playback Support";
            // 
            // pnlTestCase6
            // 
            this.pnlTestCase6.Controls.Add(this.picboxTestCase6);
            this.pnlTestCase6.Controls.Add(this.lblTestCase6);
            this.pnlTestCase6.Location = new System.Drawing.Point(84, 201);
            this.pnlTestCase6.Name = "pnlTestCase6";
            this.pnlTestCase6.Size = new System.Drawing.Size(368, 31);
            this.pnlTestCase6.TabIndex = 9;
            // 
            // picboxTestCase6
            // 
            this.picboxTestCase6.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase6.Image")));
            this.picboxTestCase6.Location = new System.Drawing.Point(326, 4);
            this.picboxTestCase6.Name = "picboxTestCase6";
            this.picboxTestCase6.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase6.TabIndex = 3;
            this.picboxTestCase6.TabStop = false;
            // 
            // lblTestCase6
            // 
            this.lblTestCase6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase6.AutoSize = true;
            this.lblTestCase6.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase6.Location = new System.Drawing.Point(4, -2);
            this.lblTestCase6.Name = "lblTestCase6";
            this.lblTestCase6.Size = new System.Drawing.Size(315, 36);
            this.lblTestCase6.TabIndex = 1;
            this.lblTestCase6.Text = "Video Playback - Server Renderd Windows Media \r\nRedirection";
            // 
            // lblSlNum7
            // 
            this.lblSlNum7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum7.AutoSize = true;
            this.lblSlNum7.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum7.Location = new System.Drawing.Point(47, 244);
            this.lblSlNum7.Name = "lblSlNum7";
            this.lblSlNum7.Size = new System.Drawing.Size(15, 18);
            this.lblSlNum7.TabIndex = 11;
            this.lblSlNum7.Text = "7";
            // 
            // pnlTestCase8
            // 
            this.pnlTestCase8.Controls.Add(this.picboxTestCase8);
            this.pnlTestCase8.Controls.Add(this.lblTestCase8);
            this.pnlTestCase8.Location = new System.Drawing.Point(84, 274);
            this.pnlTestCase8.Name = "pnlTestCase8";
            this.pnlTestCase8.Size = new System.Drawing.Size(368, 20);
            this.pnlTestCase8.TabIndex = 13;
            // 
            // picboxTestCase8
            // 
            this.picboxTestCase8.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase8.Image")));
            this.picboxTestCase8.Location = new System.Drawing.Point(182, 5);
            this.picboxTestCase8.Name = "picboxTestCase8";
            this.picboxTestCase8.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase8.TabIndex = 1;
            this.picboxTestCase8.TabStop = false;
            // 
            // lblTestCase8
            // 
            this.lblTestCase8.AutoSize = true;
            this.lblTestCase8.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase8.Location = new System.Drawing.Point(2, 3);
            this.lblTestCase8.Name = "lblTestCase8";
            this.lblTestCase8.Size = new System.Drawing.Size(200, 18);
            this.lblTestCase8.TabIndex = 12;
            this.lblTestCase8.Text = "Plug n Play Devices Redirection";
            // 
            // chkTestCase8
            // 
            this.chkTestCase8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase8.AutoSize = true;
            this.chkTestCase8.Checked = true;
            this.chkTestCase8.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTestCase8.Location = new System.Drawing.Point(11, 277);
            this.chkTestCase8.Name = "chkTestCase8";
            this.chkTestCase8.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase8.TabIndex = 10;
            this.chkTestCase8.UseVisualStyleBackColor = true;
            // 
            // lblStatus8
            // 
            this.lblStatus8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus8.AutoSize = true;
            this.lblStatus8.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus8.Location = new System.Drawing.Point(641, 275);
            this.lblStatus8.Name = "lblStatus8";
            this.lblStatus8.Size = new System.Drawing.Size(87, 18);
            this.lblStatus8.TabIndex = 15;
            this.lblStatus8.Text = "Not Checked";
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(11, 146);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 27;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // pnlTestCase4
            // 
            this.pnlTestCase4.Controls.Add(this.picboxTestCase4);
            this.pnlTestCase4.Controls.Add(this.lblTestCase4);
            this.pnlTestCase4.Location = new System.Drawing.Point(84, 141);
            this.pnlTestCase4.Name = "pnlTestCase4";
            this.pnlTestCase4.Size = new System.Drawing.Size(368, 23);
            this.pnlTestCase4.TabIndex = 7;
            // 
            // picboxTestCase4
            // 
            this.picboxTestCase4.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase4.Image")));
            this.picboxTestCase4.Location = new System.Drawing.Point(246, 4);
            this.picboxTestCase4.Name = "picboxTestCase4";
            this.picboxTestCase4.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase4.TabIndex = 3;
            this.picboxTestCase4.TabStop = false;
            // 
            // lblTestCase4
            // 
            this.lblTestCase4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase4.AutoSize = true;
            this.lblTestCase4.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase4.Location = new System.Drawing.Point(4, 4);
            this.lblTestCase4.Name = "lblTestCase4";
            this.lblTestCase4.Size = new System.Drawing.Size(255, 18);
            this.lblTestCase4.TabIndex = 1;
            this.lblTestCase4.Text = "Access Secure SaaS and Web Application";
            // 
            // tabHDX3DPro
            // 
            this.tabHDX3DPro.BackColor = System.Drawing.Color.White;
            this.tabHDX3DPro.Controls.Add(this.tlpHDX3DPro);
            this.tabHDX3DPro.Location = new System.Drawing.Point(4, 28);
            this.tabHDX3DPro.Name = "tabHDX3DPro";
            this.tabHDX3DPro.Padding = new System.Windows.Forms.Padding(3);
            this.tabHDX3DPro.Size = new System.Drawing.Size(747, 330);
            this.tabHDX3DPro.TabIndex = 2;
            this.tabHDX3DPro.Text = " Standard Contd ";
            // 
            // tlpHDX3DPro
            // 
            this.tlpHDX3DPro.ColumnCount = 5;
            this.tlpHDX3DPro.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.tlpHDX3DPro.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7F));
            this.tlpHDX3DPro.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51F));
            this.tlpHDX3DPro.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24F));
            this.tlpHDX3DPro.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14F));
            this.tlpHDX3DPro.Controls.Add(this.pnlTestCase15, 2, 7);
            this.tlpHDX3DPro.Controls.Add(this.pnlTestCase14, 2, 6);
            this.tlpHDX3DPro.Controls.Add(this.pnlTestCase13, 2, 5);
            this.tlpHDX3DPro.Controls.Add(this.lblStatus15, 4, 7);
            this.tlpHDX3DPro.Controls.Add(this.lblStatus14, 4, 6);
            this.tlpHDX3DPro.Controls.Add(this.lblStatus13, 4, 5);
            this.tlpHDX3DPro.Controls.Add(this.label8, 3, 6);
            this.tlpHDX3DPro.Controls.Add(this.label6, 3, 5);
            this.tlpHDX3DPro.Controls.Add(this.lblColHead4HDX3DPro, 4, 0);
            this.tlpHDX3DPro.Controls.Add(this.lblStatus9, 4, 1);
            this.tlpHDX3DPro.Controls.Add(this.lblStatus10, 4, 2);
            this.tlpHDX3DPro.Controls.Add(this.lblStatus11, 4, 3);
            this.tlpHDX3DPro.Controls.Add(this.lblStatus12, 4, 4);
            this.tlpHDX3DPro.Controls.Add(this.lblRequired15, 3, 1);
            this.tlpHDX3DPro.Controls.Add(this.lblRequired16, 3, 2);
            this.tlpHDX3DPro.Controls.Add(this.lblRequired17, 3, 3);
            this.tlpHDX3DPro.Controls.Add(this.lblRequired18, 3, 4);
            this.tlpHDX3DPro.Controls.Add(this.lblColHead2HDX3DPro, 2, 0);
            this.tlpHDX3DPro.Controls.Add(this.lblColHead1HDX3DPro, 1, 0);
            this.tlpHDX3DPro.Controls.Add(this.lblSlNum15, 1, 1);
            this.tlpHDX3DPro.Controls.Add(this.lblSlNum16, 1, 2);
            this.tlpHDX3DPro.Controls.Add(this.lblSlNum17, 1, 3);
            this.tlpHDX3DPro.Controls.Add(this.lblSlNum18, 1, 4);
            this.tlpHDX3DPro.Controls.Add(this.chkTestCase9, 0, 1);
            this.tlpHDX3DPro.Controls.Add(this.chkTestCase10, 0, 2);
            this.tlpHDX3DPro.Controls.Add(this.chkTestCase11, 0, 3);
            this.tlpHDX3DPro.Controls.Add(this.chkTestCase12, 0, 4);
            this.tlpHDX3DPro.Controls.Add(this.chkTestCaseHDX3DPro, 0, 0);
            this.tlpHDX3DPro.Controls.Add(this.lblColHead3HDX3DPro, 3, 0);
            this.tlpHDX3DPro.Controls.Add(this.pnlTestCase9, 2, 1);
            this.tlpHDX3DPro.Controls.Add(this.pnlTestCase10, 2, 2);
            this.tlpHDX3DPro.Controls.Add(this.pnlTestCase11, 2, 3);
            this.tlpHDX3DPro.Controls.Add(this.pnlTestCase12, 2, 4);
            this.tlpHDX3DPro.Controls.Add(this.chkTestCase13, 0, 5);
            this.tlpHDX3DPro.Controls.Add(this.chkTestCase14, 0, 6);
            this.tlpHDX3DPro.Controls.Add(this.chkTestCase15, 0, 7);
            this.tlpHDX3DPro.Controls.Add(this.label4, 1, 5);
            this.tlpHDX3DPro.Controls.Add(this.label5, 1, 6);
            this.tlpHDX3DPro.Controls.Add(this.label2, 1, 7);
            this.tlpHDX3DPro.Controls.Add(this.label7, 3, 7);
            this.tlpHDX3DPro.Controls.Add(this.chkTestCase21, 0, 8);
            this.tlpHDX3DPro.Controls.Add(this.lblSlNum13, 1, 8);
            this.tlpHDX3DPro.Controls.Add(this.lblRequired13, 3, 8);
            this.tlpHDX3DPro.Controls.Add(this.lblStatus21, 4, 8);
            this.tlpHDX3DPro.Controls.Add(this.pnlTestCase21, 2, 8);
            this.tlpHDX3DPro.Location = new System.Drawing.Point(6, 24);
            this.tlpHDX3DPro.Name = "tlpHDX3DPro";
            this.tlpHDX3DPro.RowCount = 8;
            this.tlpHDX3DPro.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpHDX3DPro.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpHDX3DPro.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tlpHDX3DPro.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tlpHDX3DPro.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tlpHDX3DPro.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tlpHDX3DPro.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tlpHDX3DPro.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tlpHDX3DPro.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 14F));
            this.tlpHDX3DPro.Size = new System.Drawing.Size(734, 306);
            this.tlpHDX3DPro.TabIndex = 11;
            // 
            // pnlTestCase15
            // 
            this.pnlTestCase15.Controls.Add(this.picboxTestCase15);
            this.pnlTestCase15.Controls.Add(this.lblTestCase15);
            this.pnlTestCase15.Location = new System.Drawing.Point(83, 228);
            this.pnlTestCase15.Name = "pnlTestCase15";
            this.pnlTestCase15.Size = new System.Drawing.Size(368, 43);
            this.pnlTestCase15.TabIndex = 27;
            // 
            // picboxTestCase15
            // 
            this.picboxTestCase15.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase15.Image")));
            this.picboxTestCase15.Location = new System.Drawing.Point(301, 6);
            this.picboxTestCase15.Name = "picboxTestCase15";
            this.picboxTestCase15.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase15.TabIndex = 2;
            this.picboxTestCase15.TabStop = false;
            // 
            // lblTestCase15
            // 
            this.lblTestCase15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase15.AutoSize = true;
            this.lblTestCase15.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase15.Location = new System.Drawing.Point(5, 6);
            this.lblTestCase15.Name = "lblTestCase15";
            this.lblTestCase15.Size = new System.Drawing.Size(290, 36);
            this.lblTestCase15.TabIndex = 5;
            this.lblTestCase15.Text = "HDX Traffic Management and Monitoring with \r\nCitrix ADM";
            // 
            // pnlTestCase14
            // 
            this.pnlTestCase14.Controls.Add(this.picboxTestCase14);
            this.pnlTestCase14.Controls.Add(this.lblTestCase14);
            this.pnlTestCase14.Location = new System.Drawing.Point(83, 195);
            this.pnlTestCase14.Name = "pnlTestCase14";
            this.pnlTestCase14.Size = new System.Drawing.Size(368, 27);
            this.pnlTestCase14.TabIndex = 26;
            // 
            // picboxTestCase14
            // 
            this.picboxTestCase14.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase14.Image")));
            this.picboxTestCase14.Location = new System.Drawing.Point(326, 2);
            this.picboxTestCase14.Name = "picboxTestCase14";
            this.picboxTestCase14.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase14.TabIndex = 2;
            this.picboxTestCase14.TabStop = false;
            // 
            // lblTestCase14
            // 
            this.lblTestCase14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase14.AutoSize = true;
            this.lblTestCase14.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase14.Location = new System.Drawing.Point(5, 6);
            this.lblTestCase14.Name = "lblTestCase14";
            this.lblTestCase14.Size = new System.Drawing.Size(316, 18);
            this.lblTestCase14.TabIndex = 4;
            this.lblTestCase14.Text = "Accelerated Traffic Support through Citrix SD-WAN";
            // 
            // pnlTestCase13
            // 
            this.pnlTestCase13.Controls.Add(this.lblTestCase13);
            this.pnlTestCase13.Controls.Add(this.picboxTestCase13);
            this.pnlTestCase13.Location = new System.Drawing.Point(83, 163);
            this.pnlTestCase13.Name = "pnlTestCase13";
            this.pnlTestCase13.Size = new System.Drawing.Size(357, 26);
            this.pnlTestCase13.TabIndex = 25;
            // 
            // lblTestCase13
            // 
            this.lblTestCase13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase13.AutoSize = true;
            this.lblTestCase13.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase13.Location = new System.Drawing.Point(4, 2);
            this.lblTestCase13.Name = "lblTestCase13";
            this.lblTestCase13.Size = new System.Drawing.Size(224, 18);
            this.lblTestCase13.TabIndex = 3;
            this.lblTestCase13.Text = "High-Definition Webcam Streaming";
            // 
            // picboxTestCase13
            // 
            this.picboxTestCase13.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase13.Image")));
            this.picboxTestCase13.Location = new System.Drawing.Point(237, 9);
            this.picboxTestCase13.Name = "picboxTestCase13";
            this.picboxTestCase13.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase13.TabIndex = 2;
            this.picboxTestCase13.TabStop = false;
            // 
            // lblStatus15
            // 
            this.lblStatus15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus15.AutoSize = true;
            this.lblStatus15.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus15.Location = new System.Drawing.Point(633, 240);
            this.lblStatus15.Name = "lblStatus15";
            this.lblStatus15.Size = new System.Drawing.Size(87, 18);
            this.lblStatus15.TabIndex = 24;
            this.lblStatus15.Text = "Not Checked";
            // 
            // lblStatus14
            // 
            this.lblStatus14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus14.AutoSize = true;
            this.lblStatus14.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus14.Location = new System.Drawing.Point(633, 199);
            this.lblStatus14.Name = "lblStatus14";
            this.lblStatus14.Size = new System.Drawing.Size(87, 18);
            this.lblStatus14.TabIndex = 23;
            this.lblStatus14.Text = "Not Checked";
            // 
            // lblStatus13
            // 
            this.lblStatus13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus13.AutoSize = true;
            this.lblStatus13.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus13.Location = new System.Drawing.Point(633, 167);
            this.lblStatus13.Name = "lblStatus13";
            this.lblStatus13.Size = new System.Drawing.Size(87, 18);
            this.lblStatus13.TabIndex = 22;
            this.lblStatus13.Text = "Not Checked";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.label8.Location = new System.Drawing.Point(457, 199);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 18);
            this.label8.TabIndex = 21;
            this.label8.Text = "Optional";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.label6.Location = new System.Drawing.Point(457, 167);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 18);
            this.label6.TabIndex = 19;
            this.label6.Text = "Mandatory";
            // 
            // lblColHead4HDX3DPro
            // 
            this.lblColHead4HDX3DPro.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblColHead4HDX3DPro.AutoSize = true;
            this.lblColHead4HDX3DPro.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColHead4HDX3DPro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead4HDX3DPro.Location = new System.Drawing.Point(633, 5);
            this.lblColHead4HDX3DPro.Name = "lblColHead4HDX3DPro";
            this.lblColHead4HDX3DPro.Size = new System.Drawing.Size(52, 19);
            this.lblColHead4HDX3DPro.TabIndex = 0;
            this.lblColHead4HDX3DPro.Text = "Status";
            // 
            // lblStatus9
            // 
            this.lblStatus9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus9.AutoSize = true;
            this.lblStatus9.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus9.Location = new System.Drawing.Point(633, 36);
            this.lblStatus9.Name = "lblStatus9";
            this.lblStatus9.Size = new System.Drawing.Size(87, 18);
            this.lblStatus9.TabIndex = 1;
            this.lblStatus9.Text = "Not Checked";
            // 
            // lblStatus10
            // 
            this.lblStatus10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus10.AutoSize = true;
            this.lblStatus10.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus10.Location = new System.Drawing.Point(633, 68);
            this.lblStatus10.Name = "lblStatus10";
            this.lblStatus10.Size = new System.Drawing.Size(87, 18);
            this.lblStatus10.TabIndex = 1;
            this.lblStatus10.Text = "Not Checked";
            // 
            // lblStatus11
            // 
            this.lblStatus11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus11.AutoSize = true;
            this.lblStatus11.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus11.Location = new System.Drawing.Point(633, 101);
            this.lblStatus11.Name = "lblStatus11";
            this.lblStatus11.Size = new System.Drawing.Size(87, 18);
            this.lblStatus11.TabIndex = 1;
            this.lblStatus11.Text = "Not Checked";
            // 
            // lblStatus12
            // 
            this.lblStatus12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus12.AutoSize = true;
            this.lblStatus12.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus12.Location = new System.Drawing.Point(633, 134);
            this.lblStatus12.Name = "lblStatus12";
            this.lblStatus12.Size = new System.Drawing.Size(87, 18);
            this.lblStatus12.TabIndex = 1;
            this.lblStatus12.Text = "Not Checked";
            // 
            // lblRequired15
            // 
            this.lblRequired15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRequired15.AutoSize = true;
            this.lblRequired15.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequired15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblRequired15.Location = new System.Drawing.Point(457, 36);
            this.lblRequired15.Name = "lblRequired15";
            this.lblRequired15.Size = new System.Drawing.Size(75, 18);
            this.lblRequired15.TabIndex = 6;
            this.lblRequired15.Text = "Mandatory";
            // 
            // lblRequired16
            // 
            this.lblRequired16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRequired16.AutoSize = true;
            this.lblRequired16.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequired16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblRequired16.Location = new System.Drawing.Point(457, 68);
            this.lblRequired16.Name = "lblRequired16";
            this.lblRequired16.Size = new System.Drawing.Size(75, 18);
            this.lblRequired16.TabIndex = 6;
            this.lblRequired16.Text = "Mandatory";
            // 
            // lblRequired17
            // 
            this.lblRequired17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRequired17.AutoSize = true;
            this.lblRequired17.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequired17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblRequired17.Location = new System.Drawing.Point(457, 101);
            this.lblRequired17.Name = "lblRequired17";
            this.lblRequired17.Size = new System.Drawing.Size(75, 18);
            this.lblRequired17.TabIndex = 6;
            this.lblRequired17.Text = "Mandatory";
            // 
            // lblRequired18
            // 
            this.lblRequired18.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRequired18.AutoSize = true;
            this.lblRequired18.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequired18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblRequired18.Location = new System.Drawing.Point(457, 134);
            this.lblRequired18.Name = "lblRequired18";
            this.lblRequired18.Size = new System.Drawing.Size(75, 18);
            this.lblRequired18.TabIndex = 6;
            this.lblRequired18.Text = "Mandatory";
            // 
            // lblColHead2HDX3DPro
            // 
            this.lblColHead2HDX3DPro.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblColHead2HDX3DPro.AutoSize = true;
            this.lblColHead2HDX3DPro.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColHead2HDX3DPro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead2HDX3DPro.Location = new System.Drawing.Point(83, 5);
            this.lblColHead2HDX3DPro.Name = "lblColHead2HDX3DPro";
            this.lblColHead2HDX3DPro.Size = new System.Drawing.Size(105, 19);
            this.lblColHead2HDX3DPro.TabIndex = 0;
            this.lblColHead2HDX3DPro.Text = "Test Scenarios";
            // 
            // lblColHead1HDX3DPro
            // 
            this.lblColHead1HDX3DPro.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblColHead1HDX3DPro.AutoSize = true;
            this.lblColHead1HDX3DPro.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColHead1HDX3DPro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead1HDX3DPro.Location = new System.Drawing.Point(32, 5);
            this.lblColHead1HDX3DPro.Name = "lblColHead1HDX3DPro";
            this.lblColHead1HDX3DPro.Size = new System.Drawing.Size(35, 19);
            this.lblColHead1HDX3DPro.TabIndex = 0;
            this.lblColHead1HDX3DPro.Text = "S/N";
            // 
            // lblSlNum15
            // 
            this.lblSlNum15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum15.AutoSize = true;
            this.lblSlNum15.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum15.ForeColor = System.Drawing.Color.Black;
            this.lblSlNum15.Location = new System.Drawing.Point(47, 36);
            this.lblSlNum15.Name = "lblSlNum15";
            this.lblSlNum15.Size = new System.Drawing.Size(15, 18);
            this.lblSlNum15.TabIndex = 1;
            this.lblSlNum15.Text = "9";
            // 
            // lblSlNum16
            // 
            this.lblSlNum16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum16.AutoSize = true;
            this.lblSlNum16.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum16.ForeColor = System.Drawing.Color.Black;
            this.lblSlNum16.Location = new System.Drawing.Point(43, 68);
            this.lblSlNum16.Name = "lblSlNum16";
            this.lblSlNum16.Size = new System.Drawing.Size(22, 18);
            this.lblSlNum16.TabIndex = 1;
            this.lblSlNum16.Text = "10";
            // 
            // lblSlNum17
            // 
            this.lblSlNum17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum17.AutoSize = true;
            this.lblSlNum17.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum17.ForeColor = System.Drawing.Color.Black;
            this.lblSlNum17.Location = new System.Drawing.Point(43, 101);
            this.lblSlNum17.Name = "lblSlNum17";
            this.lblSlNum17.Size = new System.Drawing.Size(22, 18);
            this.lblSlNum17.TabIndex = 1;
            this.lblSlNum17.Text = "11";
            // 
            // lblSlNum18
            // 
            this.lblSlNum18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum18.AutoSize = true;
            this.lblSlNum18.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum18.ForeColor = System.Drawing.Color.Black;
            this.lblSlNum18.Location = new System.Drawing.Point(43, 134);
            this.lblSlNum18.Name = "lblSlNum18";
            this.lblSlNum18.Size = new System.Drawing.Size(22, 18);
            this.lblSlNum18.TabIndex = 1;
            this.lblSlNum18.Text = "12";
            // 
            // chkTestCase9
            // 
            this.chkTestCase9.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase9.AutoSize = true;
            this.chkTestCase9.Location = new System.Drawing.Point(11, 38);
            this.chkTestCase9.Name = "chkTestCase9";
            this.chkTestCase9.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase9.TabIndex = 7;
            this.chkTestCase9.UseVisualStyleBackColor = true;
            // 
            // chkTestCase10
            // 
            this.chkTestCase10.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase10.AutoSize = true;
            this.chkTestCase10.Location = new System.Drawing.Point(11, 70);
            this.chkTestCase10.Name = "chkTestCase10";
            this.chkTestCase10.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase10.TabIndex = 7;
            this.chkTestCase10.UseVisualStyleBackColor = true;
            // 
            // chkTestCase11
            // 
            this.chkTestCase11.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase11.AutoSize = true;
            this.chkTestCase11.Location = new System.Drawing.Point(11, 103);
            this.chkTestCase11.Name = "chkTestCase11";
            this.chkTestCase11.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase11.TabIndex = 7;
            this.chkTestCase11.UseVisualStyleBackColor = true;
            // 
            // chkTestCase12
            // 
            this.chkTestCase12.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase12.AutoSize = true;
            this.chkTestCase12.Location = new System.Drawing.Point(11, 136);
            this.chkTestCase12.Name = "chkTestCase12";
            this.chkTestCase12.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase12.TabIndex = 7;
            this.chkTestCase12.UseVisualStyleBackColor = true;
            // 
            // chkTestCaseHDX3DPro
            // 
            this.chkTestCaseHDX3DPro.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCaseHDX3DPro.AutoSize = true;
            this.chkTestCaseHDX3DPro.Location = new System.Drawing.Point(11, 8);
            this.chkTestCaseHDX3DPro.Name = "chkTestCaseHDX3DPro";
            this.chkTestCaseHDX3DPro.Size = new System.Drawing.Size(15, 14);
            this.chkTestCaseHDX3DPro.TabIndex = 8;
            this.chkTestCaseHDX3DPro.UseVisualStyleBackColor = true;
            this.chkTestCaseHDX3DPro.CheckedChanged += new System.EventHandler(this.chkTestCaseHDX3DPro_CheckedChanged);
            // 
            // lblColHead3HDX3DPro
            // 
            this.lblColHead3HDX3DPro.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblColHead3HDX3DPro.AutoSize = true;
            this.lblColHead3HDX3DPro.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColHead3HDX3DPro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead3HDX3DPro.Location = new System.Drawing.Point(457, 5);
            this.lblColHead3HDX3DPro.Name = "lblColHead3HDX3DPro";
            this.lblColHead3HDX3DPro.Size = new System.Drawing.Size(160, 19);
            this.lblColHead3HDX3DPro.TabIndex = 0;
            this.lblColHead3HDX3DPro.Text = "Mandatory / Optional";
            // 
            // pnlTestCase9
            // 
            this.pnlTestCase9.Controls.Add(this.picboxTestCase9);
            this.pnlTestCase9.Controls.Add(this.lblTestCase9);
            this.pnlTestCase9.Location = new System.Drawing.Point(83, 33);
            this.pnlTestCase9.Name = "pnlTestCase9";
            this.pnlTestCase9.Size = new System.Drawing.Size(368, 24);
            this.pnlTestCase9.TabIndex = 9;
            // 
            // picboxTestCase9
            // 
            this.picboxTestCase9.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase9.Image")));
            this.picboxTestCase9.Location = new System.Drawing.Point(237, 4);
            this.picboxTestCase9.Name = "picboxTestCase9";
            this.picboxTestCase9.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase9.TabIndex = 2;
            this.picboxTestCase9.TabStop = false;
            // 
            // lblTestCase9
            // 
            this.lblTestCase9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase9.AutoSize = true;
            this.lblTestCase9.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase9.Location = new System.Drawing.Point(4, 4);
            this.lblTestCase9.Name = "lblTestCase9";
            this.lblTestCase9.Size = new System.Drawing.Size(221, 18);
            this.lblTestCase9.TabIndex = 1;
            this.lblTestCase9.Text = "3D Mouse Support and Redirection";
            // 
            // pnlTestCase10
            // 
            this.pnlTestCase10.Controls.Add(this.picboxTestCase10);
            this.pnlTestCase10.Controls.Add(this.lblTestCase10);
            this.pnlTestCase10.Location = new System.Drawing.Point(83, 63);
            this.pnlTestCase10.Name = "pnlTestCase10";
            this.pnlTestCase10.Size = new System.Drawing.Size(368, 24);
            this.pnlTestCase10.TabIndex = 10;
            // 
            // picboxTestCase10
            // 
            this.picboxTestCase10.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase10.Image")));
            this.picboxTestCase10.Location = new System.Drawing.Point(319, 4);
            this.picboxTestCase10.Name = "picboxTestCase10";
            this.picboxTestCase10.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase10.TabIndex = 2;
            this.picboxTestCase10.TabStop = false;
            // 
            // lblTestCase10
            // 
            this.lblTestCase10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase10.AutoSize = true;
            this.lblTestCase10.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase10.Location = new System.Drawing.Point(4, 4);
            this.lblTestCase10.Name = "lblTestCase10";
            this.lblTestCase10.Size = new System.Drawing.Size(309, 18);
            this.lblTestCase10.TabIndex = 1;
            this.lblTestCase10.Text = "Multi Monitor Support for 2D and 3D Applications";
            // 
            // pnlTestCase11
            // 
            this.pnlTestCase11.Controls.Add(this.picboxTestCase11);
            this.pnlTestCase11.Controls.Add(this.lblTestCase11);
            this.pnlTestCase11.Location = new System.Drawing.Point(83, 97);
            this.pnlTestCase11.Name = "pnlTestCase11";
            this.pnlTestCase11.Size = new System.Drawing.Size(368, 24);
            this.pnlTestCase11.TabIndex = 11;
            // 
            // picboxTestCase11
            // 
            this.picboxTestCase11.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase11.Image")));
            this.picboxTestCase11.Location = new System.Drawing.Point(234, 6);
            this.picboxTestCase11.Name = "picboxTestCase11";
            this.picboxTestCase11.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase11.TabIndex = 2;
            this.picboxTestCase11.TabStop = false;
            // 
            // lblTestCase11
            // 
            this.lblTestCase11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase11.AutoSize = true;
            this.lblTestCase11.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase11.Location = new System.Drawing.Point(5, 3);
            this.lblTestCase11.Name = "lblTestCase11";
            this.lblTestCase11.Size = new System.Drawing.Size(215, 18);
            this.lblTestCase11.TabIndex = 1;
            this.lblTestCase11.Text = "Rich Graphics Application Support";
            // 
            // pnlTestCase12
            // 
            this.pnlTestCase12.Controls.Add(this.picboxTestCase12);
            this.pnlTestCase12.Controls.Add(this.lblTestCase12);
            this.pnlTestCase12.Location = new System.Drawing.Point(83, 129);
            this.pnlTestCase12.Name = "pnlTestCase12";
            this.pnlTestCase12.Size = new System.Drawing.Size(368, 24);
            this.pnlTestCase12.TabIndex = 12;
            // 
            // picboxTestCase12
            // 
            this.picboxTestCase12.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase12.Image")));
            this.picboxTestCase12.Location = new System.Drawing.Point(234, 5);
            this.picboxTestCase12.Name = "picboxTestCase12";
            this.picboxTestCase12.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase12.TabIndex = 2;
            this.picboxTestCase12.TabStop = false;
            // 
            // lblTestCase12
            // 
            this.lblTestCase12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase12.AutoSize = true;
            this.lblTestCase12.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase12.Location = new System.Drawing.Point(5, 3);
            this.lblTestCase12.Name = "lblTestCase12";
            this.lblTestCase12.Size = new System.Drawing.Size(224, 18);
            this.lblTestCase12.TabIndex = 1;
            this.lblTestCase12.Text = "Pixel Perfect Lossless Compression";
            // 
            // chkTestCase13
            // 
            this.chkTestCase13.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase13.AutoSize = true;
            this.chkTestCase13.Location = new System.Drawing.Point(11, 169);
            this.chkTestCase13.Name = "chkTestCase13";
            this.chkTestCase13.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase13.TabIndex = 15;
            this.chkTestCase13.UseVisualStyleBackColor = true;
            // 
            // chkTestCase14
            // 
            this.chkTestCase14.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase14.AutoSize = true;
            this.chkTestCase14.Location = new System.Drawing.Point(11, 201);
            this.chkTestCase14.Name = "chkTestCase14";
            this.chkTestCase14.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase14.TabIndex = 14;
            this.chkTestCase14.UseVisualStyleBackColor = true;
            // 
            // chkTestCase15
            // 
            this.chkTestCase15.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase15.AutoSize = true;
            this.chkTestCase15.Location = new System.Drawing.Point(11, 242);
            this.chkTestCase15.Name = "chkTestCase15";
            this.chkTestCase15.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase15.TabIndex = 13;
            this.chkTestCase15.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(43, 167);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 18);
            this.label4.TabIndex = 17;
            this.label4.Text = "13";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(43, 199);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 18);
            this.label5.TabIndex = 18;
            this.label5.Text = "14";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(43, 240);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 18);
            this.label2.TabIndex = 16;
            this.label2.Text = "15";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.label7.Location = new System.Drawing.Point(457, 240);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 18);
            this.label7.TabIndex = 20;
            this.label7.Text = "Optional";
            // 
            // chkTestCase21
            // 
            this.chkTestCase21.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase21.AutoSize = true;
            this.chkTestCase21.Location = new System.Drawing.Point(11, 283);
            this.chkTestCase21.Name = "chkTestCase21";
            this.chkTestCase21.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase21.TabIndex = 6;
            this.chkTestCase21.UseVisualStyleBackColor = true;
            // 
            // lblSlNum13
            // 
            this.lblSlNum13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum13.AutoSize = true;
            this.lblSlNum13.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum13.Location = new System.Drawing.Point(43, 281);
            this.lblSlNum13.Name = "lblSlNum13";
            this.lblSlNum13.Size = new System.Drawing.Size(22, 18);
            this.lblSlNum13.TabIndex = 1;
            this.lblSlNum13.Text = "16";
            // 
            // lblRequired13
            // 
            this.lblRequired13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRequired13.AutoSize = true;
            this.lblRequired13.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequired13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblRequired13.Location = new System.Drawing.Point(457, 281);
            this.lblRequired13.Name = "lblRequired13";
            this.lblRequired13.Size = new System.Drawing.Size(61, 18);
            this.lblRequired13.TabIndex = 5;
            this.lblRequired13.Text = "Optional";
            // 
            // lblStatus21
            // 
            this.lblStatus21.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus21.AutoSize = true;
            this.lblStatus21.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus21.Location = new System.Drawing.Point(633, 281);
            this.lblStatus21.Name = "lblStatus21";
            this.lblStatus21.Size = new System.Drawing.Size(87, 18);
            this.lblStatus21.TabIndex = 1;
            this.lblStatus21.Text = "Not Checked";
            // 
            // pnlTestCase21
            // 
            this.pnlTestCase21.Controls.Add(this.picboxTestCase21);
            this.pnlTestCase21.Controls.Add(this.lblTestCase21);
            this.pnlTestCase21.Location = new System.Drawing.Point(83, 277);
            this.pnlTestCase21.Name = "pnlTestCase21";
            this.pnlTestCase21.Size = new System.Drawing.Size(368, 23);
            this.pnlTestCase21.TabIndex = 14;
            // 
            // picboxTestCase21
            // 
            this.picboxTestCase21.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase21.Image")));
            this.picboxTestCase21.Location = new System.Drawing.Point(110, 6);
            this.picboxTestCase21.Name = "picboxTestCase21";
            this.picboxTestCase21.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase21.TabIndex = 1;
            this.picboxTestCase21.TabStop = false;
            // 
            // lblTestCase21
            // 
            this.lblTestCase21.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase21.AutoSize = true;
            this.lblTestCase21.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase21.Location = new System.Drawing.Point(5, 4);
            this.lblTestCase21.Name = "lblTestCase21";
            this.lblTestCase21.Size = new System.Drawing.Size(92, 18);
            this.lblTestCase21.TabIndex = 1;
            this.lblTestCase21.Text = "Storebrowser";
            // 
            // tabHDXPremium
            // 
            this.tabHDXPremium.BackColor = System.Drawing.Color.White;
            this.tabHDXPremium.Controls.Add(this.tlpHDXPremium);
            this.tabHDXPremium.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabHDXPremium.Location = new System.Drawing.Point(4, 28);
            this.tabHDXPremium.Name = "tabHDXPremium";
            this.tabHDXPremium.Padding = new System.Windows.Forms.Padding(3);
            this.tabHDXPremium.Size = new System.Drawing.Size(747, 330);
            this.tabHDXPremium.TabIndex = 1;
            this.tabHDXPremium.Text = " Premium ";
            // 
            // tlpHDXPremium
            // 
            this.tlpHDXPremium.ColumnCount = 5;
            this.tlpHDXPremium.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.tlpHDXPremium.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7F));
            this.tlpHDXPremium.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51F));
            this.tlpHDXPremium.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24F));
            this.tlpHDXPremium.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14F));
            this.tlpHDXPremium.Controls.Add(this.checkBox3, 0, 2);
            this.tlpHDXPremium.Controls.Add(this.lblStatus23, 4, 7);
            this.tlpHDXPremium.Controls.Add(this.label10, 3, 7);
            this.tlpHDXPremium.Controls.Add(this.label9, 1, 7);
            this.tlpHDXPremium.Controls.Add(this.chkTestCase23, 0, 7);
            this.tlpHDXPremium.Controls.Add(this.lblColHead4HDXPremium, 4, 0);
            this.tlpHDXPremium.Controls.Add(this.lblColHead2HDXPremium, 2, 0);
            this.tlpHDXPremium.Controls.Add(this.lblColHead1HDXPremium, 1, 0);
            this.tlpHDXPremium.Controls.Add(this.chkTestCaseHDXPremium, 0, 0);
            this.tlpHDXPremium.Controls.Add(this.lblColHead3HDXPremium, 3, 0);
            this.tlpHDXPremium.Controls.Add(this.chkTestCase16, 0, 1);
            this.tlpHDXPremium.Controls.Add(this.chkTestCase19, 0, 3);
            this.tlpHDXPremium.Controls.Add(this.chkTestCase20, 0, 4);
            this.tlpHDXPremium.Controls.Add(this.chkTestCase22, 0, 6);
            this.tlpHDXPremium.Controls.Add(this.lblSlNum8, 1, 1);
            this.tlpHDXPremium.Controls.Add(this.lblSlNum9, 1, 2);
            this.tlpHDXPremium.Controls.Add(this.lblSlNum19, 1, 3);
            this.tlpHDXPremium.Controls.Add(this.lblSlNum12, 1, 4);
            this.tlpHDXPremium.Controls.Add(this.lblSlNum14, 1, 6);
            this.tlpHDXPremium.Controls.Add(this.lblRequired8, 3, 1);
            this.tlpHDXPremium.Controls.Add(this.lblRequired9, 3, 2);
            this.tlpHDXPremium.Controls.Add(this.lblRequired11, 3, 3);
            this.tlpHDXPremium.Controls.Add(this.lblRequired12, 3, 4);
            this.tlpHDXPremium.Controls.Add(this.lblRequired14, 3, 6);
            this.tlpHDXPremium.Controls.Add(this.lblStatus16, 4, 1);
            this.tlpHDXPremium.Controls.Add(this.lblStatus17, 4, 2);
            this.tlpHDXPremium.Controls.Add(this.lblStatus19, 4, 3);
            this.tlpHDXPremium.Controls.Add(this.lblStatus20, 4, 4);
            this.tlpHDXPremium.Controls.Add(this.lblStatus22, 4, 6);
            this.tlpHDXPremium.Controls.Add(this.pnlTestCase16, 2, 1);
            this.tlpHDXPremium.Controls.Add(this.pnlTestCase17, 2, 2);
            this.tlpHDXPremium.Controls.Add(this.pnlTestCase19, 2, 3);
            this.tlpHDXPremium.Controls.Add(this.pnlTestCase20, 2, 4);
            this.tlpHDXPremium.Controls.Add(this.pnlTestCase22, 2, 6);
            this.tlpHDXPremium.Controls.Add(this.pnlTestCase23, 2, 7);
            this.tlpHDXPremium.Controls.Add(this.chkTestCase24, 0, 5);
            this.tlpHDXPremium.Controls.Add(this.label17, 1, 5);
            this.tlpHDXPremium.Controls.Add(this.label24, 3, 5);
            this.tlpHDXPremium.Controls.Add(this.label31, 4, 5);
            this.tlpHDXPremium.Controls.Add(this.panel1, 2, 5);
            this.tlpHDXPremium.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlpHDXPremium.Location = new System.Drawing.Point(6, 24);
            this.tlpHDXPremium.Name = "tlpHDXPremium";
            this.tlpHDXPremium.RowCount = 8;
            this.tlpHDXPremium.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpHDXPremium.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tlpHDXPremium.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tlpHDXPremium.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpHDXPremium.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tlpHDXPremium.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tlpHDXPremium.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tlpHDXPremium.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tlpHDXPremium.Size = new System.Drawing.Size(734, 273);
            this.tlpHDXPremium.TabIndex = 10;
            // 
            // checkBox3
            // 
            this.checkBox3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(11, 72);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 23;
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // lblStatus23
            // 
            this.lblStatus23.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus23.AutoSize = true;
            this.lblStatus23.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus23.Location = new System.Drawing.Point(633, 241);
            this.lblStatus23.Name = "lblStatus23";
            this.lblStatus23.Size = new System.Drawing.Size(87, 18);
            this.lblStatus23.TabIndex = 19;
            this.lblStatus23.Text = "Not Checked";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.label10.Location = new System.Drawing.Point(457, 241);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 18);
            this.label10.TabIndex = 18;
            this.label10.Text = "Mandatory";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(43, 241);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(22, 18);
            this.label9.TabIndex = 17;
            this.label9.Text = "23";
            // 
            // chkTestCase23
            // 
            this.chkTestCase23.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase23.AutoSize = true;
            this.chkTestCase23.Location = new System.Drawing.Point(11, 243);
            this.chkTestCase23.Name = "chkTestCase23";
            this.chkTestCase23.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase23.TabIndex = 16;
            this.chkTestCase23.UseVisualStyleBackColor = true;
            // 
            // lblColHead4HDXPremium
            // 
            this.lblColHead4HDXPremium.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblColHead4HDXPremium.AutoSize = true;
            this.lblColHead4HDXPremium.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColHead4HDXPremium.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead4HDXPremium.Location = new System.Drawing.Point(633, 5);
            this.lblColHead4HDXPremium.Name = "lblColHead4HDXPremium";
            this.lblColHead4HDXPremium.Size = new System.Drawing.Size(52, 19);
            this.lblColHead4HDXPremium.TabIndex = 0;
            this.lblColHead4HDXPremium.Text = "Status";
            // 
            // lblColHead2HDXPremium
            // 
            this.lblColHead2HDXPremium.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblColHead2HDXPremium.AutoSize = true;
            this.lblColHead2HDXPremium.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColHead2HDXPremium.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead2HDXPremium.Location = new System.Drawing.Point(83, 5);
            this.lblColHead2HDXPremium.Name = "lblColHead2HDXPremium";
            this.lblColHead2HDXPremium.Size = new System.Drawing.Size(105, 19);
            this.lblColHead2HDXPremium.TabIndex = 0;
            this.lblColHead2HDXPremium.Text = "Test Scenarios";
            // 
            // lblColHead1HDXPremium
            // 
            this.lblColHead1HDXPremium.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblColHead1HDXPremium.AutoSize = true;
            this.lblColHead1HDXPremium.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColHead1HDXPremium.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead1HDXPremium.Location = new System.Drawing.Point(32, 5);
            this.lblColHead1HDXPremium.Name = "lblColHead1HDXPremium";
            this.lblColHead1HDXPremium.Size = new System.Drawing.Size(35, 19);
            this.lblColHead1HDXPremium.TabIndex = 0;
            this.lblColHead1HDXPremium.Text = "S/N";
            // 
            // chkTestCaseHDXPremium
            // 
            this.chkTestCaseHDXPremium.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCaseHDXPremium.AutoSize = true;
            this.chkTestCaseHDXPremium.Location = new System.Drawing.Point(11, 8);
            this.chkTestCaseHDXPremium.Name = "chkTestCaseHDXPremium";
            this.chkTestCaseHDXPremium.Size = new System.Drawing.Size(15, 14);
            this.chkTestCaseHDXPremium.TabIndex = 7;
            this.chkTestCaseHDXPremium.UseVisualStyleBackColor = true;
            this.chkTestCaseHDXPremium.CheckedChanged += new System.EventHandler(this.chkTestCaseHDXPremium_CheckedChanged);
            // 
            // lblColHead3HDXPremium
            // 
            this.lblColHead3HDXPremium.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblColHead3HDXPremium.AutoSize = true;
            this.lblColHead3HDXPremium.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColHead3HDXPremium.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.lblColHead3HDXPremium.Location = new System.Drawing.Point(457, 5);
            this.lblColHead3HDXPremium.Name = "lblColHead3HDXPremium";
            this.lblColHead3HDXPremium.Size = new System.Drawing.Size(160, 19);
            this.lblColHead3HDXPremium.TabIndex = 0;
            this.lblColHead3HDXPremium.Text = "Mandatory / Optional";
            // 
            // chkTestCase16
            // 
            this.chkTestCase16.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase16.AutoSize = true;
            this.chkTestCase16.Location = new System.Drawing.Point(11, 40);
            this.chkTestCase16.Name = "chkTestCase16";
            this.chkTestCase16.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase16.TabIndex = 6;
            this.chkTestCase16.UseVisualStyleBackColor = true;
            // 
            // chkTestCase19
            // 
            this.chkTestCase19.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase19.AutoSize = true;
            this.chkTestCase19.Location = new System.Drawing.Point(11, 103);
            this.chkTestCase19.Name = "chkTestCase19";
            this.chkTestCase19.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase19.TabIndex = 6;
            this.chkTestCase19.UseVisualStyleBackColor = true;
            // 
            // chkTestCase20
            // 
            this.chkTestCase20.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase20.AutoSize = true;
            this.chkTestCase20.Location = new System.Drawing.Point(11, 134);
            this.chkTestCase20.Name = "chkTestCase20";
            this.chkTestCase20.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase20.TabIndex = 6;
            this.chkTestCase20.UseVisualStyleBackColor = true;
            // 
            // chkTestCase22
            // 
            this.chkTestCase22.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase22.AutoSize = true;
            this.chkTestCase22.Location = new System.Drawing.Point(11, 202);
            this.chkTestCase22.Name = "chkTestCase22";
            this.chkTestCase22.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase22.TabIndex = 6;
            this.chkTestCase22.UseVisualStyleBackColor = true;
            // 
            // lblSlNum8
            // 
            this.lblSlNum8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum8.AutoSize = true;
            this.lblSlNum8.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum8.Location = new System.Drawing.Point(43, 38);
            this.lblSlNum8.Name = "lblSlNum8";
            this.lblSlNum8.Size = new System.Drawing.Size(22, 18);
            this.lblSlNum8.TabIndex = 1;
            this.lblSlNum8.Text = "17";
            // 
            // lblSlNum9
            // 
            this.lblSlNum9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum9.AutoSize = true;
            this.lblSlNum9.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum9.Location = new System.Drawing.Point(43, 70);
            this.lblSlNum9.Name = "lblSlNum9";
            this.lblSlNum9.Size = new System.Drawing.Size(22, 18);
            this.lblSlNum9.TabIndex = 1;
            this.lblSlNum9.Text = "18";
            // 
            // lblSlNum19
            // 
            this.lblSlNum19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum19.AutoSize = true;
            this.lblSlNum19.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum19.Location = new System.Drawing.Point(43, 101);
            this.lblSlNum19.Name = "lblSlNum19";
            this.lblSlNum19.Size = new System.Drawing.Size(22, 18);
            this.lblSlNum19.TabIndex = 1;
            this.lblSlNum19.Text = "19";
            // 
            // lblSlNum12
            // 
            this.lblSlNum12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum12.AutoSize = true;
            this.lblSlNum12.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum12.Location = new System.Drawing.Point(43, 132);
            this.lblSlNum12.Name = "lblSlNum12";
            this.lblSlNum12.Size = new System.Drawing.Size(22, 18);
            this.lblSlNum12.TabIndex = 1;
            this.lblSlNum12.Text = "20";
            // 
            // lblSlNum14
            // 
            this.lblSlNum14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSlNum14.AutoSize = true;
            this.lblSlNum14.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlNum14.Location = new System.Drawing.Point(43, 200);
            this.lblSlNum14.Name = "lblSlNum14";
            this.lblSlNum14.Size = new System.Drawing.Size(22, 18);
            this.lblSlNum14.TabIndex = 1;
            this.lblSlNum14.Text = "22";
            // 
            // lblRequired8
            // 
            this.lblRequired8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRequired8.AutoSize = true;
            this.lblRequired8.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequired8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblRequired8.Location = new System.Drawing.Point(457, 38);
            this.lblRequired8.Name = "lblRequired8";
            this.lblRequired8.Size = new System.Drawing.Size(61, 18);
            this.lblRequired8.TabIndex = 5;
            this.lblRequired8.Text = "Optional";
            // 
            // lblRequired9
            // 
            this.lblRequired9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRequired9.AutoSize = true;
            this.lblRequired9.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequired9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblRequired9.Location = new System.Drawing.Point(457, 70);
            this.lblRequired9.Name = "lblRequired9";
            this.lblRequired9.Size = new System.Drawing.Size(75, 18);
            this.lblRequired9.TabIndex = 5;
            this.lblRequired9.Text = "Mandatory";
            // 
            // lblRequired11
            // 
            this.lblRequired11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRequired11.AutoSize = true;
            this.lblRequired11.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequired11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblRequired11.Location = new System.Drawing.Point(457, 101);
            this.lblRequired11.Name = "lblRequired11";
            this.lblRequired11.Size = new System.Drawing.Size(75, 18);
            this.lblRequired11.TabIndex = 5;
            this.lblRequired11.Text = "Mandatory";
            // 
            // lblRequired12
            // 
            this.lblRequired12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRequired12.AutoSize = true;
            this.lblRequired12.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequired12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblRequired12.Location = new System.Drawing.Point(457, 132);
            this.lblRequired12.Name = "lblRequired12";
            this.lblRequired12.Size = new System.Drawing.Size(75, 18);
            this.lblRequired12.TabIndex = 5;
            this.lblRequired12.Text = "Mandatory";
            // 
            // lblRequired14
            // 
            this.lblRequired14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRequired14.AutoSize = true;
            this.lblRequired14.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequired14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblRequired14.Location = new System.Drawing.Point(457, 200);
            this.lblRequired14.Name = "lblRequired14";
            this.lblRequired14.Size = new System.Drawing.Size(75, 18);
            this.lblRequired14.TabIndex = 5;
            this.lblRequired14.Text = "Mandatory";
            // 
            // lblStatus16
            // 
            this.lblStatus16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus16.AutoSize = true;
            this.lblStatus16.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus16.Location = new System.Drawing.Point(633, 38);
            this.lblStatus16.Name = "lblStatus16";
            this.lblStatus16.Size = new System.Drawing.Size(87, 18);
            this.lblStatus16.TabIndex = 1;
            this.lblStatus16.Text = "Not Checked";
            // 
            // lblStatus17
            // 
            this.lblStatus17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus17.AutoSize = true;
            this.lblStatus17.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus17.Location = new System.Drawing.Point(633, 70);
            this.lblStatus17.Name = "lblStatus17";
            this.lblStatus17.Size = new System.Drawing.Size(87, 18);
            this.lblStatus17.TabIndex = 1;
            this.lblStatus17.Text = "Not Checked";
            // 
            // lblStatus19
            // 
            this.lblStatus19.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus19.AutoSize = true;
            this.lblStatus19.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus19.Location = new System.Drawing.Point(633, 101);
            this.lblStatus19.Name = "lblStatus19";
            this.lblStatus19.Size = new System.Drawing.Size(87, 18);
            this.lblStatus19.TabIndex = 1;
            this.lblStatus19.Text = "Not Checked";
            // 
            // lblStatus20
            // 
            this.lblStatus20.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus20.AutoSize = true;
            this.lblStatus20.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus20.Location = new System.Drawing.Point(633, 132);
            this.lblStatus20.Name = "lblStatus20";
            this.lblStatus20.Size = new System.Drawing.Size(87, 18);
            this.lblStatus20.TabIndex = 1;
            this.lblStatus20.Text = "Not Checked";
            // 
            // lblStatus22
            // 
            this.lblStatus22.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStatus22.AutoSize = true;
            this.lblStatus22.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblStatus22.Location = new System.Drawing.Point(633, 200);
            this.lblStatus22.Name = "lblStatus22";
            this.lblStatus22.Size = new System.Drawing.Size(87, 18);
            this.lblStatus22.TabIndex = 1;
            this.lblStatus22.Text = "Not Checked";
            // 
            // pnlTestCase16
            // 
            this.pnlTestCase16.Controls.Add(this.picboxTestCase16);
            this.pnlTestCase16.Controls.Add(this.lblTestCase16);
            this.pnlTestCase16.Location = new System.Drawing.Point(83, 33);
            this.pnlTestCase16.Name = "pnlTestCase16";
            this.pnlTestCase16.Size = new System.Drawing.Size(368, 28);
            this.pnlTestCase16.TabIndex = 9;
            // 
            // picboxTestCase16
            // 
            this.picboxTestCase16.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase16.Image")));
            this.picboxTestCase16.Location = new System.Drawing.Point(320, 5);
            this.picboxTestCase16.Name = "picboxTestCase16";
            this.picboxTestCase16.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase16.TabIndex = 1;
            this.picboxTestCase16.TabStop = false;
            // 
            // lblTestCase16
            // 
            this.lblTestCase16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase16.AutoSize = true;
            this.lblTestCase16.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase16.Location = new System.Drawing.Point(5, 5);
            this.lblTestCase16.Name = "lblTestCase16";
            this.lblTestCase16.Size = new System.Drawing.Size(309, 18);
            this.lblTestCase16.TabIndex = 1;
            this.lblTestCase16.Text = "Hardware Accelaration using H265 Codec Support";
            // 
            // pnlTestCase17
            // 
            this.pnlTestCase17.Controls.Add(this.picboxTestCase17);
            this.pnlTestCase17.Controls.Add(this.lblTestCase17);
            this.pnlTestCase17.Location = new System.Drawing.Point(83, 67);
            this.pnlTestCase17.Name = "pnlTestCase17";
            this.pnlTestCase17.Size = new System.Drawing.Size(368, 24);
            this.pnlTestCase17.TabIndex = 10;
            // 
            // picboxTestCase17
            // 
            this.picboxTestCase17.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase17.Image")));
            this.picboxTestCase17.Location = new System.Drawing.Point(184, 5);
            this.picboxTestCase17.Name = "picboxTestCase17";
            this.picboxTestCase17.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase17.TabIndex = 1;
            this.picboxTestCase17.TabStop = false;
            // 
            // lblTestCase17
            // 
            this.lblTestCase17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase17.AutoSize = true;
            this.lblTestCase17.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase17.Location = new System.Drawing.Point(5, 3);
            this.lblTestCase17.Name = "lblTestCase17";
            this.lblTestCase17.Size = new System.Drawing.Size(175, 18);
            this.lblTestCase17.TabIndex = 1;
            this.lblTestCase17.Text = "HTML5 Redirection Support";
            // 
            // pnlTestCase19
            // 
            this.pnlTestCase19.Controls.Add(this.picboxTestCase19);
            this.pnlTestCase19.Controls.Add(this.lblTestCase19);
            this.pnlTestCase19.Location = new System.Drawing.Point(83, 98);
            this.pnlTestCase19.Name = "pnlTestCase19";
            this.pnlTestCase19.Size = new System.Drawing.Size(368, 24);
            this.pnlTestCase19.TabIndex = 12;
            // 
            // picboxTestCase19
            // 
            this.picboxTestCase19.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase19.Image")));
            this.picboxTestCase19.Location = new System.Drawing.Point(289, 4);
            this.picboxTestCase19.Name = "picboxTestCase19";
            this.picboxTestCase19.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase19.TabIndex = 2;
            this.picboxTestCase19.TabStop = false;
            // 
            // lblTestCase19
            // 
            this.lblTestCase19.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase19.AutoSize = true;
            this.lblTestCase19.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase19.Location = new System.Drawing.Point(5, 5);
            this.lblTestCase19.Name = "lblTestCase19";
            this.lblTestCase19.Size = new System.Drawing.Size(281, 18);
            this.lblTestCase19.TabIndex = 1;
            this.lblTestCase19.Text = "Windows Media Stream Redirection Support";
            // 
            // pnlTestCase20
            // 
            this.pnlTestCase20.Controls.Add(this.picboxTestCase20);
            this.pnlTestCase20.Controls.Add(this.lblTestCase20);
            this.pnlTestCase20.Location = new System.Drawing.Point(83, 128);
            this.pnlTestCase20.Name = "pnlTestCase20";
            this.pnlTestCase20.Size = new System.Drawing.Size(368, 24);
            this.pnlTestCase20.TabIndex = 13;
            // 
            // picboxTestCase20
            // 
            this.picboxTestCase20.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase20.Image")));
            this.picboxTestCase20.Location = new System.Drawing.Point(283, 5);
            this.picboxTestCase20.Name = "picboxTestCase20";
            this.picboxTestCase20.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase20.TabIndex = 1;
            this.picboxTestCase20.TabStop = false;
            // 
            // lblTestCase20
            // 
            this.lblTestCase20.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase20.AutoSize = true;
            this.lblTestCase20.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase20.Location = new System.Drawing.Point(5, 3);
            this.lblTestCase20.Name = "lblTestCase20";
            this.lblTestCase20.Size = new System.Drawing.Size(263, 18);
            this.lblTestCase20.TabIndex = 1;
            this.lblTestCase20.Text = "Optimization for Microsoft Teams Support";
            // 
            // pnlTestCase22
            // 
            this.pnlTestCase22.Controls.Add(this.picboxTestCase22);
            this.pnlTestCase22.Controls.Add(this.lblTestCase22);
            this.pnlTestCase22.Location = new System.Drawing.Point(83, 194);
            this.pnlTestCase22.Name = "pnlTestCase22";
            this.pnlTestCase22.Size = new System.Drawing.Size(368, 24);
            this.pnlTestCase22.TabIndex = 15;
            // 
            // picboxTestCase22
            // 
            this.picboxTestCase22.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase22.Image")));
            this.picboxTestCase22.Location = new System.Drawing.Point(157, 8);
            this.picboxTestCase22.Name = "picboxTestCase22";
            this.picboxTestCase22.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase22.TabIndex = 1;
            this.picboxTestCase22.TabStop = false;
            // 
            // lblTestCase22
            // 
            this.lblTestCase22.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase22.AutoSize = true;
            this.lblTestCase22.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase22.Location = new System.Drawing.Point(5, 7);
            this.lblTestCase22.Name = "lblTestCase22";
            this.lblTestCase22.Size = new System.Drawing.Size(146, 18);
            this.lblTestCase22.TabIndex = 1;
            this.lblTestCase22.Text = "PIV Smartcard Support";
            // 
            // pnlTestCase23
            // 
            this.pnlTestCase23.Controls.Add(this.picboxTestCase23);
            this.pnlTestCase23.Controls.Add(this.lblTestCase23);
            this.pnlTestCase23.Location = new System.Drawing.Point(83, 231);
            this.pnlTestCase23.Name = "pnlTestCase23";
            this.pnlTestCase23.Size = new System.Drawing.Size(368, 39);
            this.pnlTestCase23.TabIndex = 20;
            // 
            // picboxTestCase23
            // 
            this.picboxTestCase23.Image = ((System.Drawing.Image)(resources.GetObject("picboxTestCase23.Image")));
            this.picboxTestCase23.Location = new System.Drawing.Point(313, 3);
            this.picboxTestCase23.Name = "picboxTestCase23";
            this.picboxTestCase23.Size = new System.Drawing.Size(16, 16);
            this.picboxTestCase23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picboxTestCase23.TabIndex = 1;
            this.picboxTestCase23.TabStop = false;
            // 
            // lblTestCase23
            // 
            this.lblTestCase23.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestCase23.AutoSize = true;
            this.lblTestCase23.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestCase23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblTestCase23.Location = new System.Drawing.Point(5, 0);
            this.lblTestCase23.Name = "lblTestCase23";
            this.lblTestCase23.Size = new System.Drawing.Size(302, 36);
            this.lblTestCase23.TabIndex = 2;
            this.lblTestCase23.Text = "Video Conferencing on Skype for Business with \r\nHDX Real TimeOptimization Pack Su" +
    "pport";
            // 
            // chkTestCase24
            // 
            this.chkTestCase24.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase24.AutoSize = true;
            this.chkTestCase24.Location = new System.Drawing.Point(11, 167);
            this.chkTestCase24.Name = "chkTestCase24";
            this.chkTestCase24.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase24.TabIndex = 22;
            this.chkTestCase24.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(43, 165);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(22, 18);
            this.label17.TabIndex = 1;
            this.label17.Text = "21";
            // 
            // label24
            // 
            this.label24.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.label24.Location = new System.Drawing.Point(457, 165);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(75, 18);
            this.label24.TabIndex = 5;
            this.label24.Text = "Mandatory";
            // 
            // label31
            // 
            this.label31.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.label31.Location = new System.Drawing.Point(633, 165);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(87, 18);
            this.label31.TabIndex = 1;
            this.label31.Text = "Not Checked";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label38);
            this.panel1.Location = new System.Drawing.Point(83, 161);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(368, 24);
            this.panel1.TabIndex = 9;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(252, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.ttHelp.SetToolTip(this.pictureBox1, "This test case will verify that your Thin Client supports \nBrowser Content Redire" +
        "ction, on the virtual desktop.");
            // 
            // label38
            // 
            this.label38.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.label38.Location = new System.Drawing.Point(6, -1);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(238, 18);
            this.label38.TabIndex = 1;
            this.label38.Text = "Browser Content Redirection Support";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 23);
            this.label13.TabIndex = 0;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(0, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 23);
            this.label14.TabIndex = 0;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(0, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 23);
            this.label15.TabIndex = 0;
            // 
            // checkBox2
            // 
            this.checkBox2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(11, 8);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 22;
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(0, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(100, 23);
            this.label16.TabIndex = 0;
            // 
            // chkTestCase4
            // 
            this.chkTestCase4.Location = new System.Drawing.Point(0, 0);
            this.chkTestCase4.Name = "chkTestCase4";
            this.chkTestCase4.Size = new System.Drawing.Size(104, 24);
            this.chkTestCase4.TabIndex = 0;
            // 
            // chkTestCase17
            // 
            this.chkTestCase17.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase17.AutoSize = true;
            this.chkTestCase17.Location = new System.Drawing.Point(11, 68);
            this.chkTestCase17.Name = "chkTestCase17";
            this.chkTestCase17.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase17.TabIndex = 6;
            this.chkTestCase17.UseVisualStyleBackColor = true;
            // 
            // chkTestCase18
            // 
            this.chkTestCase18.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTestCase18.AutoSize = true;
            this.chkTestCase18.Location = new System.Drawing.Point(11, 98);
            this.chkTestCase18.Name = "chkTestCase18";
            this.chkTestCase18.Size = new System.Drawing.Size(15, 14);
            this.chkTestCase18.TabIndex = 6;
            this.chkTestCase18.UseVisualStyleBackColor = true;
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(212, 522);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(130, 29);
            this.btnBack.TabIndex = 1;
            this.btnBack.Text = "< Back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnValidate
            // 
            this.btnValidate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.btnValidate.FlatAppearance.BorderSize = 0;
            this.btnValidate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnValidate.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnValidate.ForeColor = System.Drawing.Color.White;
            this.btnValidate.Location = new System.Drawing.Point(353, 521);
            this.btnValidate.Name = "btnValidate";
            this.btnValidate.Size = new System.Drawing.Size(130, 29);
            this.btnValidate.TabIndex = 1;
            this.btnValidate.Text = "Validate";
            this.btnValidate.UseVisualStyleBackColor = false;
            this.btnValidate.Click += new System.EventHandler(this.btnValidate_Click);
            // 
            // ttHelp
            // 
            this.ttHelp.IsBalloon = true;
            // 
            // lblSampleFileLocation
            // 
            this.lblSampleFileLocation.AutoSize = true;
            this.lblSampleFileLocation.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSampleFileLocation.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblSampleFileLocation.Location = new System.Drawing.Point(16, 57);
            this.lblSampleFileLocation.Name = "lblSampleFileLocation";
            this.lblSampleFileLocation.Size = new System.Drawing.Size(162, 19);
            this.lblSampleFileLocation.TabIndex = 11;
            this.lblSampleFileLocation.Text = "Sample Files Location: ";
            // 
            // btnSampleFilesLocation
            // 
            this.btnSampleFilesLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.btnSampleFilesLocation.FlatAppearance.BorderSize = 0;
            this.btnSampleFilesLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSampleFilesLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSampleFilesLocation.ForeColor = System.Drawing.Color.White;
            this.btnSampleFilesLocation.Location = new System.Drawing.Point(176, 57);
            this.btnSampleFilesLocation.Name = "btnSampleFilesLocation";
            this.btnSampleFilesLocation.Size = new System.Drawing.Size(84, 22);
            this.btnSampleFilesLocation.TabIndex = 12;
            this.btnSampleFilesLocation.Text = "Browse";
            this.btnSampleFilesLocation.UseVisualStyleBackColor = false;
            this.btnSampleFilesLocation.Click += new System.EventHandler(this.btnSampleFilesLocation_Click);
            // 
            // lblSampleFileLocationValue
            // 
            this.lblSampleFileLocationValue.AutoSize = true;
            this.lblSampleFileLocationValue.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSampleFileLocationValue.Location = new System.Drawing.Point(267, 58);
            this.lblSampleFileLocationValue.Name = "lblSampleFileLocationValue";
            this.lblSampleFileLocationValue.Size = new System.Drawing.Size(28, 19);
            this.lblSampleFileLocationValue.TabIndex = 13;
            this.lblSampleFileLocationValue.Text = "C:\\";
            // 
            // lblBuildNumber
            // 
            this.lblBuildNumber.AutoSize = true;
            this.lblBuildNumber.Font = new System.Drawing.Font("Calibri", 8.25F);
            this.lblBuildNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(102)))), ((int)(((byte)(106)))));
            this.lblBuildNumber.Location = new System.Drawing.Point(5, 537);
            this.lblBuildNumber.Name = "lblBuildNumber";
            this.lblBuildNumber.Size = new System.Drawing.Size(71, 13);
            this.lblBuildNumber.TabIndex = 16;
            this.lblBuildNumber.Text = "Build Number";
            // 
            // picboxTopLogo
            // 
            this.picboxTopLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picboxTopLogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picboxTopLogo.Image = global::ThinClientVerification.Properties.Resources.Screen_1_800x800;
            this.picboxTopLogo.Location = new System.Drawing.Point(-13, -13);
            this.picboxTopLogo.Name = "picboxTopLogo";
            this.picboxTopLogo.Size = new System.Drawing.Size(780, 110);
            this.picboxTopLogo.TabIndex = 15;
            this.picboxTopLogo.TabStop = false;
            // 
            // frmVerification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 567);
            this.Controls.Add(this.lblBuildNumber);
            this.Controls.Add(this.lblSampleFileLocationValue);
            this.Controls.Add(this.btnSampleFilesLocation);
            this.Controls.Add(this.lblSampleFileLocation);
            this.Controls.Add(this.tabTestCases);
            this.Controls.Add(this.lblSeperatorLine1);
            this.Controls.Add(this.txtResult);
            this.Controls.Add(this.lblHeading);
            this.Controls.Add(this.btnValidate);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnViewReport);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.picboxTopLogo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmVerification";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Citrix Ready - Thin Client Verification";
            this.Load += new System.EventHandler(this.frmVerification_Load);
            this.Shown += new System.EventHandler(this.frmVerification_Shown);
            this.tabTestCases.ResumeLayout(false);
            this.tabHDXReady.ResumeLayout(false);
            this.tlpHDXReady.ResumeLayout(false);
            this.tlpHDXReady.PerformLayout();
            this.pnlTestCase7.ResumeLayout(false);
            this.pnlTestCase7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase7)).EndInit();
            this.pnlTestCase1.ResumeLayout(false);
            this.pnlTestCase1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase1)).EndInit();
            this.pnlTestCase2.ResumeLayout(false);
            this.pnlTestCase2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase2)).EndInit();
            this.pnlTestCase3.ResumeLayout(false);
            this.pnlTestCase3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase3)).EndInit();
            this.pnlTestCase5.ResumeLayout(false);
            this.pnlTestCase5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase5)).EndInit();
            this.pnlTestCase6.ResumeLayout(false);
            this.pnlTestCase6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase6)).EndInit();
            this.pnlTestCase8.ResumeLayout(false);
            this.pnlTestCase8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase8)).EndInit();
            this.pnlTestCase4.ResumeLayout(false);
            this.pnlTestCase4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase4)).EndInit();
            this.tabHDX3DPro.ResumeLayout(false);
            this.tlpHDX3DPro.ResumeLayout(false);
            this.tlpHDX3DPro.PerformLayout();
            this.pnlTestCase15.ResumeLayout(false);
            this.pnlTestCase15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase15)).EndInit();
            this.pnlTestCase14.ResumeLayout(false);
            this.pnlTestCase14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase14)).EndInit();
            this.pnlTestCase13.ResumeLayout(false);
            this.pnlTestCase13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase13)).EndInit();
            this.pnlTestCase9.ResumeLayout(false);
            this.pnlTestCase9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase9)).EndInit();
            this.pnlTestCase10.ResumeLayout(false);
            this.pnlTestCase10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase10)).EndInit();
            this.pnlTestCase11.ResumeLayout(false);
            this.pnlTestCase11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase11)).EndInit();
            this.pnlTestCase12.ResumeLayout(false);
            this.pnlTestCase12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase12)).EndInit();
            this.pnlTestCase21.ResumeLayout(false);
            this.pnlTestCase21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase21)).EndInit();
            this.tabHDXPremium.ResumeLayout(false);
            this.tlpHDXPremium.ResumeLayout(false);
            this.tlpHDXPremium.PerformLayout();
            this.pnlTestCase16.ResumeLayout(false);
            this.pnlTestCase16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase16)).EndInit();
            this.pnlTestCase17.ResumeLayout(false);
            this.pnlTestCase17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase17)).EndInit();
            this.pnlTestCase19.ResumeLayout(false);
            this.pnlTestCase19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase19)).EndInit();
            this.pnlTestCase20.ResumeLayout(false);
            this.pnlTestCase20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase20)).EndInit();
            this.pnlTestCase22.ResumeLayout(false);
            this.pnlTestCase22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase22)).EndInit();
            this.pnlTestCase23.ResumeLayout(false);
            this.pnlTestCase23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTestCase23)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTopLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnViewReport;
        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.Label lblSeperatorLine1;
        private System.Windows.Forms.TabControl tabTestCases;
        private System.Windows.Forms.TabPage tabHDXReady;
        private System.Windows.Forms.TableLayoutPanel tlpHDXReady;
        private System.Windows.Forms.Label lblColHead2;
        private System.Windows.Forms.Label lblColHead4;
        private System.Windows.Forms.Label lblSlNum1;
        private System.Windows.Forms.Label lblSlNum2;
        private System.Windows.Forms.Label lblSlNum3;
        private System.Windows.Forms.Label lblSlNum4;
        private System.Windows.Forms.Label lblSlNum5;
        private System.Windows.Forms.Label lblSlNum6;
        private System.Windows.Forms.Label lblTestCase1;
        private System.Windows.Forms.Label lblTestCase2;
        private System.Windows.Forms.Label lblTestCase3;
        private System.Windows.Forms.Label lblTestCase4;
        private System.Windows.Forms.Label lblTestCase5;
        private System.Windows.Forms.Label lblTestCase6;
        private System.Windows.Forms.Label lblStatus1;
        private System.Windows.Forms.Label lblStatus2;
        private System.Windows.Forms.Label lblStatus3;
        private System.Windows.Forms.Label lblStatus4;
        private System.Windows.Forms.Label lblStatus5;
        private System.Windows.Forms.Label lblStatus6;
        private System.Windows.Forms.TabPage tabHDXPremium;
        private System.Windows.Forms.TabPage tabHDX3DPro;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lblColHead1;
        private System.Windows.Forms.TableLayoutPanel tlpHDXPremium;
        private System.Windows.Forms.Label lblColHead2HDXPremium;
        private System.Windows.Forms.Label lblColHead4HDXPremium;
        private System.Windows.Forms.Label lblSlNum9;
        private System.Windows.Forms.Label lblSlNum19;
        private System.Windows.Forms.Label lblSlNum12;
        private System.Windows.Forms.Label lblSlNum13;
        private System.Windows.Forms.Label lblSlNum14;
        private System.Windows.Forms.Label lblSlNum8;
        private System.Windows.Forms.Label lblTestCase16;
        private System.Windows.Forms.Label lblStatus16;
        private System.Windows.Forms.Label lblColHead1HDXPremium;
        private System.Windows.Forms.Label lblTestCase17;
        private System.Windows.Forms.Label lblTestCase19;
        private System.Windows.Forms.Label lblTestCase21;
        private System.Windows.Forms.Label lblTestCase20;
        private System.Windows.Forms.Label lblTestCase22;
        private System.Windows.Forms.Label lblStatus17;
        private System.Windows.Forms.Label lblStatus19;
        public System.Windows.Forms.Label lblStatus20;
        private System.Windows.Forms.Label lblStatus21;
        private System.Windows.Forms.Label lblStatus22;
        private System.Windows.Forms.TableLayoutPanel tlpHDX3DPro;
        private System.Windows.Forms.Label lblColHead2HDX3DPro;
        private System.Windows.Forms.Label lblColHead4HDX3DPro;
        private System.Windows.Forms.Label lblSlNum16;
        private System.Windows.Forms.Label lblSlNum17;
        private System.Windows.Forms.Label lblSlNum18;
        private System.Windows.Forms.Label lblColHead1HDX3DPro;
        private System.Windows.Forms.Label lblTestCase9;
        private System.Windows.Forms.Label lblSlNum15;
        private System.Windows.Forms.Label lblStatus9;
        private System.Windows.Forms.Label lblTestCase10;
        private System.Windows.Forms.Label lblTestCase11;
        private System.Windows.Forms.Label lblTestCase12;
        private System.Windows.Forms.Label lblStatus10;
        private System.Windows.Forms.Label lblStatus11;
        private System.Windows.Forms.Label lblStatus12;
        private System.Windows.Forms.Button btnValidate;
        private System.Windows.Forms.Label lblRequired1;
        private System.Windows.Forms.Label lblRequired2;
        private System.Windows.Forms.Label lblRequired3;
        private System.Windows.Forms.Label lblRequired4;
        private System.Windows.Forms.Label lblRequired5;
        private System.Windows.Forms.Label lblRequired6;
        private System.Windows.Forms.Label lblRequired8;
        private System.Windows.Forms.Label lblRequired9;
        private System.Windows.Forms.Label lblRequired11;
        private System.Windows.Forms.Label lblRequired12;
        private System.Windows.Forms.Label lblRequired13;
        private System.Windows.Forms.Label lblRequired14;
        private System.Windows.Forms.Label lblRequired15;
        private System.Windows.Forms.Label lblRequired16;
        private System.Windows.Forms.Label lblRequired17;
        private System.Windows.Forms.Label lblRequired18;
        private System.Windows.Forms.CheckBox chkTestCase1;
        private System.Windows.Forms.CheckBox chkTestCase2;
        private System.Windows.Forms.CheckBox chkTestCase3;
        private System.Windows.Forms.CheckBox chkTestCase4;
        private System.Windows.Forms.CheckBox chkTestCase5;
        private System.Windows.Forms.CheckBox chkTestCase6;
        private System.Windows.Forms.CheckBox chkTestCase16;
        private System.Windows.Forms.CheckBox chkTestCase17;
        private System.Windows.Forms.CheckBox chkTestCase18;
        private System.Windows.Forms.CheckBox chkTestCase19;
        private System.Windows.Forms.CheckBox chkTestCase20;
        private System.Windows.Forms.CheckBox chkTestCase21;
        private System.Windows.Forms.CheckBox chkTestCase22;
        private System.Windows.Forms.CheckBox chkTestCase9;
        private System.Windows.Forms.CheckBox chkTestCase10;
        private System.Windows.Forms.CheckBox chkTestCase11;
        private System.Windows.Forms.CheckBox chkTestCase12;
        private System.Windows.Forms.CheckBox chkTestCaseHDXReady;
        private System.Windows.Forms.CheckBox chkTestCaseHDXPremium;
        private System.Windows.Forms.CheckBox chkTestCaseHDX3DPro;
        private System.Windows.Forms.ToolTip ttHelp;
        private System.Windows.Forms.Panel pnlTestCase1;
        private System.Windows.Forms.PictureBox picboxTestCase1;
        private System.Windows.Forms.Panel pnlTestCase2;
        private System.Windows.Forms.PictureBox picboxTestCase2;
        private System.Windows.Forms.Label lblSampleFileLocation;
        private System.Windows.Forms.Button btnSampleFilesLocation;
        private System.Windows.Forms.Label lblSampleFileLocationValue;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogSampleFileLocation;
        private System.Windows.Forms.FolderBrowserDialog folderBrowseDialogVideoPath;
        private System.Windows.Forms.RadioButton testAudioDevices;
        private System.Windows.Forms.Label lblColHead3;
        private System.Windows.Forms.Label lblColHead3HDXPremium;
        private System.Windows.Forms.Label lblColHead3HDX3DPro;
        private System.Windows.Forms.Panel pnlTestCase3;
        private System.Windows.Forms.PictureBox picboxTestCase3;
        private System.Windows.Forms.Panel pnlTestCase4;
        private System.Windows.Forms.PictureBox picboxTestCase4;
        private System.Windows.Forms.Panel pnlTestCase5;
        private System.Windows.Forms.PictureBox picboxTestCase5;
        private System.Windows.Forms.Panel pnlTestCase6;
        private System.Windows.Forms.PictureBox picboxTestCase6;
        private System.Windows.Forms.Panel pnlTestCase16;
        private System.Windows.Forms.PictureBox picboxTestCase16;
        private System.Windows.Forms.Panel pnlTestCase17;
        private System.Windows.Forms.PictureBox picboxTestCase17;
        private System.Windows.Forms.Panel pnlTestCase19;
        private System.Windows.Forms.Panel pnlTestCase20;
        private System.Windows.Forms.PictureBox picboxTestCase20;
        private System.Windows.Forms.Panel pnlTestCase21;
        private System.Windows.Forms.PictureBox picboxTestCase21;
        private System.Windows.Forms.Panel pnlTestCase22;
        private System.Windows.Forms.PictureBox picboxTestCase22;
        private System.Windows.Forms.Panel pnlTestCase9;
        private System.Windows.Forms.Panel pnlTestCase10;
        private System.Windows.Forms.Panel pnlTestCase11;
        private System.Windows.Forms.Panel pnlTestCase12;
        private System.Windows.Forms.PictureBox picboxTestCase9;
        private System.Windows.Forms.PictureBox picboxTestCase10;
        private System.Windows.Forms.PictureBox picboxTestCase11;
        private System.Windows.Forms.PictureBox picboxTestCase12;
        private System.Windows.Forms.PictureBox picboxTopLogo;
        private System.Windows.Forms.CheckBox chkTestCase8;
        private System.Windows.Forms.Label lblSlNum7;
        private System.Windows.Forms.Panel pnlTestCase8;
        private System.Windows.Forms.PictureBox picboxTestCase8;
        private System.Windows.Forms.Label lblTestCase8;
        private System.Windows.Forms.Label lblStatus8;
        private System.Windows.Forms.Label lblBuildNumber;
        private System.Windows.Forms.Label lblNumNetScaler;
        private System.Windows.Forms.Label lblRequiredNetScaler;
        private System.Windows.Forms.CheckBox chkTestCase7;
        private System.Windows.Forms.Panel pnlTestCase7;
        private System.Windows.Forms.PictureBox picboxTestCase7;
        private System.Windows.Forms.Label lblTestCase7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblStatus7;
        private System.Windows.Forms.FolderBrowserDialog browseFolder;
        private System.Windows.Forms.Label lblprocessing;
        private System.Windows.Forms.CheckBox chkTestCase13;
        private System.Windows.Forms.CheckBox chkTestCase14;
        private System.Windows.Forms.CheckBox chkTestCase15;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblStatus15;
        private System.Windows.Forms.Label lblStatus14;
        private System.Windows.Forms.Label lblStatus13;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTestCase13;
        private System.Windows.Forms.Label lblTestCase14;
        private System.Windows.Forms.Label lblTestCase15;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblStatus23;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblTestCase23;
        private System.Windows.Forms.CheckBox chkTestCase23;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel pnlTestCase15;
        private System.Windows.Forms.PictureBox picboxTestCase15;
        private System.Windows.Forms.Panel pnlTestCase14;
        private System.Windows.Forms.PictureBox picboxTestCase14;
        private System.Windows.Forms.Panel pnlTestCase13;
        private System.Windows.Forms.PictureBox picboxTestCase13;
        private System.Windows.Forms.Panel pnlTestCase23;
        private System.Windows.Forms.PictureBox picboxTestCase23;
        private System.Windows.Forms.PictureBox picboxTestCase19;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox chkTestCase24;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox3;
    }
}