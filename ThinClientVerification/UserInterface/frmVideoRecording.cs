﻿using Microsoft.Expression.Encoder.ScreenCapture;
using System;
using System.Drawing;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;

using System.IO;
using Microsoft.Expression.Encoder.Devices;
using Microsoft.Expression.Encoder.Profiles;
using System.Collections.ObjectModel;
using ThinClient.Verification.Common;
using ThinClient.Verification.UserInterface;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;

namespace ThinClientVerification.UserInterface
{
    public partial class frmVideoRecording : Form
    {
        public static int width = Screen.PrimaryScreen.Bounds.Width;
        public static int height = Screen.PrimaryScreen.Bounds.Height;
        public static int btnPauseCount = 0;
        public static string filePath = "";
        public static string fileName = "";
        public static string videoName = "";
        public static ScreenCaptureJob vid;
        public static string pauseFlag = "pause";
        public static string msgUser = "";
        static System.Timers.Timer scrnCaptureTimer = new System.Timers.Timer();
        public static System.Timers.Timer stopWatch;
        public static TimeSpan elapsedTime = TimeSpan.Zero;
        public bool isTimerRunning = false;
        public static Stopwatch clock;

        public static frmVerification mainFrmCtrl;
        public static Button btnValidate;

        public static frmQuestionBanner bannerCtrl;
        public static byte testcaseName=0;
        public static string company_model = "";

        public frmVideoRecording(frmQuestionBanner frmBanner)
        {
            InitializeComponent();
            this.FormClosing += frmVideoRecording_FormClosing;

            bannerCtrl = frmBanner;

            clock = new Stopwatch();
            stopWatch = new System.Timers.Timer();
            stopWatch.Interval = 1000;
            stopWatch.Elapsed += new System.Timers.ElapsedEventHandler(StopWatch_Elapsed);
        }

        public frmVideoRecording(frmVerification frmBanner1, Button btnValid, string userInfo, byte testcaseNumber)
        {
            company_model = userInfo;
            testcaseName = testcaseNumber;
            videoName = company_model + "_TestCase" + testcaseName + "_" + DateTime.Now.ToString("ddMMyyTHHmmss") + ".mkv";
            fileName = System.IO.Directory.GetCurrentDirectory() + "\\Recording\\" +videoName;

            // sbResult1 = sbResult;
            InitializeComponent();
            this.FormClosing += frmVideoRecording_FormClosing;
            mainFrmCtrl = frmBanner1;
            btnValidate = btnValid;

            clock = new Stopwatch();
            stopWatch = new System.Timers.Timer();
            stopWatch.Interval = 1000;
            stopWatch.Elapsed += new System.Timers.ElapsedEventHandler(StopWatch_Elapsed);
        }
        private void StopWatch_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                elapsedTime = clock.Elapsed;
                elapsedTime = new TimeSpan(elapsedTime.Hours, elapsedTime.Minutes, elapsedTime.Seconds);
                this.lblTimer.BeginInvoke((MethodInvoker)delegate () { this.lblTimer.Text = elapsedTime.ToString(); ; });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void frmVideoRecording_Load(object sender, EventArgs e)
        {
            Constants.isRecorderOpen = true;
            btnStop.Enabled = true;
            btnPlay.Enabled = false;
            btnPause.Enabled = true;
            btnRecord.Enabled = false;
            Screen myScreen = Screen.PrimaryScreen;
            int mywidth = (myScreen.Bounds.Width);
            int myheight = (myScreen.Bounds.Height);
            this.Location = new Point(mywidth - this.Width - 20, 20);
            lblWatermark.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            btnValidate.Enabled = false;
            mainFrmCtrl.Enabled = false;
            RecordVideo();
            
        }

        private void btnRecord_Click(object sender, EventArgs e)
        {
            btnRecord.Enabled = false;
            btnPause.Enabled = true;
            btnStop.Enabled = true;
            btnPlay.Enabled = false;
            RecordVideo();
            
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            btnRecord.Enabled = false;
            btnStop.Enabled = true;
            btnPlay.Enabled = false;

            if (pauseFlag == "pause")
            {
                vid.Pause();
                if (isTimerRunning == true)
                {
                    stopWatch.Stop();
                    clock.Stop();
                    isTimerRunning = false;
                }
                pauseFlag = "resume";
                btnPause.Image = Properties.Resources.niEX5xpKT;
            }
            else
            {
                vid.Resume();
                if (isTimerRunning == false)
                {
                    stopWatch.Start();
                    clock.Start();
                    isTimerRunning = true;
                }
                pauseFlag = "pause";
                btnPause.Image = Properties.Resources.player_pause;
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
           
            btnStop.Enabled = false;
            btnPause.Enabled = false;
            btnRecord.Enabled = true;
            btnPlay.Enabled = true;
            vid.Stop();

            stopWatch.Stop();
            clock.Stop();
            clock.Reset();
            isTimerRunning = false;
            elapsedTime = TimeSpan.Zero;

            mainFrmCtrl.WindowState = FormWindowState.Normal;
            btnValidate.Enabled = true;
            mainFrmCtrl.Enabled = true;

            filePath = System.IO.Directory.GetCurrentDirectory() + "\\Recording";
            msgUser = "1) Your media file ‘" + videoName + "’ has been successfully processed and saved at the below location: " + filePath + "\n" +
                "2) URL https://citrixready.sharefile.com/r-r3888eaf8511346c1be24d84ca335e5b1 is opened." + "\n" +
                 "3) Please upload " + videoName + " " + "\n";
                
            MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
           
            ProcessStartInfo psStartInfo = new ProcessStartInfo();
            psStartInfo.WindowStyle = ProcessWindowStyle.Maximized;

            psStartInfo.FileName = @"" + "\"" + Constants.APP_PROCESS_INTERNETEXPLORER + "\"";
            psStartInfo.Arguments = @"" + "\"" + Constants.UPLOAD_MEDIA_FILE + "\"";
            Process ps = Process.Start(psStartInfo);
           
            //screenshot                      
            scrnCaptureTimer.Interval = 1000;
            scrnCaptureTimer.AutoReset = false;
            scrnCaptureTimer.Start();
            scrnCaptureTimer.Elapsed += new System.Timers.ElapsedEventHandler(CaptureScreen);
          
            Globals.VerificationScreenResult.Append("Video is saved "+videoName);

            msgUser = "Please close recording tool.";
            MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

private void btnPlay_Click(object sender, EventArgs e)
{
btnPlay.Enabled = false;
btnStop.Enabled = false;
btnPause.Enabled = false;
btnRecord.Enabled = true;

msgUser = "Video not available in the destination folder!";
if (File.Exists(fileName))
{
   ProcessStartInfo psStartInfo = new ProcessStartInfo();
   psStartInfo.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_WINDOWSMEDIAPLAYER + "\"";
   psStartInfo.Arguments = @" " + "\"" + fileName + "\" /fullscreen";
   psStartInfo.WindowStyle = ProcessWindowStyle.Maximized;
   Process ps = Process.Start(psStartInfo);
}
else { MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information); }

}

private void frmVideoRecording_FormClosing(object sender, FormClosingEventArgs e)
{
if (e.CloseReason == CloseReason.UserClosing)
{
   mainFrmCtrl.WindowState = FormWindowState.Normal;
   if (btnStop.Enabled == true)
   {
                    /*msgUser = "Are you sure you want to stop the recording?." + "\n\n" +
                              "Please refer the path (ThinClientVerification\\bin\\Debug\\Recording) to get the recordings";
                    DialogResult msgResult;
                    msgResult = MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    if (msgResult == DialogResult.OK)

                   {
                       vid.Stop();
                       btnValidate.Enabled = true;
                       mainFrmCtrl.Enabled = true;
                       Constants.isRecorderOpen = false;
                   }*/

                    filePath = System.IO.Directory.GetCurrentDirectory() + "\\Recording"; 
                    msgUser = "Are you sure you want to stop the recording?." + "\n\n" + 
                        "1) Your media file ‘" + videoName + "’ will be saved at the below location: " + filePath + "\n" +
                        "2) URL https://citrixready.sharefile.com/r-r58ecfa975f884fbead03ce417ccd3ab9 will be opened." + "\n" + 
                        "3) Please upload " + videoName + " " + "\n"; DialogResult msgResult; 
                    
                    msgResult = MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information); 
                    if (msgResult == DialogResult.OK)
                    {
                        vid.Stop(); 
                        btnValidate.Enabled = true; 
                        mainFrmCtrl.Enabled = true; 
                        Constants.isRecorderOpen = false;

                        ProcessStartInfo psStartInfo = new ProcessStartInfo();                        
                        psStartInfo.WindowStyle = ProcessWindowStyle.Maximized;                        
                        psStartInfo.FileName = @"" + "\"" + Constants.APP_PROCESS_INTERNETEXPLORER + "\"";                        
                        psStartInfo.Arguments = @"" + "\"" + Constants.UPLOAD_MEDIA_FILE + "\"";                        
                        Process ps = Process.Start(psStartInfo);
                        //Thread.Sleep(3000);
                        //screenshot
                        /*scrnCaptureTimer.Interval = 1000;                        
                        scrnCaptureTimer.AutoReset = false;                        
                        scrnCaptureTimer.Start();                        
                        scrnCaptureTimer.Elapsed += new System.Timers.ElapsedEventHandler(CaptureScreen); */                       
                        Globals.VerificationScreenResult.Append("Video is saved " + videoName);                        
                        //
                                         
                    }


       else
       {
           e.Cancel = true;
           this.Focus();
       }
   }
}
Constants.isRecorderOpen = false;
}


public void RecordVideo()
{
try
{

   vid = new ScreenCaptureJob();

   vid.OutputScreenCaptureFileName = fileName;

   Collection<EncoderDevice> devices = EncoderDevices.FindDevices(EncoderDeviceType.Audio);

                // Iterates through all audio devices and selects the internal audio
                /*foreach (EncoderDevice device in devices)
                {

                    if (!device.Name.ToLower().Contains("speaker") & !device.Name.ToLower().Contains("earphone"))
                    { vid.AddAudioDeviceSource(device); }

                }*/
   //vid.CaptureRectangle = new Rectangle(3, 3, 20, 10);
   vid.CaptureRectangle = new Rectangle(0, 0, width - width % 8, height - height % 8);

   vid.ScreenCaptureVideoProfile = new ScreenCaptureVideoProfile();
       vid.ShowFlashingBoundary = false;
       vid.ScreenCaptureVideoProfile.Quality = 40;
       vid.ScreenCaptureVideoProfile.FrameRate = 25;
       vid.CaptureMouseCursor = true;
       vid.ScreenCaptureVideoProfile.Force16Pixels = true;

       // Starts capture
       vid.Start();

       if (isTimerRunning == false)
       {
           stopWatch.Start();
           clock.Start();
           isTimerRunning = true;
       }

}
catch (Exception ex)
{
   MessageBox.Show("A runtime exception occured: " + ex.Message + "Please try again");
}

}
        private static void CreateFolderIfNotAvailable(string path)
        {
            bool folderExists = Directory.Exists(path);
            if (!folderExists)
            {
                Directory.CreateDirectory(path);
            }
        }


        private static void CaptureScreen(object sender, EventArgs e)
        {
            scrnCaptureTimer.Stop();

            int screenLeft = SystemInformation.VirtualScreen.Left;
            int screenTop = SystemInformation.VirtualScreen.Top;
            int screenWidth = SystemInformation.VirtualScreen.Width;
            int screenHeight = SystemInformation.VirtualScreen.Height;

            Bitmap bmpScreenshot = new Bitmap(screenWidth, screenHeight, PixelFormat.Format32bppArgb);
            var gfxScreenshot = Graphics.FromImage(bmpScreenshot);


            gfxScreenshot.CopyFromScreen(screenLeft, screenTop, 0, 0, bmpScreenshot.Size, CopyPixelOperation.SourceCopy);
            string currentDirectory = System.IO.Directory.GetCurrentDirectory();
            CreateFolderIfNotAvailable(currentDirectory + "\\Screenshots");

            Globals.screenshotName = System.IO.Directory.GetCurrentDirectory() + "\\Screenshots\\Screenshot_" + DateTime.Now.ToString("yyyyMMddTHHmmss") + ".jpeg";
            bmpScreenshot.Save(Globals.screenshotName, ImageFormat.Jpeg);
        }
     
    }
    }
