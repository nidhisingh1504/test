﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using ThinClient.Verification.Common;
using ThinClient.Verification.UserInterface;
using static ThinClient.Verification.Common.Utilities;

namespace ThinClientVerification.UserInterface
{
    public partial class frmQuestionBanner : Form
    {
        public static frmVerification mainfrmCtrl;
        public static frmVideoRecording frmVideo;
        public static int QsnCount;
        public static int ClickCount;
        public static bool IsButton = false;
        Form _frm;
        int _testcaseid;
        Process _ps;
        int width;
        int height;
        XDocument xmlDoc;

        public frmQuestionBanner(frmVerification frmCtrl, int testcaseId, Process ps)
        {
            _frm = frmCtrl;
            _frm.Hide();
            _testcaseid = testcaseId;
            _ps = ps;

            InitializeComponent();
            SystemEvents.DisplaySettingsChanged += systemEvents_DisplaySettingsChanged;

        }

        private void systemEvents_DisplaySettingsChanged(object sender, EventArgs e)
        {
            try
            {
                Screen screen = Screen.PrimaryScreen;
                width = screen.Bounds.Width;
                height = screen.Bounds.Height;
                this.AutoSize = true;
                this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                lblQuestion.AutoSize = true;
                if (Screen.PrimaryScreen.Bounds.Width > 1000)
                {
                    this.MaximumSize = new Size(960, 500);
                    lblQuestion.MaximumSize = new System.Drawing.Size(940, 0);
                }
                else
                {
                    this.MaximumSize = new Size(width - 40, 500);
                    lblQuestion.MaximumSize = new System.Drawing.Size(this.width - 30, 0);
                }
                this.Location = new Point(20, 20);
                this.BringToFront();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Globals.counter--;
            if (Constants.IsGuidelines == true)
            {
                lblHeader.Text = "Step" + " " + Globals.counter.ToString() + " " + "of" + " " + (QsnCount - 1);
            }
            else
            {
                lblHeader.Text = "Step" + " " + Globals.counter.ToString() + " " + "of" + " " + QsnCount;
            }
            ClickCount = Globals.counter;

            if (Constants.IsGuidelines == true)
            {
                if (Globals.counter == 0)
                {
                    btnBack.Enabled = false;
                    lblHeader.Text = "Guidelines";
                }
            }
            else
            {
                if (Globals.counter == 1)
                {
                    btnBack.Enabled = false;
                }
            }

            if (Constants.IsGuidelines == true)
            {
                if (Globals.counter == QsnCount - 2)
                {
                    btnNext.Text = "Next";
                }
            }
            else
            {
                if (Globals.counter == QsnCount - 1)
                {
                    btnNext.Text = "Next";
                }
            }
            btnNext.Enabled = true;


            var Question = from questions in xmlDoc.Descendants("Question")
                           where questions.Element("No").Value == ClickCount.ToString()
                           select new
                           {
                               Qsn = questions.Element("Qsn").Value,
                           };
            lblQuestion.Text = "";
            foreach (var q in Question)
            {
                lblQuestion.Text = lblQuestion.Text + q.Qsn + "\n";
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Globals.counter++;

            ClickCount = Globals.counter;

            if (QsnCount != 1)
            {
                if (Constants.IsGuidelines == true)
                {
                    if (Globals.counter == 1)
                    {
                        btnBack.Enabled = true;
                    }
                }
                else
                {
                    if (Globals.counter == 2)
                    {
                        btnBack.Enabled = true;
                    }
                }
            }


            if (Constants.IsGuidelines == true)
            {
                if (Globals.counter != QsnCount)
                {
                    if (QsnCount != 1)
                    {
                        if (Constants.IsGuidelines == true)
                        {
                            lblHeader.Text = "Step" + " " + Globals.counter.ToString() + " " + "of" + " " + (QsnCount - 1);
                        }
                        else
                        {
                            lblHeader.Text = "Step" + " " + Globals.counter.ToString() + " " + "of" + " " + QsnCount;
                        }
                    }

                    var Question = from questions in xmlDoc.Descendants("Question")
                                   where questions.Element("No").Value == ClickCount.ToString()
                                   select new
                                   {
                                       Qsn = questions.Element("Qsn").Value,
                                   };
                    lblQuestion.Text = "";
                    foreach (var q in Question)
                    {
                        lblQuestion.Text = lblQuestion.Text + q.Qsn + "\n";
                    }
                }

            }
            else
            {
                if (Globals.counter != QsnCount + 1)
                {
                    if (QsnCount != 1)
                    {
                        if (Constants.IsGuidelines == true)
                        {
                            lblHeader.Text = "Step" + " " + Globals.counter.ToString() + " " + "of" + " " + (QsnCount - 1);
                        }
                        else
                        {
                            lblHeader.Text = "Step" + " " + Globals.counter.ToString() + " " + "of" + " " + QsnCount;
                        }
                    }

                    var Question = from questions in xmlDoc.Descendants("Question")
                                   where questions.Element("No").Value == ClickCount.ToString()
                                   select new
                                   {
                                       Qsn = questions.Element("Qsn").Value,
                                   };
                    lblQuestion.Text = "";
                    foreach (var q in Question)
                    {
                        lblQuestion.Text = lblQuestion.Text + q.Qsn + "\n";
                    }
                }
            }

            if (Constants.IsGuidelines == true)
            {
                if (Globals.counter == QsnCount - 1)
                {
                    btnNext.Text = "Exit";
                }
            }
            else
            {
                if (Globals.counter == QsnCount)
                {
                    btnNext.Text = "Exit";
                }
            }

            if (Constants.IsGuidelines == true)
            {
                if (Globals.counter == QsnCount)
                {
                    //  Globals.RTOP = "";
                    //  Globals.VOIP = "";

                    ////  Globals.counter = 0;
                    // // QsnCount = 0;
                    if (_testcaseid == 13 || _testcaseid == 7 || _testcaseid == 9)
                    {
                        //Globals.isAudacityTestCaseCompleted = true;
                        //Globals.isRTOPCompleted = true;
                        if (!_ps.HasExited)
                        {
                            _ps.Kill();
                        }
                    }
                    //if (_frm != null)
                    //{
                    //    _frm.Show();
                    //}
                    IsButton = true;
                    this.Close();
                    // lblHeader.Text = "";
                }
            }
            else
            {
                if (Globals.counter == QsnCount + 1)
                {

                    if (_testcaseid == 13 || _testcaseid == 7 || _testcaseid == 9)
                    {
                        //Globals.isAudacityTestCaseCompleted = true;
                        //Globals.isRTOPCompleted = true;
                        if (!_ps.HasExited)
                        {
                            _ps.Kill();
                        }
                    }
                    //if (_frm != null)
                    //{
                    //    _frm.Show();
                    //}
                    IsButton = true;
                    this.Close();
                    //   lblHeader.Text = "";
                }
            }


        }

        private void frmQuestionBanner_Load(object sender, EventArgs e)
        {
            xmlDoc = null;
            this.BringToFront();
            btnBack.Enabled = false;
            

            if (Globals.RTOP == "RTOP")
            {

                xmlDoc = XDocument.Load(Environment.CurrentDirectory + @"\QSN_RTOP.xml");
                this.Text = "HDX RTOP with Skype For Business";

            }
            if (Globals.VOIP == "VOIP")
            {
                xmlDoc = XDocument.Load(Environment.CurrentDirectory + @"\QSN_VOIP.xml");
                this.Text = "HDX Real-Time Audio (VoIP)";
            }
            QsnCount = xmlDoc.Root.Elements().Count();


            if (Globals.counter != QsnCount + 1)
            {
                var Question = xmlDoc.Descendants("Question").Where(x => x.Element("No").Value == "0");
                if (Question.Count() > 0)
                {
                    lblHeader.Text = "Guidelines";
                    lblQuestion.Text = lblQuestion.Text + Question.Elements("Qsn").FirstOrDefault().Value;
                    Constants.IsGuidelines = true;
                }

                else
                {
                    var Question2 = xmlDoc.Descendants("Question").Where(x => x.Element("No").Value == "1");
                    if (Question2.Count() > 0)
                    {
                        lblQuestion.Text = lblQuestion.Text + Question2.Elements("Qsn").FirstOrDefault().Value;
                        lblHeader.Text = "Step" + " " + "1" + " " + "of" + " " + QsnCount;
                        if (QsnCount == 1)
                        {
                            btnNext.Text = "Exit";
                        }
                    }
                }

                if (Constants.IsGuidelines == false)
                {
                    Globals.counter = 1;
                }

                Globals.recordingMessage = false;
                Globals.IsVideoRecorded = false;
                Globals.isAudacityTestCaseCompleted = false;
                Globals.isRTOPCompleted1 = false;
                Globals.isRTOPCompleted2 = false;

            }


            Form f2 = Application.OpenForms["frmVideoRecording"];
            if (f2 != null)
                f2.BringToFront();
            else
            {
                frmVideo = new frmVideoRecording(this);
                if (Constants.isRecorderOpen == false) { frmVideo.Show(); }
            }


        }



        private void frmQuestionBanner_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (!Globals.recordingMessage)
            {
                if (MessageBox.Show("Are you sure you want to stop the execution of the test case?", Constants.MSGBOX_CAPTION, MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                {
                    e.Cancel = true;
                    if(IsButton == true)
                    {
                        if (Constants.IsGuidelines == true)
                        {
                            Globals.counter = QsnCount - 1;
                        }
                        else
                        {
                            Globals.counter = QsnCount;
                        }
                    }
                }
                else
                {
                    Constants.IsGuidelines = false;
                    Globals.RTOP = "";
                    Globals.VOIP = "";
                    Globals.counter = 0;
                    QsnCount = 0;
                    Globals.recordingMessage = true;
                    Form f = Application.OpenForms["frmVideoRecording"];
                    if (f != null)
                        f.Close();
                    if (_frm != null)
                    {
                        _frm.Show();

                    }


                }
            }
            else
            {
                if (_frm != null)
                {
                    _frm.Show();
                }
            }



        }
    }
}
