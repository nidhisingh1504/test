﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThinClient.Verification.Common;
using ThinClient.Verification.UserInterface;

namespace ThinClientVerification
{

    public partial class frmCredential : Form
    {


        public frmCredential()
        {
            InitializeComponent();               
            lblSeperatorLine1.Visible = false;       

            this.AcceptButton = btnSubmit;
           
        }

        public static string AddDoubleQuotes(string value)
        {
            return "\"" + value + "\"";
        }
        public string GetUniqueSiteName()
        {
            string siteName = "Site";
            var uniqueKey = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 6);
            return siteName + uniqueKey;

        }
        public string CreatePolicyScript(string templateName, string policyName)
        {
            string uniqueSiteName = GetUniqueSiteName();
            var script =
  @"Add-PSSnapin Citrix.Common.GroupPolicy" + Environment.NewLine +
  @"New-PSDrive " + uniqueSiteName + " –PSProvider CitrixGroupPolicy –Root \\ -Controller localhost" + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\" + Environment.NewLine +
  @"copy-item Templates:\" + templateName + " " + uniqueSiteName + ":\\User\\" + policyName + Environment.NewLine +
  @"copy-item Templates:\" + templateName + " " + uniqueSiteName + ":\\Computer\\" + policyName + Environment.NewLine;

            return script;
        }
        public string CheckCitrixPolicyExist(string policyName)
        {

            string uniqueSiteName = GetUniqueSiteName();
            var script =
  @"Add-PSSnapin Citrix.Common.GroupPolicy" + Environment.NewLine +
  @"New-PSDrive " + uniqueSiteName + " –PSProvider CitrixGroupPolicy –Root \\ -Controller localhost" + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\User\\" + policyName + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\Computer\\" + policyName + Environment.NewLine;
            return script;
        }
        public string GetCitrixPolicyListScript()
        {
            string uniqueSiteName = GetUniqueSiteName();
            var script =
  @"Add-PSSnapin Citrix.Common.GroupPolicy" + Environment.NewLine +
  @"New-PSDrive " + uniqueSiteName + " –PSProvider CitrixGroupPolicy –Root \\ -Controller localhost" + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\User" + Environment.NewLine +
  @"dir" + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\Computer" + Environment.NewLine +
  @"dir" + Environment.NewLine;
            return script;
        }
        public string GetPolicyStatusChangeScript(string policyName, string statusName)
        {
            string uniqueSiteName = GetUniqueSiteName();
            var script =
  @"Add-PSSnapin Citrix.Common.GroupPolicy" + Environment.NewLine +
  @"New-PSDrive " + uniqueSiteName + " –PSProvider CitrixGroupPolicy –Root \\ -Controller localhost" + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\" + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\User\\" + policyName + Environment.NewLine +
  @"Set-ItemProperty . –Name Enabled –Value " + statusName + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\Computer\\" + policyName + Environment.NewLine +
  @"Set-ItemProperty . –Name Enabled –Value " + statusName;
            return script;

        }
        public static string EnablePowerShellPermission(string ipAddress)
        {
            System.Collections.ObjectModel.Collection<PSObject> results = new System.Collections.ObjectModel.Collection<PSObject>();
            string ip = AddDoubleQuotes(ipAddress);
            string script = "winrm s winrm/config/client '@{TrustedHosts=" + ip + "}'"; // whatever you want to execute

            // step 1 entered
            //StreamWriter fileLogHDXRTOP = new StreamWriter(Constants.LOG_FILE_NAME, true);
            //fileLogHDXRTOP.WriteLine("Step 1 entered");
            //fileLogHDXRTOP.Close();

            //string script1 = @"Clear-Item -Path WSMan:\localhost\Client\TrustedHosts –Force";

            using (Runspace runspace = RunspaceFactory.CreateRunspace())
            {
                try
                {
                    //StreamWriter fileLogHDXRTOP1 = new StreamWriter(Constants.LOG_FILE_NAME, true);
                    runspace.Open();
                    using (Pipeline pipeline = runspace.CreatePipeline())
                    {
                        // log

                      //  fileLogHDXRTOP1.WriteLine("Step 2 pipeline entered");


                        pipeline.Commands.AddScript(script);
                        results = pipeline.Invoke();
                       // fileLogHDXRTOP1.WriteLine("Step 3  script invokrd");
                    }

                    //fileLogHDXRTOP1.Close();
                }
                finally
                {
                    runspace.Close();
                }
            }

            // convert the script result into a single string
            StringBuilder stringBuilder = new StringBuilder();
            foreach (PSObject obj in results)
            {
                stringBuilder.AppendLine(obj.ToString());
            }
            return stringBuilder.ToString();


        }
        public static string ExecuteScriptHandledPolicyExist(string psScript, WSManConnectionInfo connectionInfo)
        {
            string response = "";
            System.Collections.ObjectModel.Collection<PSObject> results = new System.Collections.ObjectModel.Collection<PSObject>();
            try
            {
                using (Runspace runspace = RunspaceFactory.CreateRunspace(connectionInfo))
                {
                    try
                    {
                        runspace.Open();

                        using (Pipeline pipeline = runspace.CreatePipeline())
                        {

                            pipeline.Commands.AddScript(psScript);
                            results = pipeline.Invoke();
                            if (pipeline.Error.Count > 0)
                            {
                                response = "PolicyNotExist";
                            }
                            else
                            {
                                response = "PolicyFound";
                            }
                        }
                    }
                    finally
                    {
                        runspace.Close();
                    }
                }

                // Not Required to Read

                // convert the script result into a single string
                //StringBuilder stringBuilder = new StringBuilder();
                //foreach (PSObject obj in results)
                //{
                //    stringBuilder.AppendLine(obj.ToString());
                //}

                // response = stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                response = "";
                MessageBox.Show(ex.Message + "dtk" + ex.StackTrace);
            }

            return response;

        }
        public static string ExecuteScript(string psScript, WSManConnectionInfo connectionInfo)
        {

            System.Collections.ObjectModel.Collection<PSObject> results = new System.Collections.ObjectModel.Collection<PSObject>();
            try
            {
                using (Runspace runspace = RunspaceFactory.CreateRunspace(connectionInfo))
                {
                    try
                    {
                        runspace.Open();
                        using (Pipeline pipeline = runspace.CreatePipeline())
                        {

                            pipeline.Commands.AddScript(psScript);
                            results = pipeline.Invoke();
                        }
                    }
                    finally
                    {
                        runspace.Close();
                    }
                }

                // convert the script result into a single string
                StringBuilder stringBuilder = new StringBuilder();
                foreach (PSObject obj in results)
                {
                    stringBuilder.AppendLine(obj.ToString());
                }
                return stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public string PowerShellScriptCall(string serverIpAddress, string username, string password, string callmethod, string policytitle, string status = "")
        {
            string result = "";

            try
            {

                string domainName = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;

                //string username = "rt";
                string fullName = domainName + "\\" + username;

                var str = password;

                var sc = new SecureString();
                foreach (char c in str) sc.AppendChar(c);


                WSManConnectionInfo connectionInfo = new WSManConnectionInfo(false, Constants.CitrixStudioIP, 5985, "/wsman", "http://schemas.microsoft.com/powershell/Microsoft.PowerShell", new System.Management.Automation.PSCredential(fullName, sc));
                string Hdx_Ready_PolicyName = "HDX_Ready";
                //string Hdx_Ready_PolicyName = "Standard";
                string Hdx_Premium_PolicyName = "HDX_Premium";
                string Hdx_Pro_PolicyName = "HDX_3D_Pro";
                //string Hdx_Premium_PolicyName = "Premium";
                //string Hdx_Pro_PolicyName = "Standard_contd";

                string Hdx_Ready_TemplateName = AddDoubleQuotes("HDX Ready");
                //string Hdx_Ready_TemplateName = AddDoubleQuotes("Standard");
                string Hdx_Premium_TemplateName = AddDoubleQuotes("HDX Premium");
                string Hdx_Pro_TemplateName = AddDoubleQuotes("HDX 3D Pro");
                //string Hdx_Premium_TemplateName = AddDoubleQuotes("Premium");
                //string Hdx_Pro_TemplateName = AddDoubleQuotes("Standard contd");

                string policyname = "";
                string templatename = "";
                switch (policytitle)
                {
                    case "1":
                        policyname = Hdx_Ready_PolicyName;
                        templatename = Hdx_Ready_TemplateName;
                        break;

                    case "2":
                        policyname = Hdx_Premium_PolicyName;
                        templatename = Hdx_Premium_TemplateName;
                        break;

                    case "3":
                        policyname = Hdx_Pro_PolicyName;
                        templatename = Hdx_Pro_TemplateName;
                        break;

                }


                //var checkPolicyExist()
                var getPolicyExistResponse = "";
                if (callmethod == "Create")
                {
                    getPolicyExistResponse = ExecuteScriptHandledPolicyExist(CheckCitrixPolicyExist(policyname), connectionInfo);
                    if (getPolicyExistResponse == "PolicyNotExist")
                    {
                        var createPolicyResponse = ExecuteScript(CreatePolicyScript(templatename, policyname), connectionInfo);
                        if (!string.IsNullOrEmpty(createPolicyResponse))
                        {
                            var getPolicyResponse = ExecuteScript(GetCitrixPolicyListScript(), connectionInfo);
                            if (getPolicyResponse.Contains("Name=" + policyname))
                            {
                                result = "Policy :" + policyname + "  Already Exist in Studio";
                            }
                        }
                    }
                    else
                    {
                        result = "Policy :" + policyname + "  Already Exist in Studio";
                    }
                }
                else if (callmethod == "StatusChange")
                {
                    // follow like above method
                    getPolicyExistResponse = ExecuteScriptHandledPolicyExist(CheckCitrixPolicyExist(policyname), connectionInfo);
                    if (getPolicyExistResponse == "PolicyFound")
                    {
                        getPolicyExistResponse = ExecuteScript(GetPolicyStatusChangeScript(policyname, status), connectionInfo);
                    }

                }
                //if (getPolicyExistResponse == "PolicyFound")
                //{
                //    var HDX3DdisableResponse1 = PowerShellScriptCall(Constants.CitrixStudioIP, Constants.CitrixStudioUserName, Constants.CitrixStudioPassword, "StatusChange", "1", "true");
                //    // MessageBox.Show("Enabled");
                //}
                //var s1 = ExecuteScript(GetPolicyStatusChangeScript ("HDX_Premium","True"), connectionInfo);
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

      
        public static string StartWinRMService()
        {
            System.Collections.ObjectModel.Collection<PSObject> results = new System.Collections.ObjectModel.Collection<PSObject>();

            //  string script = "start-service winrm"; // whatever you want to execute

            string remotingScript = "Enable-PSRemoting -SkipNetworkProfileCheck -Force";

            //string script1 = @"Clear-Item -Path WSMan:\localhost\Client\TrustedHosts –Force";

            using (Runspace runspace = RunspaceFactory.CreateRunspace())
            {
                try
                {
                    runspace.Open();
                    using (Pipeline pipeline = runspace.CreatePipeline())
                    {
                        //   pipeline.Commands.AddScript(script);
                        pipeline.Commands.AddScript(remotingScript);
                        results = pipeline.Invoke();
                    }
                }
                finally
                {
                    runspace.Close();
                }
            }

            // convert the script result into a single string
            StringBuilder stringBuilder = new StringBuilder();
            foreach (PSObject obj in results)
            {
                stringBuilder.AppendLine(obj.ToString());
            }
            return stringBuilder.ToString();


        }

        [DllImport("advapi32.DLL", SetLastError = true)]
        public static extern int LogonUser(string lpszUsername, string lpszDomain, string lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);


        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();

            //string msgUser = "Please Provide cardentials" + "\n" + "\n" +
            //  "Click OK to exit the application or Cancel to abort.";
            //DialogResult result = MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            //if (result == DialogResult.OK)
            //{
            //    this.Close();
            //    Application.Exit();
            //}
        }

        private void frmCardential_Load(object sender, EventArgs e)
        {
            // pictureBox1.Visible = false;
            lblprocessing.Visible = false;
            lbldomain.Text = "Log on to domain : " + Constants.CitrixStudioDomainName;
        

        }
        public string ValidateInput(string Ip, string username, string password)
        {
            string message = "";
            if (!string.IsNullOrEmpty(Ip))
            {
                if (Regex.IsMatch(Ip, @"^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$"))
                {

                }
                else if(Regex.IsMatch(Ip, @"^[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(/\S*)?$"))
                {

                }
                else
                {
                    message = "Please provide valid Hostname (or) Ip Address.";
                    return message;
                }
            }
            else
            {
                message = "Please provide the host name or IP Address of Citrix Studio machine.";
                return message;
            }
            if (!string.IsNullOrEmpty(username))
            {
                string[] new_x = username.Split('\\');
                if (new_x.Count() == 2)
                {

                }
                else
                {
                    message = "Please enter valid domain.domainname\\username.";
                    return message;

                }
            }
            else
            {
                message = "To connect to Citrix Studio machine, type domain name\\username.";





                return message;
            }
            if (string.IsNullOrEmpty(password))
            {
                message = "Please provide administrator password to connect to Citrix studio machine.";
                return message;
            }
            return message = "";
        }
        private async void btnSubmit_Click(object sender, EventArgs e)
        {
            //NEW CODE
            DisableItem();
            string newFileName = System.IO.Path.GetRandomFileName();
            try
            {

                Constants.ValidationMsg = ValidateInput(txtIP.Text, txtUserName.Text, txtPassword.Text);

                if (string.IsNullOrEmpty(Constants.ValidationMsg))
                {

                    // pictureBox1.Visible = true;
                    lblprocessing.Visible = true;

                     await Task.Run(() => {
                        string domainusername = txtUserName.Text;

                        String[] splittedUsername = domainusername.Split('\\');

                        string domain = splittedUsername[0];
                        string uname = splittedUsername[1];
                        string domainUserFoldername = uname + "." + domain;


                        var t = Globals.Selected_HDX_Category.ToString();

                        var impersonationContext = new WrappedImpersonationContext(domain, uname, txtPassword.Text);
                        impersonationContext.Enter();
                      
                        string usernameGroupPolicyPath = "";
                        string domainUsernamegroupPolicyPath = "";
                        string BaseDirectory = "\\\\" + txtIP.Text + "\\C$\\Users\\";
                        string userName = uname;
                        string appDataDirectory = "\\AppData\\Local\\Citrix";
                        string fullPathUserName = BaseDirectory + userName + appDataDirectory;

                        string fullPathDomainUserName = BaseDirectory + domainUserFoldername + appDataDirectory;
                        

                        if (Directory.Exists(BaseDirectory + userName))
                        {
                            if (!Directory.Exists(fullPathUserName))
                            {
                                System.IO.Directory.CreateDirectory(fullPathUserName);
                            }
                        }
                      

                        if (Directory.Exists(BaseDirectory + domainUserFoldername))
                        {
                            if (!Directory.Exists(fullPathDomainUserName))
                            {
                                System.IO.Directory.CreateDirectory(fullPathDomainUserName);
                            }
                        }

                     // this is for Username folder 
                     try{


                             var foldersFound = Directory.GetDirectories(fullPathUserName, "Templates", SearchOption.AllDirectories);
                             if (foldersFound.Count() == 0)
                             {
                                 System.IO.Directory.CreateDirectory(fullPathUserName + "\\GroupPolicy\\Templates");
                                 usernameGroupPolicyPath = fullPathUserName + "\\GroupPolicy\\Templates";
                             }
                             else
                             {
                                 usernameGroupPolicyPath = foldersFound[0];
                             }
                         }
                         catch(Exception xe)
                         {
                             MessageBox.Show(xe.Message);
                         }

                       
                        try
                        {
                            DirectoryInfo templatedir1 = new DirectoryInfo(usernameGroupPolicyPath);
                            FileInfo[] filesInDir1 = templatedir1.GetFiles("*.gpt");
                            var gptFileDirectory = new DirectoryInfo(Environment.CurrentDirectory);
                            FileInfo[] gptFiles = gptFileDirectory.GetFiles("*.gpt");
                            if (File.Exists(usernameGroupPolicyPath + "\\templates.gpt"))
                            {
                                File.Delete(usernameGroupPolicyPath + "\\templates.gpt");
                            }
                            foreach (var item in gptFiles)
                            {

                                System.IO.File.Copy(item.FullName, usernameGroupPolicyPath + "\\" + item.Name, true);

                            }
                        }
                        catch
                        {

                        }

                        System.Threading.Thread.Sleep(10000);
                        // this is for Domain Username folder 
                        if (Directory.Exists(BaseDirectory + domainUserFoldername))
                        {
                            var domainfoldersFound = Directory.GetDirectories(fullPathDomainUserName, "Templates", SearchOption.AllDirectories);

                            if (domainfoldersFound.Count() == 0)
                            {
                                System.IO.Directory.CreateDirectory(fullPathUserName + "\\GroupPolicy\\Templates");
                                domainUsernamegroupPolicyPath = fullPathUserName + "\\GroupPolicy\\Templates";
                            }
                            else
                            {
                                domainUsernamegroupPolicyPath = domainfoldersFound[0];
                            }
                            try
                            {
                                DirectoryInfo templatedir1 = new DirectoryInfo(domainUsernamegroupPolicyPath);
                                FileInfo[] filesInDir1 = templatedir1.GetFiles("*.gpt");
                                var gptFileDirectory = new DirectoryInfo(Environment.CurrentDirectory);
                                FileInfo[] gptFiles = gptFileDirectory.GetFiles("*.gpt");
                                if (File.Exists(domainUsernamegroupPolicyPath + "\\templates.gpt"))
                                {
                                    File.Delete(domainUsernamegroupPolicyPath + "\\templates.gpt");
                                }
                                foreach (var item in gptFiles)
                                {

                                    System.IO.File.Copy(item.FullName, domainUsernamegroupPolicyPath + "\\" + item.Name, true);

                                }
                            }
                            catch
                            {

                            }
                        }
                        // assign username ,password,ipaddress local constant variables 
                        Constants.CitrixStudioIP = txtIP.Text;
                        Constants.CitrixStudioUserName = uname;
                        Constants.CitrixStudioPassword = txtPassword.Text;
                        
                         impersonationContext.Leave();

                    });




                    await Task.Factory.StartNew(() => StartWinRMService());
                    //StartWinRMService();
                    await Task.Factory.StartNew(() => EnablePowerShellPermission(Constants.CitrixStudioIP));

                    //await Task.Factory.StartNew(() => Constants.tempLoadingFlag = HDXReady());
                    //frmLoading icon1 = new frmLoading();
                    //icon1.ShowDialog();
                    //pictureBox1.Visible = false;
                    lblprocessing.Visible = false;

                    this.Close();
                }
                else
                {

                    MessageBox.Show(Constants.ValidationMsg, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }

            catch (System.IO.IOException ex)
            {
                //pictureBox1.Visible = false;
                lblprocessing.Visible = false;
                if (ex.Message == "The network path was not found.\r\n")
                {
                    MessageBox.Show("Unable to connect to Citrix Studio machine. Try connecting again Make sure the hostname/IP address of the remote machine is correct."+ "\n" + "\n" +
                                    "Remote computer is turned on and connected to the network, and that remote access is enabled.",
                                    Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (ex.Message == "The user name or password is incorrect.\r\n")
                {
                    MessageBox.Show("Invalid username or password. Please check and try again.", Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    
                    MessageBox.Show(ex.Message, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (System.Exception se)
            {
              //  pictureBox1.Visible = false;
                lblprocessing.Visible = false;
                int ret = Marshal.GetLastWin32Error();
                //  MessageBox.Show(ret.ToString(), "Error code: " + ret.ToString());
                MessageBox.Show(se.Message);
            }
            EnableItem();
        }

        public string HDXReady()
        {
            //step 1 create
            string result = String.Empty;
            var response = PowerShellScriptCall(Constants.CitrixStudioIP, Constants.CitrixStudioUserName, Constants.CitrixStudioPassword, "Create", "1");

            if (response.Contains("Already Exist in Studio"))
            {
                var enableResponse1 = PowerShellScriptCall(Constants.CitrixStudioIP, Constants.CitrixStudioUserName, Constants.CitrixStudioPassword, "StatusChange", "1", "true");
                
                var disableResponse1 = PowerShellScriptCall(Constants.CitrixStudioIP, Constants.CitrixStudioUserName, Constants.CitrixStudioPassword, "StatusChange", "2", "false");
               
                var disableResponse2 = PowerShellScriptCall(Constants.CitrixStudioIP, Constants.CitrixStudioUserName, Constants.CitrixStudioPassword, "StatusChange", "3", "false");

            }
            return result = "success";

                // Constants.HDXReadyPolicyStatus = true;               
            
        

        }

        public void DisableItem()
        {
            txtIP.Enabled = false;
            txtUserName.Enabled = false;
            txtPassword.Enabled = false;
            btnSubmit.Enabled = false;
            btnCancel.Enabled = false;
        }

        public void EnableItem()
        {
            txtIP.Enabled = true;
            txtUserName.Enabled = true;
            txtPassword.Enabled = true;
            btnSubmit.Enabled = true;
            btnCancel.Enabled = true;
        }

        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
    }



    public sealed class WrappedImpersonationContext
    {
        public enum LogonType : int
        {
            Interactive = 2,
            Network = 3,
            Batch = 4,
            Service = 5,
            Unlock = 7,
            NetworkClearText = 8,
            NewCredentials = 9
        }

        public enum LogonProvider : int
        {
            Default = 0,  // LOGON32_PROVIDER_DEFAULT
            WinNT35 = 1,
            WinNT40 = 2,  // Use the NTLM logon provider.
            WinNT50 = 3   // Use the negotiate logon provider.
        }

        [DllImport("advapi32.dll", EntryPoint = "LogonUserW", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern bool LogonUser(String lpszUsername, String lpszDomain,
            String lpszPassword, LogonType dwLogonType, LogonProvider dwLogonProvider, ref IntPtr phToken);

        [DllImport("kernel32.dll")]
        public extern static bool CloseHandle(IntPtr handle);

        private string _domain, _password, _username;
        private IntPtr _token;
        private WindowsImpersonationContext _context;

        private bool IsInContext
        {
            get { return _context != null; }
        }

        public WrappedImpersonationContext(string domain, string username, string password)
        {
            _domain = String.IsNullOrEmpty(domain) ? "." : domain;
            _username = username;
            _password = password;
        }

        // Changes the Windows identity of this thread. Make sure to always call Leave() at the end.
        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        public void Enter()
        {
            if (IsInContext)
                return;

            _token = IntPtr.Zero;
            bool logonSuccessfull = LogonUser(_username, _domain, _password, LogonType.NewCredentials, LogonProvider.WinNT50, ref _token);
            if (!logonSuccessfull)
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
            WindowsIdentity identity = new WindowsIdentity(_token);
            _context = identity.Impersonate();

            Debug.WriteLine(WindowsIdentity.GetCurrent().Name);
        }

        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        public void Leave()
        {
            if (!IsInContext)
                return;

            _context.Undo();

            if (_token != IntPtr.Zero)
            {
                CloseHandle(_token);
            }
            _context = null;
        }
    }
}
