﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThinClient.Verification.Common;

namespace ThinClientVerification.UserInterface
{
    public partial class frmSplashScreen : Form
    {
        public frmSplashScreen()
        {
            InitializeComponent();
            Screen myScreen = Screen.PrimaryScreen;
            int screenWidth = (myScreen.WorkingArea.Width);
            int screenHeight = (myScreen.WorkingArea.Height);

            picboxSplashScreen.Width = screenWidth - 100;
            picboxSplashScreen.Height = screenHeight - 50;
            picboxSplashScreen.Image = Image.FromFile(Globals.screenshotName);
        }
        Timer tmr;
        private void frmSplashScreen_Shown(object sender, EventArgs e)
        {
            tmr = new Timer();
            tmr.Interval = 1000;
            tmr.Start();
            tmr.Tick += tmr_Tick;
        }

        void tmr_Tick(object sender, EventArgs e)
        {
            tmr.Stop();
            this.Hide();
        }

        private void frmSplashScreen_Load(object sender, EventArgs e)
        {
            
        }
    }
}
